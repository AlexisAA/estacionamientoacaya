﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitorSAP
{

    public partial class Form1 : Form
    {
        pasavar vari = new pasavar();
        public int xClick = 0, yClick = 0;
        int tiemposegundos = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            configuracion f = new configuracion();

            if (f.ShowDialog() == DialogResult.OK)
            {
                vari.hora = f.phora;
                vari.minutos = f.pminuto;
                vari.pcheck = f.pcheck;
                vari.folder = f.pfolder;
                vari.lectura = f.pcarpetalectura;
            }
        }

        private void pic_cierra_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show("¿Esta seguro de cerrar la aplicación?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                case DialogResult.Yes:
                    this.Close();
                    break;
                case DialogResult.No:
                    break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblhora.Text = DateTime.Now.ToString();

            tiemposegundos = tiemposegundos + 1;
            if (tiemposegundos == 1800)
            {
                tiemposegundos = 0;
                procesaarchivos();
            }

            if (tiemposegundos > 1800)
            {
                tiemposegundos = 0;
            }

            if (vari.pcheck)
            {
                int hr = DateTime.Now.Hour;
                int min = DateTime.Now.Minute;
                int sec = DateTime.Now.Second;
                int hrb = int.Parse(vari.hora);
                int minb = int.Parse(vari.minutos);
                if (hr == hrb && min == minb && sec == 0)
                {
                    timer1.Enabled = false;
                    preocesstiempo();
                    Application.DoEvents();
                    timer1.Enabled = true;
                }
            }

        }

        private void lbl_titulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            { xClick = e.X; yClick = e.Y; }
            else
            {
                int _left = this.Left + (e.X - xClick);
                int _top = this.Top + (e.Y - yClick);
                this.Left = this.Left + (e.X - xClick);
                this.Top = this.Top + (e.Y - yClick);
            }
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

            notifyIcon.Visible = false;
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

            notifyIcon.Visible = false;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                notifyIcon.Visible = true; ;
                notifyIcon.BalloonTipText = "Minimizado a notificaciones";
                notifyIcon.BalloonTipTitle = "Monitor SAP Autofacturación";
                notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                notifyIcon.ShowBalloonTip(5000);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int deskHeight = Screen.PrimaryScreen.Bounds.Height - this.Height;
            int deskWidth = Screen.PrimaryScreen.Bounds.Width - this.Width;
            Location = new Point(deskWidth, deskHeight - 40);

            System.DateTime today = System.DateTime.Now;
            System.DateTime answer = today.AddDays(-1);

            fbusca.Text = answer.ToString();

            IniFile inif = new IniFile("Settings.ini");

            vari.hora = inif.Read("hora");
            vari.minutos = inif.Read("minuto");

            vari.folder = inif.Read("sourcefile");
            vari.lectura = inif.Read("lectura_procesados");

            if (inif.Read("checa") == "1")
            {
                vari.pcheck = true;
            }
            else
            {
                vari.pcheck = false;
            }
        }

        private void lbl_titulo_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            preocess();
        }

        public void preocess()
        {

            //lbl_t.Text = "";
            string fb1 = fbusca.Value.ToString("yyyy-MM-dd 00:00");
            string fb2 = fbusca.Value.ToString("yyyy-MM-dd 23:59");


            wsservicio.ws_sap servi = new wsservicio.ws_sap();
            var respu = servi.regresa(fb1, fb2);


            DateTime dt = DateTime.Parse(fbusca.Value.ToString("yyyy-MM-dd 00:00"));
            string ok = dt.ToString("yyyyMMdd");
            string filename = dt.ToString("yyyyMMdd");

            var jsonlist = JsonConvert.DeserializeObject<List<facturas>>(respu);

            string path = @vari.folder + "\\" + filename + ".txt";
            using (var sw = new StreamWriter(path))
            {
                for (var i = 0; i < jsonlist.Count; i++)
                {
                    sw.WriteLine(jsonlist[i].id_sistema + "|" + jsonlist[i].tipo + "|" + jsonlist[i].rfc + "|" + jsonlist[i].Razon_social + "|" + jsonlist[i].Fecha_Factura + "|" + jsonlist[i].UUID + "|" + jsonlist[i].iva + "|" + jsonlist[i].Importe);
                }
            }
        }

        public void preocesstiempo()
        {
            DateTime diaanterior = DateTime.Now.AddDays(-1);
            fbusca.Value = diaanterior;

            string fb1 = fbusca.Value.ToString("yyyy-MM-dd 00:00");
            string fb2 = fbusca.Value.ToString("yyyy-MM-dd 23:59");

            //ws_sapSoapClient soli = new ws_sapSoapClient();
            wsservicio.ws_sap soli = new wsservicio.ws_sap();
            var respu = soli.regresa(fb1, fb2);

            DateTime dt = DateTime.Parse(fbusca.Value.ToString("yyyy-MM-dd 00:00"));
            string ok = dt.ToString("yyyyMMdd");
            string filename = dt.ToString("yyyyMMdd");

            var jsonlist = JsonConvert.DeserializeObject<List<facturas>>(respu);

            string path = vari.folder + "\\" + filename + ".txt";
            using (var sw = new StreamWriter(path))
            {
                for (var i = 0; i < jsonlist.Count; i++)
                {
                    sw.WriteLine(jsonlist[i].id_sistema + "|" + jsonlist[i].tipo + "|" + jsonlist[i].rfc + "|" + jsonlist[i].Razon_social + "|" + jsonlist[i].Fecha_Factura + "|" + jsonlist[i].UUID + "|" + jsonlist[i].iva + "|" + jsonlist[i].Importe);
                }
            }


        }
        public void procesaarchivos()
        {
            DirectoryInfo di = new DirectoryInfo(vari.lectura);
            var fia = di.GetFiles();
            int cuantos = fia.Count();
            if (cuantos > 0)
            {
                foreach (var fi in di.GetFiles())
                {
                    leefile(fi.Name);
                }
            }
        }

        public void leefile(string archivo)
        {
            int counter = 0;
            string line;
            System.IO.StreamReader file =
                new System.IO.StreamReader(vari.lectura + "\\" + archivo);
            string pasavar = "";
            pasavar += "{'results':[";
            while ((line = file.ReadLine()) != null)
            {
                string s = line;
                string[] words = s.Split('|');
                string xuuid = words[0];
                string CODEJ = words[1];
                string MSGEJ = words[2];
                string POLIZA = words[3];
                string EJERCICIO = words[4];
                pasavar += "{'uuid':'" + xuuid + "','CODEJ':'" + CODEJ + "','MSGEJ':'" + MSGEJ + "','POLIZA':'" + POLIZA + "','EJERCICIO':'" + EJERCICIO + "'},";
                //savepoli(xuuid, CODEJ, MSGEJ, POLIZA, EJERCICIO);
                System.Console.WriteLine(line);
                counter++;
            }
            pasavar = pasavar.TrimEnd(',');
            pasavar += "]}";
            wsservicio.ws_sap soli = new wsservicio.ws_sap();
            //ws_sapSoapClient soli = new ws_sapSoapClient();
            bool sino = soli.recipol(pasavar);
            file.Close();
            if (sino)
            {

                File.Move(vari.lectura + "\\" + archivo, vari.lectura + "\\Enviadas\\" + archivo);
            }
            else
            {
                File.Move(vari.lectura + "\\" + archivo, vari.lectura + "\\Error\\" + archivo);
                sendemail(archivo);
            }


        }

        public void sendemail(string archivo)
        {
            try
            {
                IniFile inif = new IniFile("Settings.ini");

                string nameadmin = inif.Read("admin");
                string correoadmin = inif.Read("email");

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.1and1.mx");

                mail.From = new MailAddress("factura@espacia.mx");
                mail.To.Add(correoadmin);
                mail.Subject = "Ocurrio un error";
                mail.Body = "Estimado (a) " + nameadmin + " Ocurrio un error en la transferencia del archivo de polizas al portal de facturación archivo: " + archivo;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("factura@espacia.mx", "Facturacion2017");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }
    }
}

public class facturas
{
    public string id_sistema { get; set; }
    public string tipo { get; set; }
    public string rfc { get; set; }
    public string Razon_social { get; set; }
    public string Fecha_Factura { get; set; }
    public string UUID { get; set; }
    public string iva { get; set; }
    public string Importe { get; set; }
}
