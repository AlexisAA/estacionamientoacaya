﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitorSAP
{
    public partial class configuracion : Form
    {
        public string phora { get; set; }
        public string pminuto { get; set; }
        public bool pcheck { get; set; }
        public string pfolder { get; set; }
        public string pcarpetalectura { get; set; }

        public int xClick = 0, yClick = 0;
        public configuracion()
        {
            InitializeComponent();
        }

        private void pic_cierra_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void configuracion_Load(object sender, EventArgs e)
        {
            IniFile inif = new IniFile("Settings.ini");
            String tfuente = inif.Read("sourcefile");
            string xhora = inif.Read("hora");
            string xminuto = inif.Read("minuto");
            string xcheca = inif.Read("checa");
            string xadmin = inif.Read("admin");
            string xemail = inif.Read("email");
            string xproce = inif.Read("lectura_procesados");

            txt_folder.Text = tfuente;
            choras.Text = xhora;
            cminutos.Text = xminuto;
            admin.Text = xadmin;
            email.Text = xemail;
            txt_lectura.Text = xproce;
            if (xcheca == "1")
            {
                tcheca.Checked = true;
            }
            else
            {
                tcheca.Checked = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txt_folder.Text == "")
            {
                MessageBox.Show("Por favor capture la ruta donde se almacenarán los archivos");
                return;
            }
            if (admin.Text == "")
            {
                MessageBox.Show("Por favor capture el nombre del Administrador");
                return;
            }
            if (email.Text == "")
            {
                MessageBox.Show("Por favor capture el e-mail del administrador");
                return;
            }
            if (txt_lectura.Text == "")
            {
                MessageBox.Show("Por favor capture la ruta donde se almacenarán los archivos");
                return;
            }
            if (tcheca.Checked)
            {
                if (choras.Text == "")
                {
                    MessageBox.Show("Por favor capturela hora");
                    return;
                }
                if (cminutos.Text == "")
                {
                    MessageBox.Show("Por favor capture los minutos");
                    return;
                }
            }

            var MyIni = new IniFile("Settings.ini");
            MyIni.Write("sourcefile", txt_folder.Text);
            MyIni.Write("hora", choras.Text);
            MyIni.Write("minuto", cminutos.Text);
            MyIni.Write("admin", admin.Text);
            MyIni.Write("email", email.Text);
            MyIni.Write("lectura_procesados", txt_lectura.Text);
            if (tcheca.Checked)
            {
                MyIni.Write("checa", "1");
                pcheck = true;
            }
            else
            {
                MyIni.Write("checa", "0");
                pcheck = false;
            }

            phora = choras.Text;
            pminuto = cminutos.Text;
            pfolder = txt_folder.Text;
            pcarpetalectura = txt_lectura.Text; 
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void panelSup_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            { xClick = e.X; yClick = e.Y; }
            else
            {
                int _left = this.Left + (e.X - xClick);
                int _top = this.Top + (e.Y - yClick);
                this.Left = this.Left + (e.X - xClick);
                this.Top = this.Top + (e.Y - yClick);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderBrowserDialog
            {
                //Description = Resources.DefaultLocationDialogHelp,
                Description = "Seleccione la carpeta donde se alojarán los archivos.",
                ShowNewFolderButton = true
            };

            if (DialogResult.OK == folderDialog.ShowDialog())
            {
                txt_folder.Text = folderDialog.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderBrowserDialog
            {
                //Description = Resources.DefaultLocationDialogHelp,
                Description = "Seleccione la carpeta de donde se leeran los archivos.",
                ShowNewFolderButton = true
            };

            if (DialogResult.OK == folderDialog.ShowDialog())
            {
                txt_lectura.Text = folderDialog.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
