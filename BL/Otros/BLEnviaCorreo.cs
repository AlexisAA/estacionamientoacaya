﻿using System;
using System.Text;

using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

namespace BL.Otros
{
    public class BLEnviaCorreo
    {
        public string EnviaFcturas(string correo, string _path, string _nombreXML, string _nombrePDF)
        {
            string resp = "";
            //checas("Entro");
            #region Mensaje correo
            string strMensaje = "";
            strMensaje = "<html style='font-size: 12px; font-family: Helvetica, Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #eeeeee;'>";
            strMensaje += "<head><title></title><meta charset='utf-8' /></head>";

            strMensaje += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
            strMensaje += "<p style='font-size: 24px; font-weight: bold; text-transform: uppercase; color: #000000;'>Estimado Cliente:</p>";
            strMensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            strMensaje += "Por medio del siguiente correo se le hace llegar la factura que usted ha emitido a través del Portal de Facturación del Centro Comercial Acaya Mazatlán";
            strMensaje += "</p></div>";

            strMensaje += "<div style='text-align: left;'><p style='font-size: 16px; color: #000000;'>";
            strMensaje += "Usted puede consultar la factura en línea en la siguiente dirección: ";
            strMensaje += "<A HREF='http://facturación.acayamazatlan.espacia.mx' TARGET='_new'>http://facturación.acayamazatlan.espacia.mx</A>";
            strMensaje += "</div><br/><br/>";
            strMensaje += "<div><p style='font-size: 20px; font-weight: bold; text-transform: uppercase; color: #004370;'>Atentamente</p>";
            strMensaje += "<p style='font-size: 16px; font-weight: bold; text-transform: uppercase; color: #004370;'></p></div>";

            strMensaje += "<div style='width: 30%; height: 30%; padding: 0; text-align: left;'><img src='cid:imagen' style='Width: 30px'/></div>";

            strMensaje += "</body></html>";
            //checas(strMensaje);
            string imgCorreo = string.Empty;
            imgCorreo = ConfigurationManager.AppSettings["imgCorreo"].ToString();
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strMensaje, Encoding.UTF8, MediaTypeNames.Text.Html);
            LinkedResource img = new LinkedResource(imgCorreo, MediaTypeNames.Image.Jpeg);
            img.ContentId = "imagen";
            htmlView.LinkedResources.Add(img);
            #endregion
            //checas("2");
            #region Configuración de correo
            //Datos para la configuración del correo
            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            //string strServer = "smtp.gmail.com";
            //string strRemitente = "facturacionespacia@gmail.com";
            ////string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            //string strContrasenia = "Prime@123";
            //int intPuerto = 587;
            //bool ssl =true;
            // checas("valido");

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = intPuerto;
            datosSmtp.EnableSsl = ssl;

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            //objetoMail.AlternateViews.Add(plainView);
            objetoMail.AlternateViews.Add(htmlView);

            MailAddress mailDestinatario = new MailAddress(strRemitente);
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "Servicio de Emisión de Comprobantes Fiscales Digitales - Centro Comercial Acaya Mazatlán";
            #endregion
            //checas("envio");
            //Datos de las copias
            #region Agrega copias
            string[] addMailsCC = correo.Split('|');

            foreach (string mail in addMailsCC)
            {
                if (mail != "")
                {
                    MailAddress mailCC = new MailAddress(mail);
                    objetoMail.CC.Add(mailCC);
                }
            }

           // checas("atach");
            #endregion

            #region Adjunta archivos
            try
            {
                Attachment adjunaXML = new Attachment(_path + _nombreXML);
                objetoMail.Attachments.Add(adjunaXML);

                if (_nombrePDF != null)
                {
                    Attachment adjunaPDF = new Attachment(_path + _nombrePDF);
                    objetoMail.Attachments.Add(adjunaPDF);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region Envía correo
            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                resp = "Ok";
               // checas("envio");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            return resp;
        }

        public string EnviaLayOut(string correo, string mensaje)
        {
            string resp = "";

            #region Mensaje correo
            string strMensaje = "";
            strMensaje = "<html style='font-size: 12px; font-family: Helvetica, Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #eeeeee;'>";
            strMensaje += "<head><title></title><meta charset='utf-8' /></head>";

            strMensaje += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
            strMensaje += "<p style='font-size: 10px; font-weight: bold; text-transform: uppercase; color: #000000;'>" + mensaje + "</p>";
            strMensaje += "<div><p style='font-size: 16px; color: #000000;'>";
            strMensaje += "Por medio del siguiente correo se le hace llegar la factura que usted ha emitido a través del Portal de Facturación del Centro Comercial Acaya Mazatlán";
            strMensaje += "</p></div>";

            strMensaje += "</body></html>";
                        
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(strMensaje, Encoding.UTF8, MediaTypeNames.Text.Html);
            #endregion

            #region Configuración de correo
            //Datos para la configuración del correo
            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = intPuerto;
            datosSmtp.EnableSsl = ssl;

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            //objetoMail.AlternateViews.Add(plainView);
            objetoMail.AlternateViews.Add(htmlView);

            MailAddress mailDestinatario = new MailAddress(strRemitente);
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "LayOut" ;
            #endregion

            //Datos de las copias
            #region Agrega copias
            string[] addMailsCC = correo.Split('|');

            foreach (string mail in addMailsCC)
            {
                if (mail != "")
                {
                    MailAddress mailCC = new MailAddress(mail);
                    objetoMail.CC.Add(mailCC);
                }
            }
            #endregion

            #region Envía correo
            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                resp = "Ok";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            return resp;
        }

        private byte[] convierteaBits(string _path)
        {
            byte[] resp = null;
            try
            {
                FileStream fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                resp = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
            }
            catch (Exception ex) { throw ex; }

            return resp;
        }

        public void checas(string vali)
        {
            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential("facturacionmzt@acaya.mx", "SU$*0220");
            datosSmtp.Host = "smtp.office365.com";
            datosSmtp.Port = 587;
            datosSmtp.EnableSsl = true;

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress("facturacionmzt@acaya.mx");
            objetoMail.From = mailRemitente;
            //objetoMail.AlternateViews.Add(plainView);
            //objetoMail.AlternateViews.Add(htmlView);

            MailAddress mailDestinatario = new MailAddress("jtrume@gmail.com");
            objetoMail.To.Add(mailDestinatario);
            objetoMail.Subject = "Servicio de Emisión de Comprobantes Fiscales Digitales - Centro Comercial Acaya Mazatlán";

            objetoMail.Body = vali;
            datosSmtp.Send(objetoMail);
            datosSmtp.Dispose();
        }
    }
}
