﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BL.Otros
{
    public class BLValidaciones
    {
        public bool ValidaRFC(string rfc)
        {
            //string pattern = @"^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$";
            string pattern = @"^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$";

            if (string.IsNullOrEmpty(rfc))
                return false;
            else
                return Regex.IsMatch(rfc, pattern);
        }

        public bool ValidaEmail(string email)
        {
            string pattern = @"^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$";

            if (string.IsNullOrEmpty(email))
                return false;
            else
                return Regex.IsMatch(email, pattern);
        }

        public bool ValidaCampoVacio(string campo)
        {
            if (string.IsNullOrEmpty(campo))
                return true;
            else
                return false;
        }

        public bool ValidaTmanioCampo(string campo, int sizemin, int sizemax)
        {
            if (campo.Length > sizemax)
                return true;
            else if (campo.Length >= sizemin)
                return true;
            else
                return false;
        }

        public bool ValidateRFCLocal(string rfc)
        {
            bool validaTF = true;

            if (ValidaCampoVacio(rfc))
                validaTF = false;
            else
            {
                if (rfc.Length > 13)
                    validaTF = false;
                else
                {
                    if (rfc.Length < 10)
                        validaTF = false;
                    else
                    {
                        if (ValidaRFC(rfc))
                            validaTF = true;
                        else
                            validaTF = false;
                    }
                }
            }
            return validaTF;
        }

        public bool ValidateDecimal(string valor)
        {
            bool validaDec = true;
            decimal valuedec = 0;

            if (ValidaCampoVacio(valor))
                validaDec = false;
            else
                validaDec = decimal.TryParse(valor, out valuedec);

            return validaDec;
        }

        public bool ValidateDecimalMayorACero(string valor)
        {
            decimal valuedec = 0;

            if (valuedec > 0)
                return true;
            else
                return false;
        }

        public bool ValidaFormadePago(string valor)
        {
            bool validFP = false;
            string[] arrayFP = new string[] { "01", "02", "03", "04", "05", "06", "08", "12", "13", "14", "15", "17", "23", "24", "25", "26", "27", "28", "29", "99" };
            foreach (string item in arrayFP)
            {
                if(item == valor)
                {
                    validFP = true;
                    break;
                }
            }

            return validFP;

        }

        public bool ValidaMetododePago(string valor)
        {
            bool validFP = false;

            string[] arrayFP = new string[] { "PUE", "PPD" };
            foreach (string item in arrayFP)
            {
                if (item == valor)
                {
                    validFP = true;
                    break;
                }
            }

            return validFP;
        }

        public bool Validausocfdi(string valor)
        {
            bool validUC = false;

            string[] arrayUC = new string[] { "G01", "G02", "G03", "I01", "I02", "I03", "I04", "I05", "I06", "I07", "I08", "D01", "D02", "D03", "D04", "D05", "D06", "D07", "D08", "D09", "D10", "P01" };
            foreach (string item in arrayUC)
            {
                if (item == valor)
                {
                    validUC = true;
                    break;
                }
            }

            return validUC;

        }

        public bool ValidaClaveUnidad(string valor)
        {
            bool validFP = false;

            string[] arrayFP = new string[] { "E48","H87", "EA","ACT","KGM","GRM","A9","MTR","INH","FOT","YRD","SMI","MTK","CMK","MTQ","LTR","GLI","GLL","HUR","DAY","ANN","C62","5B","AB","LO","XLT","LH","AS","HEA","IE","NMP","SET","ZZ","XBX","XKI","XOK" };
            foreach (string item in arrayFP)
            {
                if (item == valor)
                {
                    validFP = true;
                    break;
                }
            }

            return validFP;

        }

    }
}
