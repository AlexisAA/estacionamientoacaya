﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;

namespace BL.Portal
{
    public class BLTickets_Facturados
    {
        #region buscaTickets_Facturados
        /// <summary>
        /// Busca tickets facturados activos
        /// </summary>
        /// <param name="no_ticket"></param>
        /// <returns></returns>
        public List<Tickets_Facturados> buscaTickets_Facturados(string no_ticket)
        {
            Tickets_Facturados tf = new Tickets_Facturados();
            tf.No_Ticket = no_ticket;
            try
            {
                return BuTickets_Facturados.Buscar(tf);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region ABC Tickets_Facturas
        /// <summary>
        /// Inserta Ticket_Factura
        /// </summary>
        /// <param name="no_ticket"></param>
        /// <param name="id_factura"></param>
        /// <returns></returns>
        public bool insertaTenF(string no_ticket, int id_factura)
        {
            Tickets_Facturados tf = new Tickets_Facturados();
            tf.No_Ticket = no_ticket;
            tf.ID_Factura = id_factura;
            tf.Valido = true;
            try
            {
                return BuTickets_Facturados.Insertar(tf);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

    }
}
