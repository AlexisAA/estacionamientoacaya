﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;

namespace BL.Portal
{
    public class BLClientes
    {
        #region buscaCliente

        public List<Clientes> buscaCliente()
        {
            Clientes cliente = new Clientes();
            try
            {
                return BuClientes.Buscar(cliente);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar cliente pro RFC
        /// </summary>
        /// <param name="rfc"></param>
        /// <returns></returns>
        public List<Clientes> buscaClienteRFC(string rfc)
        {
            Clientes cliente = new Clientes();
            cliente.RFC = rfc;
            try
            {
                return BuClientes.Buscar(cliente);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar cliente por ID
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Clientes> buscaClienteID(int id_cliente)
        {
            Clientes cliente = new Clientes();
            cliente.ID_Cliente = id_cliente;
            try
            {
                return BuClientes.Buscar(cliente);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region ABC Clientes
        /// <summary>
        /// Metodo para insertar cliente
        /// </summary>
        /// <param name="rfc"></param>
        /// <param name="razon_social"></param>
        /// <param name="calle"></param>
        /// <param name="no_ext"></param>
        /// <param name="no_int"></param>
        /// <param name="cp"></param>
        /// <param name="col"></param>
        /// <param name="del"></param>
        /// <param name="ciudad"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        public bool insertaClientes(string rfc, string razon_social, string calle, string no_ext, string no_int,
            string cp, string col, string del, string ciudad, string estado)
        {
            Clientes cliente = new Clientes();
            cliente.RFC = rfc;
            cliente.Razon_social = razon_social;
            cliente.Calle = calle;
            cliente.No_Exterior = no_ext;
            cliente.No_Interior = no_int;
            cliente.Codigo_Postal = cp;
            cliente.Col_Pob_Cd = col;
            cliente.Del_Muni = del;
            cliente.Ciudad = ciudad;
            cliente.Estado = estado;
            try
            {
                return BuClientes.Insertar(cliente);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para actualizar cliente
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="razon_social"></param>
        /// <param name="calle"></param>
        /// <param name="no_ext"></param>
        /// <param name="no_int"></param>
        /// <param name="cp"></param>
        /// <param name="col"></param>
        /// <param name="del"></param>
        /// <param name="ciudad"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        public bool actualizaCliente(int id_cliente, string razon_social, string calle, string no_ext, string no_int, 
            string cp, string col, string del, string ciudad, string estado)
        {
            Clientes cliente = new Clientes();
            cliente.ID_Cliente = id_cliente;
            cliente.Razon_social = razon_social;
            cliente.Calle = calle;
            cliente.No_Exterior = no_ext;
            cliente.No_Interior = no_int;
            cliente.Codigo_Postal = cp;
            cliente.Col_Pob_Cd = col;
            cliente.Del_Muni = del;
            cliente.Ciudad = ciudad;
            cliente.Estado = estado;
            try
            {
                return BuClientes.Actualizar(cliente);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

    }
}
