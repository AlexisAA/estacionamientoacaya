﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;

namespace BL.Portal
{
    public class BLPensiones
    {


        #region ABC Pensiones
        public bool insertaPension(decimal cantidad, string unidad, string descripcion, decimal precio_unitario, decimal importe,
            string correos, DateTime fecha_creacion, int id_factura)
        {
            Pensiones pen = new Pensiones();
            pen.Cantidad = cantidad;
            pen.Unidad = unidad;
            pen.Descripcion = descripcion;
            pen.Precio_Unitario = precio_unitario;
            pen.Importe = importe;
            pen.Correos = correos;
            pen.Fecha_Creacion = fecha_creacion;
            pen.ID_Factura = id_factura;
            try
            {
                return BUPensiones.Insertar(pen);
            }
            catch(Exception ex) { throw ex; }
        }
        #endregion


    }
}
