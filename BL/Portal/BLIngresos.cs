﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;

namespace BL.Portal
{
    public class BLIngresos
    {

        #region buscaIngresos_Pensiones
        /// <summary>
        /// Metodo para buscar Ingresos_Pensiones
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Ingresos> buscaIngresos_PensionesXCliente(int id_cliente)
        {
            Ingresos ingpen = new Ingresos();
            ingpen.ID_Cliente = id_cliente;
            try
            {
                return BUIngresos.Buscar(ingpen);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar Ingresos_Pensiones
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public List<Ingresos> buscaIngresos_PensionesXId(int id_ingreso)
        {
            Ingresos ingpen = new Ingresos();
            ingpen.ID_IngresoPensiones = id_ingreso;
            try
            {
                return BUIngresos.Buscar(ingpen);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region ABC Ingresos_Pensiones
        /// <summary>
        /// Metodo para insertar factura
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="subtotal"></param>
        /// <param name="total"></param>
        /// <param name="forma_pago"></param>
        /// /// <param name="descripcion"></param>
        /// <param name="fecha_creacion"></param>
        /// <returns></returns>
        public bool insertaIngresosPensiones(int id_cliente, decimal subtotal, decimal total, string forma_pago, string desccripcion, DateTime fecha_creacion)
        {
            Ingresos ingpen = new Ingresos();
            ingpen.ID_Cliente = id_cliente;
            ingpen.Subtotal = subtotal;
            ingpen.Total = total;
            ingpen.Forma_Pago = forma_pago;
            ingpen.Descripcion = desccripcion;
            ingpen.Fecha_Creacion = fecha_creacion;

            try
            {
                return BUIngresos.Insertar(ingpen);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
