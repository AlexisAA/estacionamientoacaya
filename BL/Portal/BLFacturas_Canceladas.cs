﻿using DAL.Bean.Portal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Portal
{
    public class BLFacturas_Canceladas
    {
        #region buscaFacturas
        /// <summary>
        /// Metodo para buscar facturas canceladas
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Facturas_Canceladas> buscaFacturas_Canceladas(int id_factura_can)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura_Cancelada = id_factura_can;

            try
            {
                return BUFacturas_Canceladas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar Facturas_Canceladas
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public List<Facturas_Canceladas> buscaFacturas_Canceladas(int id_factura, int id_cliente, string uuid)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura = id_factura;
            fac.ID_Cliente = id_cliente;
            fac.UUID = uuid;

            try
            {
                return BUFacturas_Canceladas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar facturas canceladas
        /// </summary>
        /// <param name="id_factura"></param>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Facturas_Canceladas> buscaFacturaCancelada(int id_factura, int id_cliente)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura = id_factura;
            fac.ID_Cliente = id_cliente;

            try
            {
                return BUFacturas_Canceladas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        public List<Facturas_Canceladas> buscaFacturaCancelada(string uuid)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.UUID = uuid;

            try
            {
                return BUFacturas_Canceladas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region ABC Facturas
        /// <summary>
        /// Metodo para insertar factura cancelada
        /// </summary>
        /// <param name="id_factura"></param>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <param name="fecha_factura"></param>
        /// <param name="nombre_xml"></param>
        /// <param name="archivo_xml"></param>
        /// <param name="descripcion"></param>
        /// <param name="valido"></param>
        /// <returns></returns>
        public bool insertaFacturaCancelada(int id_factura, int id_cliente, string uuid, DateTime fecha_factura, string nombre_xml, byte[] archivo_xml, string descripcion, bool valido)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura = id_factura;
            fac.ID_Cliente = id_cliente;
            fac.UUID = uuid;
            fac.Fecha_Factura = fecha_factura;
            fac.Nombre_XML = nombre_xml;
            fac.Archivo_XML = archivo_xml;
            fac.Descripcion = descripcion;
            fac.Valido = valido;
            try
            {
                return BUFacturas_Canceladas.Insertar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para actualizar factura cancelada
        /// </summary>
        /// <param name="id_factura"></param>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <param name="fecha_factura"></param>
        /// <param name="nombre_xml"></param>
        /// <param name="archivo_xml"></param>
        /// <param name="descripcion"></param>
        /// <param name="valido"></param>
        /// <returns></returns>
        public bool actualizaFacturaCancelada(int id_factura, int id_cliente, string uuid, DateTime fecha_factura, string nombre_xml, byte[] archivo_xml, string descripcion, bool valido)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura = id_factura;
            fac.ID_Cliente = id_cliente;
            fac.UUID = uuid;
            fac.Fecha_Factura = fecha_factura;
            fac.Nombre_XML = nombre_xml;
            fac.Archivo_XML = archivo_xml;
            fac.Descripcion = descripcion;
            fac.Valido = valido;
            try
            {
                return BUFacturas_Canceladas.Actualizar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// metodo para eliminar logico factura cancelada
        /// </summary>
        /// <param name="id_factura"></param>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <param name="fecha_factura"></param>
        /// <param name="nombre_xml"></param>
        /// <param name="archivo_xml"></param>
        /// <param name="descripcion"></param>
        /// <param name="valido"></param>
        /// <returns></returns>
        public bool eliminaFacturaCancelada(int id_factura_cancel)
        {
            Facturas_Canceladas fac = new Facturas_Canceladas();
            fac.ID_Factura = id_factura_cancel;
            fac.Valido = false;

            try
            {
                return BUFacturas_Canceladas.Eliminar(fac);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region DAL
        //public DataTable buscaFacturas_Clientes(int id_cliente, DateTime finicio, DateTime ffin)
        //{
        //    DataTable dtBLFacturas = new DataTable();
        //    dtBLFacturas = null;
        //    try
        //    {
        //        dtBLFacturas = DALFacturas_Varios.DALBuscaFacturas(id_cliente, finicio, ffin);

        //        return dtBLFacturas;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        //public DataTable buscaFacturas_Clientes(int id_cliente, DateTime finicio, DateTime ffin, string ofactura)
        //{
        //    DataTable dtBLFacturas = new DataTable();
        //    dtBLFacturas = null;
        //    try
        //    {
        //        dtBLFacturas = DALFacturas_Varios.DALBuscaFacturas(id_cliente, finicio, ffin, ofactura);

        //        return dtBLFacturas;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}
        #endregion
    }
}
