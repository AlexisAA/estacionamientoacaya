﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;

namespace BL.Portal
{
    public class BLCorreo_Cliente
    {

        #region BuscaCorreo
        /// <summary>
        /// Metodo para buscar correo
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Correo_Cliente> BuscaCorreo(int id_cliente)
        {
            Correo_Cliente cc = new Correo_Cliente();
            cc.ID_Cliente = id_cliente;
            try
            {
                return BUCorreo_Cliente.Buscar(cc);
            }
            catch(Exception ex) { throw ex; }

        }

        /// <summary>
        /// Metodo para buscar correo
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="correo"></param>
        /// <returns></returns>
        public List<Correo_Cliente> BuscaCorreo(int id_cliente, string correo)
        {
            Correo_Cliente cc = new Correo_Cliente();
            cc.ID_Cliente = id_cliente;
            cc.Correo = correo;
            try
            {
                return BUCorreo_Cliente.Buscar(cc);
            }
            catch (Exception ex) { throw ex; }

        }
        #endregion

        #region ABC Correo Cliente
        public bool insertaCorreoCliente(int id_cliente, string correo)
        {
            Correo_Cliente cc = new Correo_Cliente();
            cc.ID_Cliente = id_cliente;
            cc.Correo = correo;
            cc.Valido = true;
            try
            {
                return BUCorreo_Cliente.Insertar(cc);
            }
            catch (Exception ex) { throw ex; }
        }

        public bool ActualizaCorreoCliente(int id_correo, bool valido)
        {
            Correo_Cliente cc = new Correo_Cliente();
            cc.ID_Correo = id_correo;
            cc.Valido = valido;
            try
            {
                return BUCorreo_Cliente.Actualizar(cc);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

    }
}
