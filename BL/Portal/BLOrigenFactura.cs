﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using DAL.DataAccess.Portal;

namespace BL.Portal
{
    public class BLOrigenFactura
    {
        public DataTable buscaOrigenFactura()
        {
            DataTable dtBLOFacturas = new DataTable();
            dtBLOFacturas = null;
            try
            {
                dtBLOFacturas = DALOrigenFactura.DALBuscaOFactura();

                return dtBLOFacturas;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
