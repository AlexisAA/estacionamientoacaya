﻿using System;
using System.Collections.Generic;

using DAL.Bean.Portal;
using DAL.DataAccess.Portal;
using System.Data;

namespace BL.Portal
{
    public class BLFacturas
    {
        #region buscaFacturas
        /// <summary>
        /// Metodo para buscar facturas
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <returns></returns>
        public List<Facturas> buscaFacturas(int id_factura)
        {
            Facturas fac = new Facturas();
            fac.ID_Factura = id_factura;
            try
            {
                return BuFacturas.Buscar(fac);
            }
            catch(Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar facturas
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public List<Facturas> buscaFacturas(int id_cliente, string uuid)
        {
            Facturas fac = new Facturas();
            fac.ID_Cliente = id_cliente;
            fac.UUID = uuid;
            try
            {
                return BuFacturas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para contar facturas y determinar el Folio
        /// </summary>
        /// <returns></returns>
        public int cuentaFacturas()
        {
            int noReg = 0;
            List<Facturas> csFcturas = new List<Facturas>();
            Facturas fac = new Facturas();
            try
            {
                csFcturas = BuFacturas.Buscar(fac);
                noReg = csFcturas.Count;
                return noReg;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Metodo para buscar facturas
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="id_factura"></param>
        /// <returns></returns>
        public List<Facturas> buscaFactura(int id_factura, int id_cliente)
        {
            Facturas fac = new Facturas();
            fac.ID_Factura = id_factura;
            fac.ID_Cliente = id_cliente;
            try
            {
                return BuFacturas.Buscar(fac);
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region ABC Facturas
        /// <summary>
        /// Metodo para insertar factura
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="uuid"></param>
        /// <param name="fecha_factura"></param>
        /// <param name="importe"></param>
        /// <param name="folio"></param>
        /// <param name="serie"></param>
        /// <param name="nombre_xml"></param>
        /// <param name="nombre_pdf"></param>
        /// <param name="archivo_xml"></param>
        /// <param name="archivo_pdf"></param>
        /// <param name="valido"></param>
        /// <returns></returns>
        public bool insertaFactura(int id_cliente, string uuid, DateTime fecha_factura, decimal importe, string folio,
            string serie, string descripcion, string nombre_xml, string nombre_pdf, byte[] archivo_xml, 
            byte[] archivo_pdf, string id_origen_factura, bool valido)
        {
            Facturas fac = new Facturas();
            fac.ID_Cliente = id_cliente;
            fac.UUID = uuid;
            fac.Fecha_Factura = fecha_factura;
            fac.Importe = importe;
            fac.Folio = folio;
            fac.Serie = serie;
            fac.Descripcion = descripcion;
            fac.Nombre_XML = nombre_xml;
            fac.Nombre_PDF = nombre_pdf;
            fac.Archivo_XML = archivo_xml;
            fac.Archivo_PDF = archivo_pdf;
            fac.ID_Origen_Factura = id_origen_factura;
            fac.Valido = valido;
            try
            {
                return BuFacturas.Insertar(fac);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region DAL
        public DataTable buscaFacturas_Clientes(int id_cliente, DateTime finicio, DateTime ffin)
        {
            DataTable dtBLFacturas = new DataTable();
            dtBLFacturas = null;
            try
            {
                dtBLFacturas = DALFacturas_Varios.DALBuscaFacturas(id_cliente, finicio, ffin);

                return dtBLFacturas;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable buscaFacturas_Clientes(int id_cliente, DateTime finicio, DateTime ffin, string ofactura)
        {
            DataTable dtBLFacturas = new DataTable();
            dtBLFacturas = null;
            try
            {
                dtBLFacturas = DALFacturas_Varios.DALBuscaFacturas(id_cliente, finicio, ffin, ofactura);

                return dtBLFacturas;
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}
