﻿using System;
using System.Collections.Generic;

using DAL.Bean.Catalogos;
using System.Data;
using DAL.DataAccess.Catalogos;

namespace BL.Catalogos
{
    public class BLTickets
    {
        #region Busca Ticket
        /// <summary>
        /// Metodo para buscar Ticket
        /// </summary>
        /// <param name="no_ticket"></param>
        /// <returns></returns>
        public List<Tickets> buscaTicket(string no_ticket)
        {
            Tickets ticket = new Tickets();
            ticket.No_Ticket = no_ticket;
            try
            {
                return BuTickets.Buscar(ticket);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region ABC Tickets
        /// <summary>
        /// Inserta ticket recibido desde WS
        /// </summary>
        /// <param name="no_ticket"></param>
        /// <param name="id_sistema"></param>
        /// <param name="nombre_sistema"></param>
        /// <param name="fecha_ticket"></param>
        /// <param name="importe_ticket"></param>
        /// <param name="forma_pago"></param>
        /// <param name="no_tarjeta"></param>
        /// <returns></returns>
        public bool insertaTicket(string no_ticket, string id_sistema, string nombre_sistema, DateTime fecha_ticket, decimal importe_ticket,
            string forma_pago, string no_tarjeta, DateTime fecha_carga)
        {
            Tickets ticket = new Tickets();
            ticket.No_Ticket = no_ticket;
            ticket.ID_Sistema = id_sistema;
            ticket.Nombre_Sistema = nombre_sistema;
            ticket.Fecha_Ticket = fecha_ticket;
            ticket.Importe_Total = importe_ticket;
            ticket.Forma_Pago = forma_pago;
            ticket.No_Tarjeta = no_tarjeta;
            ticket.Fecha_Carga = fecha_carga;
            try
            {
                return BuTickets.Insertar(ticket);
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Actualiza Tickets recibidos desde el WS
        /// </summary>
        /// <param name="no_ticket"></param>
        /// <param name="fecha_ticket"></param>
        /// <param name="importe_ticket"></param>
        /// <param name="forma_pago"></param>
        /// <param name="no_tarjeta"></param>
        /// <returns></returns>
        public bool actualizaTicket(string no_ticket, DateTime fecha_ticket, decimal importe_ticket, string forma_pago, string no_tarjeta)
        {
            Tickets ticket = new Tickets();
            ticket.No_Ticket = no_ticket;
            ticket.Fecha_Ticket = fecha_ticket;
            ticket.Importe_Total = importe_ticket;
            ticket.Forma_Pago = forma_pago;
            ticket.No_Tarjeta = no_tarjeta;
            try
            {
                return BuTickets.Actualizar(ticket);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region DAL
        public DataTable buscaEstacionamientos()
        {
            DataTable dtBEst = new DataTable();
            dtBEst = null;
            try
            {
                dtBEst = DALTickets_Varios.DALBuscaEstacionamiento();

                return dtBEst;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable buscaFormaPago()
        {
            DataTable dtBFP = new DataTable();
            dtBFP = null;
            try
            {
                dtBFP = DALTickets_Varios.DALBuscaFormaPago();

                return dtBFP;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable buscaTickets(DateTime fini, DateTime ffin, string estaciona, string formapago)
        {
            DataTable dtBTickets = new DataTable();
            dtBTickets = null;
            try
            {
                dtBTickets = DALTickets_Varios.DALBuscaTickets(fini, ffin, estaciona, formapago);

                return dtBTickets;
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

    }
}
