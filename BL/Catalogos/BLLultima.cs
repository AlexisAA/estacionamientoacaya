﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Bean.Catalogos;
using System.Data;
using DAL.DataAccess.Catalogos;
using System.Data.Common;

namespace BL.Catalogos
{
    public class BLLultima
    {
        public DataTable buscaultima()
        {
            DataTable dtBultimo = new DataTable();
            dtBultimo = null;
            try
            {
                dtBultimo = DALultimo1.DALultimos();

                return dtBultimo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
