﻿using System;
using System.Collections.Generic;

using DAL.Bean.Catalogos;

namespace BL.Catalogos
{
    public class BLUsuarios
    {

        #region BuscaUsuario
        /// <summary>
        /// Metodo para buscar usuario que se conecta desde el Web Service
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public List<Usuarios> buscaUsuarioWS(string usuario)
        {
            Usuarios user = new Usuarios();
            user.Usuario = usuario;
            try
            {
                return BuUsuarios.Buscar(user);
            }
            catch(Exception ex) { throw ex; }
        }

        public List<Usuarios> buscaUsuario(string usuario)
        {
            Usuarios user = new Usuarios();
            user.Usuario = usuario;
            try
            {
                return BuUsuarios.Buscar(user);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion


    }
}
