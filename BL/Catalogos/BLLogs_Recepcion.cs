﻿using System;
using System.Collections.Generic;

using DAL.Bean.Catalogos;

namespace BL.Catalogos
{
    public class BLLogs_Recepcion
    {

        #region ABC Logs_Recepcion
        /// <summary>
        /// Inserta Log de recepción de tickets
        /// </summary>
        /// <param name="id_usuario"></param>
        /// <param name="fecha"></param>
        /// <param name="reg_recibidos"></param>
        /// <param name="reg_insertados"></param>
        /// <param name="observaciones"></param>
        /// <returns></returns>
        public bool insertaLog_Recepcion(int id_usuario, DateTime fecha, int reg_recibidos, int reg_insertados, int reg_actualizados,
            int reg_error, string observaciones)
        {
            Logs_Recepcion lr = new Logs_Recepcion();
            lr.Id_Usuario = id_usuario;
            lr.Fecha = fecha;
            lr.Reg_Recibidos = reg_recibidos;
            lr.Reg_Insertados = reg_insertados;
            lr.Reg_Actualizados = reg_actualizados;
            lr.Reg_Error = reg_error;
            lr.Observaciones = observaciones;
            try
            {
                return BuLogs_Recepcion.Insertar(lr);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

    }
}
