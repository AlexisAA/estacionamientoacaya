﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Bean.Catalogos;
using System.Data;
using DAL.DataAccess.Catalogos;
using System.Data.Common;

namespace BL.Catalogos
{
    public class BLestados
    {
        public DataTable buscaestados()
        {
            DataTable dtBEst = new DataTable();
            dtBEst = null;
            try
            {
                dtBEst = DALLestados1.DALestad();
                
                //dtBEst = DALTickets_Varios.DALBuscaEstacionamiento();

                return dtBEst;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable buscmuni(int estado)
        {
            DataTable dtBEst = new DataTable();
            dtBEst = null;
            try
            {
                dtBEst = DALLestados1.DALmunic(estado);

                return dtBEst;
            }
            catch (Exception ex) { throw ex; }
        }

        public DataTable busclocalidad(int municipio)
        {
            DataTable dtBEst = new DataTable();
            dtBEst = null;
            try
            {
                dtBEst = DALLestados1.DALlocal(municipio);

                return dtBEst;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
