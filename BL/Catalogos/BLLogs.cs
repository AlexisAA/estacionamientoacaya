﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Bean.Catalogos;
using System.Data;
using DAL.DataAccess.Catalogos;

namespace BL.Catalogos
{
    public class BLLogs
    {

        /// <summary>
        /// Metodo para insertar log
        /// </summary>
        /// <param name="Formulario"></param>
        /// <param name="Evento"></param>
        /// <param name="Metodo"></param>
        /// <param name="Fecha"></param>
        /// <param name="Respuesta"></param>
        /// <param name="Descripcion"></param>
        /// <param name="EsSistema"></param>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public bool insertaLog(string Formulario, string Evento, string Metodo, DateTime Fecha, string Respuesta, string Descripcion,
            bool EsSistema, int Id_Usuario)
        {
            Logs log = new Logs();
            log.Formulario = Formulario;
            log.Evento = Evento;
            log.Metodo = Metodo;
            log.Fecha = Fecha;
            log.Respuesta = Respuesta;
            log.Descripcion = Descripcion;
            log.EsSistema = EsSistema;
            log.ID_Usuario = Id_Usuario;
            try
            {
                return BULogs.Insertar(log);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #region DALLogs_Varios
        ///// <summary>
        ///// Metodo para buscar formularios
        ///// </summary>
        ///// <returns></returns>
        //public DataTable buscaFormulario()
        //{
        //    DataTable dtBLFormulario = new DataTable();
        //    dtBLFormulario = null;
        //    try
        //    {
        //        dtBLFormulario = DALLogs_Varios.DALbuscaFormulario();

        //        return dtBLFormulario;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="formulario"></param>
        ///// <returns></returns>
        //public DataTable buscaEvento(string formulario)
        //{
        //    DataTable dtBLEvento = new DataTable();
        //    dtBLEvento = null;
        //    try
        //    {
        //        dtBLEvento = DALLogs_Varios.DALbuscaEvento(formulario);

        //        return dtBLEvento;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="formulario"></param>
        ///// <param name="evento"></param>
        ///// <returns></returns>
        //public DataTable buscaMetodo(string formulario, string evento)
        //{
        //    DataTable dtBLMetodo = new DataTable();
        //    dtBLMetodo = null;
        //    try
        //    {
        //        dtBLMetodo = DALLogs_Varios.DALbuscaMetodo(formulario, evento);

        //        return dtBLMetodo;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="formulario"></param>
        ///// <param name="evento"></param>
        ///// <param name="metodo"></param>
        ///// <returns></returns>
        //public DataTable buscaUsuario(string formulario, string evento, string metodo)
        //{
        //    DataTable dtBLUsuario = new DataTable();
        //    dtBLUsuario = null;
        //    try
        //    {
        //        dtBLUsuario = DALLogs_Varios.DALbuscaUsuario(formulario, evento, metodo);

        //        return dtBLUsuario;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="formulario"></param>
        ///// <param name="evento"></param>
        ///// <param name="metodo"></param>
        ///// <param name="finicio"></param>
        ///// <param name="ffin"></param>
        ///// <param name="idusuario"></param>
        ///// <returns></returns>
        //public DataTable buscaLog(string formulario, string evento, string metodo, DateTime finicio, DateTime ffin,
        //    int idusuario)
        //{
        //    DataTable dtBLLog = new DataTable();
        //    dtBLLog = null;
        //    try
        //    {
        //        dtBLLog = DALLogs_Varios.DALbuscaLog(formulario, evento, metodo, finicio, ffin, idusuario);

        //        return dtBLLog;
        //    }
        //    catch (Exception ex) { throw ex; }
        //}
        #endregion

    }
}
