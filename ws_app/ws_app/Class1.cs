﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using System.Data.OleDb;
using System.Globalization;
/// <summary>
/// Descripción breve de Class1
/// </summary>
public class Class1
{
    public Class1()
    {
       

    }

    public DataTable Conectar(string sql)
    {
        try
        {
            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());
            SqlCommand cmd = new SqlCommand(sql, cadena);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;
            cadena.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            cadena.Close();
            return dt;
        }
        catch (Exception ex)
        {
            String script = "<script>alert('" + ex.Message + "')</script>";
            System.Web.HttpContext.Current.Response.Write(script);
            return null;
        }
    }

    public bool Actualiza(string sql)
    {
        bool resul = false;
        try
        {
            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());

            cadena.Open();
            SqlCommand cmd = cadena.CreateCommand();
            cmd.CommandText = sql;      
            cmd.ExecuteNonQuery();

            resul= true;
        }
        catch {
            resul= false;
        }
        return resul;
    }

    public bool insertafacturados(string ticket,int factura,int valido)
    {
        bool resul = false;
        string sql = "";
        try
        {
            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());

            sql = "Insert into TBL_Tickets_Facturados(No_Ticket,ID_Factura,Valido) Values('" + ticket + "'," + factura + "," + valido + ")";

            cadena.Open();
            SqlCommand cmd = cadena.CreateCommand();
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();

            resul = true;
            cadena.Close();
        }
        catch
        {
            resul = false;
        }
        return resul;
    }

    public bool buscar(string sql)
    {
        bool regresa = false;
        try
        {
            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());
            SqlCommand cmd = new SqlCommand(sql, cadena);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;
            cadena.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            cadena.Close();
            if (dt.Rows.Count > 0)
            {
                regresa = true;
            }
            else {
                regresa = false;
            }

            return regresa;
        }
        catch (Exception ex)
        {
            String script = "<script>alert('" + ex.Message + "')</script>";
            System.Web.HttpContext.Current.Response.Write(script);
            return false;
        }
    }
}