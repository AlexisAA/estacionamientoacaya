﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ws_app
{
    /// <summary>
    /// Descripción breve de wsapp
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class wsapp : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string login(string imei, string pass)
        {
            string respuesta = string.Empty;
            seguridad desc = new seguridad();

            string passdesencripta = desc.encriptar(pass);
            Class1 busca = new Class1();
            bool sino = busca.buscar("Select * from TBL_Usuarios where imei ='"+imei+"' and contrasenia ='" + passdesencripta + "'");
            if (sino)
            {
                respuesta = "si";
            }
            else
            {
                respuesta = "no";
            }

            return respuesta;
        }
    }
}
