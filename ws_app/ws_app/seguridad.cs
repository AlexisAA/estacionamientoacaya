﻿using System;
using System.Text;

using System.Security.Cryptography;


namespace ws_app
{
    public class seguridad
    {
        /// <summary> Llave estatica para la Encripcion / Desencripcion.</summary>
        private static string strKey = "keyFacturacionEspacia";

        /// <summary> Encriptamos un String</summary>
        /// <param name="texto"></param>
        /// <returns>String encriptado</returns>
        public string encriptar(string texto)
        {
            //Arreglo de bytes donde guardaremos la llave
            byte[] keyArray;

            //Arreglo de bytes donde guardaresmos el texto que vamos a encriiptar
            byte[] arrayaCifrar = UTF8Encoding.UTF8.GetBytes(texto);

            //Se utilizan las clases de encriptación provistas por el Framework Algoritmo MD5
            MD5CryptoServiceProvider hashMd5 = new MD5CryptoServiceProvider();

            //Se guarda la llave para que se le realice hashing
            keyArray = hashMd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(strKey));

            hashMd5.Clear();

            //Algoritmo 3DAS
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            //Se empieza con la transformación de la cadena
            ICryptoTransform cTransform = tdes.CreateEncryptor();

            //Arreglo de bytes donde se guarda la cadena cifrada
            byte[] arrayResultado = cTransform.TransformFinalBlock(arrayaCifrar, 0, arrayaCifrar.Length);

            tdes.Clear();

            //Se regresa el resultado en forma de una cadea
            return Convert.ToBase64String(arrayResultado, 0, arrayResultado.Length);

        }

        /// <summary> Desencriptamos un String</summary>
        /// <param name="textoEncriptado"></param>
        /// <returns>String desencriptado</returns>
        public string desencriptar(string textoEncriptado)
        {
            byte[] keyArray;

            //convierte el texto en una secuencia de bytes
            byte[] Array_a_Descifrar = Convert.FromBase64String(textoEncriptado);


            //Se llama a las clases que tienen los algoritmos de encriptación se le aplica hashing algoritmo MD5
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(strKey));

            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();

            byte[] resultArray = cTransform.TransformFinalBlock(Array_a_Descifrar, 0, Array_a_Descifrar.Length);

            tdes.Clear();
            //se regresa en forma de cadena
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}