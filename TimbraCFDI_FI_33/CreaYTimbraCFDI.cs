﻿using BL.Catalogos;
using BL.Otros;
using BL.Portal;
using DAL.Bean.Portal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TimbraCFDI_FI_33
{
    public class CreaYTimbraCFDI
    {
        private string Folio = "";
        public string claveproducto = ConfigurationManager.AppSettings["ClaveProducto"].ToString();
        public string ClaveUnidad = ConfigurationManager.AppSettings["ClaveUnidad"].ToString();
        public string UsoCFDI = ConfigurationManager.AppSettings["UsoCFDI"].ToString();
        
        public CreaYTimbraCFDI()
        {
            Folio = "";
        }

        /// <summary>
        /// CreaTimbraCFDI
        /// </summary>
        /// <param name="dtticket"></param>
        /// <param name="dtahora"></param>
        /// <param name="tneto"></param>
        /// <param name="total"></param>
        /// <param name="metododepago"></param>
        /// <param name="rfc"></param>
        /// <param name="nombre_razons"></param>
        /// <returns></returns>
        /// 
        public string[] CreaTimbraCFDI(DataTable dtticket, string rfc, string nombre_razons, int idCliente, string Origen_Factura, string correo, bool esProduccion, bool EsCliente,string usoC)
        {
            string cadena01 = "";
            string cadena02 = "";
            string cadena03 = "";
            string cadena04A = "";
            string[] cadenaconcatenada = new string[4];
            string[] strConceptos = new string[5];

            try
            {
                if (rfc != "XAXX010101000")
                {
                    if (EsCliente)
                    {
                        claveproducto = "";
                        claveproducto = ConfigurationManager.AppSettings["ClaveProductoClientes"].ToString();
                    }
                }

                strConceptos = CadenaConceptos(dtticket);

                if (strConceptos != null && strConceptos[0] != "Error")
                {
                    Folio = "";

                    #region Asignacion y armado de cadena 03: Conceptos
                    decimal TNeto = 0;
                    decimal TImpuesto = 0;
                    decimal Total = 0;
                    string MetododePago = "";

                    TNeto = Convert.ToDecimal(strConceptos[1].ToString());
                    TImpuesto = Convert.ToDecimal(strConceptos[2].ToString());
                    Total = TNeto + TImpuesto;
                    MetododePago = strConceptos[3].ToString();

                    cadena03 = strConceptos[0].ToString();
                    #endregion

                    #region  Asignacion y armado de cadena 01: Datos CFDI
                    DateTime dtAhora = new DateTime();
                    dtAhora = DateTime.Now.AddMinutes(-5);
                    cadena01 = CadenaDatosCFDI(dtAhora, TNeto, Total, MetododePago);

                    if (string.IsNullOrEmpty(cadena01))
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se genero la cadena01";
                        cadenaconcatenada[2] = "Error en el metodo CadenaDatosCFDI.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region Asignacion y armado de cadena 02: Datos Receptor
                    //cadena02 = CadenaDatosReceptor(rfc, nombre_razons, false);
                    cadena02 = CadenaDatosReceptorLayOut(rfc, nombre_razons, false,usoC);
                    #endregion

                    #region Asignacion y armado de cadena 04A: Traslados
                    cadena04A = Cadena04A(TImpuesto);
                    #endregion

                    #region Asigancion para Timbrar
                    string userPAC = "";
                    string passPAC = "";

                    userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                    passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();

                    WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();
                    WS_FI_33.Peticion ws_fi_pet = new WS_FI_33.Peticion();

                    ws_fi_pet.rfcEmisor = ConfigurationManager.AppSettings["RFCEmisor"].ToString();
                    ws_fi_pet.tipoCFDI = 1;
                    ws_fi_pet.cadenaTXT = cadena01 + cadena02 + cadena03 + cadena04A;
                    ws_fi_pet.usuario = userPAC;
                    ws_fi_pet.contrasena = passPAC;
                    ws_fi_pet.productivo = esProduccion;
                    ws_fi_pet.pdf = true;

                    //BLEnviaCorreo blec_123 = new BLEnviaCorreo();

                    //blec_123.EnviaLayOut("sertomy_06@hotmail.com",ws_fi_pet.cadenaTXT);

                    WS_FI_33.Respuesta ws_fi_res = new WS_FI_33.Respuesta();

                    ws_fi_res = ws_fi_v33.TimbrarTxt(ws_fi_pet);
                    #endregion

                    #region Repuesta del Timbrado
                    if (ws_fi_res.exito)
                    {
                        if (!string.IsNullOrEmpty(ws_fi_res.uuid))
                        {
                            string UUID = "";

                            UUID = ws_fi_res.uuid;

                            if (!string.IsNullOrEmpty(ws_fi_res.xmlTimbrado))
                            {
                                byte[] archivoXML = null;
                                byte[] archivoPDF = null;
                                string respenv = "";

                                if (esProduccion)
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        respenv = "El PAC no entrego el archivo PDF, Verificar con el PAC. UUID: " + UUID;
                                        insertaLog("btnFacturar", "ObtenerPDF-PAC", "Error", respenv, true);
                                    }
                                }
                                else
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        string _urlpdf = ConfigurationManager.AppSettings["PDF_Prueba"].ToString();

                                        archivoPDF = convierteaBits(_urlpdf);
                                    }
                                }

                                archivoXML = Encoding.UTF8.GetBytes(ws_fi_res.xmlTimbrado);

                                #region Guarda Factura
                                bool respIns = false;
                                int ID_Cliente = 0;
                                string _Correo = "";
                                string Serie = "";
                                string Descripcion = "";
                                string Nombre_XML = "";
                                string Nombre_PDF = "";
                                string ID_Origen_Factura = "";
                                bool Valido = true;

                                ID_Cliente = idCliente;
                                _Correo = correo;
                                Descripcion = strConceptos[4].ToString();
                                Nombre_XML = rfc.Trim() + "_" + UUID + ".xml";
                                Nombre_PDF = rfc.Trim() + "_" + UUID + ".pdf";
                                ID_Origen_Factura = Origen_Factura;

                                try
                                {
                                    respIns = GuardaFactura(ID_Cliente, UUID, dtAhora, Total, Folio, Serie,
                                        Descripcion, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF,
                                        ID_Origen_Factura, Valido);

                                    if (respIns)
                                    {
                                        int ID_Factura = 0;
                                        ID_Factura = InsertaTicketenFactura(ID_Cliente, UUID, dtticket);

                                        #region Almacena Factura
                                        respIns = AlmacenaFactura(dtAhora, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF);

                                        if (respIns)
                                        {
                                            #region Guarda Correo

                                            respIns = GuardaCorreo(ID_Cliente, _Correo.Trim());

                                            if (respIns)
                                            {
                                                #region Envía Correo
                                                string pathBobeda = "";
                                                string pathAnio = "";
                                                string pathMes = "";
                                                string pathDia = "";
                                                string path = "";
                                                string Anio = "";
                                                string Mes = "";
                                                string Dia = "";

                                                Anio = dtAhora.Year.ToString();
                                                Mes = dtAhora.Month.ToString();
                                                Dia = dtAhora.Day.ToString();

                                                Mes = Mes.Length > 1 ? Mes : "0" + Mes;
                                                Dia = Dia.Length > 1 ? Dia : "0" + Dia;

                                                pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
                                                pathAnio = pathBobeda + "/" + Anio;
                                                pathMes = pathBobeda + "/" + Anio + "/" + Mes;
                                                pathDia = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia;
                                                path = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia + "/";

                                                BLEnviaCorreo blec = new BLEnviaCorreo();
                                                try
                                                {
                                                    if (archivoPDF == null)
                                                        Nombre_PDF = null;

                                                    respenv = blec.EnviaFcturas(_Correo.Trim(), path, Nombre_XML, Nombre_PDF);

                                                    if (respenv == "Ok")
                                                    {
                                                        if (respenv != "")
                                                            respenv = "La factura ya fue envíada a su correo";
                                                        else
                                                            respenv = respenv + ". La factura ya fue envíada a su correo";
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    insertaLog("btnFacturar", "EnviaFcturas", "Error", ex.Message, true);
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                if (respenv != "")
                                                    respenv = "La factura ya fue envíada a su correo";
                                                else
                                                    respenv = respenv + ". No se pudo guardar correo. Por favor descargue las facturas";
                                            }
                                            #endregion

                                            string x = "";
                                            x = respenv + ", Factura: " + ID_Factura;
                                            insertaLog("btnFacturar", "GuardaCorreo", "Ok", x, false);

                                            #region Arma y codifica la cadena para pasar la información a Factura
                                            BLCodifica blCodifica = new BLCodifica();
                                            string pasaCadena = "";
                                            string codificaCadena = "";
                                            pasaCadena = ID_Cliente.ToString() + "|" + ID_Factura.ToString() + "|" + respenv;
                                            codificaCadena = blCodifica.encriptar(pasaCadena);
                                            //Session["Key"] = codificaCadena.ToString();

                                            cadenaconcatenada[0] = "Ok";
                                            cadenaconcatenada[1] = codificaCadena.ToString();
                                            cadenaconcatenada[2] = "Timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                            #endregion
                                        }
                                        else
                                        {
                                            cadenaconcatenada[0] = "Error";
                                            cadenaconcatenada[1] = "No se pudo guardar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                            cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        cadenaconcatenada[0] = "Error";
                                        cadenaconcatenada[1] = "No guardo la factura en BD.";
                                        cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);
                                    cadenaconcatenada[0] = "Error";
                                    cadenaconcatenada[1] = ex.Message;
                                    cadenaconcatenada[2] = ex.InnerException.ToString() + ". Pero Si timbro la factura correctamente, anexo UUID: " + UUID; ;
                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    Folio = "";

                                    return cadenaconcatenada;
                                }
                                #endregion
                            }
                            else
                            {
                                insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID, false);

                                cadenaconcatenada[0] = "Error";
                                cadenaconcatenada[1] = "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID;
                                cadenaconcatenada[2] = "Verificar con el PAC";
                                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                            }
                        }
                        else
                        {
                            insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener UUID, el PAC no lo proporciono.", false);

                            cadenaconcatenada[0] = "Error";
                            cadenaconcatenada[1] = "No se pudo Obtener UUID el PAC no lo proporciono";
                            cadenaconcatenada[2] = "Verificar con el PAC";
                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                        }
                    }
                    else
                    {
                        string allError = "";

                        allError = ws_fi_res.errorEspecifico + ", " + ws_fi_res.errorGeneral;

                        insertaLog("btnFacturar", "TimbrarTxT", "Error", allError, false);

                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = ws_fi_res.errorEspecifico;
                        cadenaconcatenada[2] = ws_fi_res.errorGeneral;
                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                    }
                    #endregion
                }
                else
                {
                    cadenaconcatenada[0] = strConceptos[0];
                    cadenaconcatenada[1] = strConceptos[1];
                    cadenaconcatenada[2] = strConceptos[2];
                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                Folio = "";
            }

            Folio = "";
            return cadenaconcatenada;
        }
        
        public string[] CreaTimbraCFDI_CargaIngresos(DataTable dtIngresos, string Origen_Factura, string nombreArchivo, bool esProduccion)
        {
            string usoC = "";
            string cadena01 = "";
            string cadena02 = "";
            string cadena03 = "";
            string cadena04A = "";
            string[] cadenaconcatenada = new string[4];
            string[] strConceptos = new string[10];
            string rfc = "";
            Clientes _cliente = new Clientes();

            try
            {
                //usoC = dtIngresos.Rows[0].Field<string>("USO_CFDI");
                usoC = Convert.ToString(ConfigurationManager.AppSettings["USOCFDI"]);

                strConceptos = CadenaConceptosCargaIngresos(dtIngresos);

                if (strConceptos != null && strConceptos[0] != "Error")
                {
                    Folio = "";

                    #region Asignacion y armado de cadena 03: Conceptos
                    decimal TNeto = 0;
                    decimal TImpuesto = 0;
                    decimal Total = 0;
                    string MetododePago = "";
                    string FormadePago = "01";
                    string claveunidad = string.Empty;
                    string razonsocial = string.Empty;

                    cadena03 = strConceptos[0].ToString();

                    TNeto = Convert.ToDecimal(strConceptos[1].ToString());
                    TImpuesto = Convert.ToDecimal(strConceptos[2].ToString());
                    Total = Convert.ToDecimal(strConceptos[3].ToString());//TNeto + TImpuesto;
                    //MetododePago = "PUE";
                    MetododePago = strConceptos[6].ToString();
                    FormadePago = strConceptos[7].ToString();
                    claveunidad = strConceptos[8].ToString();
                    razonsocial = strConceptos[9].ToString();

                    rfc = strConceptos[4].ToString();

                    if(!string.IsNullOrEmpty(rfc))
                    {
                        _cliente = BuscaCliente(rfc);

                        if(_cliente == null)
                        {
                            //cadenaconcatenada[0] = "Error";
                            //cadenaconcatenada[1] = "No se encontro el Cliente con RFC: " + rfc;
                            //cadenaconcatenada[2] = "Error: RFC invalido.";
                            //cadenaconcatenada[3] = "";
                            //Folio = "";
                            string sql_ic = "Insert into TBL_Clientes(Rfc,Razon_Social) Values(@Rfc,@Razon_Social)";
                            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());
                            cadena.Open();
                            SqlCommand cmd = cadena.CreateCommand();
                            cmd.CommandText = sql_ic;
                            cmd.Parameters.AddWithValue("@Rfc", rfc);
                            cmd.Parameters.AddWithValue("@Razon_Social", razonsocial);
                            cmd.ExecuteNonQuery();

                            cadenaconcatenada[0] = strConceptos[0];
                            cadenaconcatenada[1] = strConceptos[1];
                            cadenaconcatenada[2] = strConceptos[2];
                            cadenaconcatenada[3] = "RFC: " + rfc;

                            return cadenaconcatenada;
                        }
                    }
                    else
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se encontro el Cliente.";
                        cadenaconcatenada[2] = "Error: RFC invalido.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region  Asignacion y armado de cadena 01: Datos CFDI
                    DateTime dtAhora = new DateTime();
                    dtAhora = DateTime.Now.AddMinutes(-5);
                    //cadena01 = CadenaDatosCFDI(dtAhora, TNeto, Total, MetododePago);
                    cadena01 = CadenaDatosCFDI_lay(dtAhora, TNeto, Total, MetododePago, FormadePago);
                    
                    if (string.IsNullOrEmpty(cadena01))
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se genero la cadena01";
                        cadenaconcatenada[2] = "Error en el metodo CadenaDatosCFDI.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region Asignacion y armado de cadena 02: Datos Receptor
                    cadena02 = CadenaDatosReceptorLayOut(rfc, _cliente.Razon_social, false, usoC);
                    #endregion

                    #region Asignacion y armado de cadena 04A: Traslados
                    //cadena04A = Cadena04A(Total);
                    cadena04A = Cadena04A(TImpuesto);
                    #endregion

                    #region Asigancion para Timbrar
                    string userPAC = "";
                    string passPAC = "";

                    userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                    passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();

                    WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();
                    WS_FI_33.Peticion ws_fi_pet = new WS_FI_33.Peticion();

                    ws_fi_pet.rfcEmisor = ConfigurationManager.AppSettings["RFCEmisor"].ToString();
                    ws_fi_pet.cadenaTXT = cadena01 + cadena02 + cadena03 + cadena04A;
                    ws_fi_pet.usuario = userPAC;
                    ws_fi_pet.contrasena = passPAC;
                    ws_fi_pet.productivo = esProduccion;
                    ws_fi_pet.pdf = true;
                    ws_fi_pet.tipoCFDI = 1;

                    WS_FI_33.Respuesta ws_fi_res = new WS_FI_33.Respuesta();

                    ws_fi_res = ws_fi_v33.TimbrarTxt(ws_fi_pet);
                    #endregion

                    #region Repuesta del Timbrado
                    if (ws_fi_res.exito)
                    {
                        if (!string.IsNullOrEmpty(ws_fi_res.uuid))
                        {
                            string UUID = "";

                            UUID = ws_fi_res.uuid;

                            if (!string.IsNullOrEmpty(ws_fi_res.xmlTimbrado))
                            {
                                byte[] archivoXML = null;
                                byte[] archivoPDF = null;
                                string respenv = "";

                                if (esProduccion)
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        respenv = "El PAC no entrego el archivo PDF, Verificar con el PAC. UUID: " + UUID;
                                        insertaLog("btnFacturar", "ObtenerPDF-PAC", "Error", respenv, true);
                                    }
                                }
                                else
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        string _urlpdf = ConfigurationManager.AppSettings["PDF_Prueba"].ToString();

                                        archivoPDF = convierteaBits(_urlpdf);
                                    }
                                }

                                archivoXML = Encoding.UTF8.GetBytes(ws_fi_res.xmlTimbrado);

                                #region Guarda Factura
                                bool respIns = false;
                                int ID_Cliente = 0;
                                string _Correo = "";
                                string Serie = "";
                                string Descripcion = "";
                                string Nombre_XML = "";
                                string Nombre_PDF = "";
                                string ID_Origen_Factura = "";
                                bool Valido = true;

                                ID_Cliente = _cliente.ID_Cliente;
                                _Correo = strConceptos[5].ToString();
                                if (Origen_Factura == "G")
                                {
                                    Descripcion = "Facturación Global";
                                }
                                else {
                                    Descripcion = "Facturación de archivo: " + nombreArchivo;
                                }
                                
                                Nombre_XML = rfc.Trim() + "_" + UUID + ".xml";
                                Nombre_PDF = rfc.Trim() + "_" + UUID + ".pdf";
                                ID_Origen_Factura = Origen_Factura;

                                try
                                {
                                    respIns = GuardaFactura(ID_Cliente, UUID, dtAhora, Total, Folio, Serie,
                                        Descripcion, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF,
                                        ID_Origen_Factura, Valido);

                                    if (respIns)
                                    {
                                        int ID_Factura = 0;
                                        ID_Factura = InsertaRegistroFacturado(ID_Cliente, UUID, dtIngresos, _Correo);

                                        #region Almacena Factura
                                        respIns = AlmacenaFactura(dtAhora, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF);

                                        if (respIns)
                                        {
                                            #region Envía Correo
                                            string pathBobeda = "";
                                            string pathAnio = "";
                                            string pathMes = "";
                                            string pathDia = "";
                                            string path = "";
                                            string Anio = "";
                                            string Mes = "";
                                            string Dia = "";

                                            Anio = dtAhora.Year.ToString();
                                            Mes = dtAhora.Month.ToString();
                                            Dia = dtAhora.Day.ToString();

                                            Mes = Mes.Length > 1 ? Mes : "0" + Mes;
                                            Dia = Dia.Length > 1 ? Dia : "0" + Dia;

                                            pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
                                            pathAnio = pathBobeda + "/" + Anio;
                                            pathMes = pathBobeda + "/" + Anio + "/" + Mes;
                                            pathDia = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia;
                                            path = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia + "/";
                                            
                                            BLEnviaCorreo blec = new BLEnviaCorreo();
                                            try
                                            {
                                                if (archivoPDF == null)
                                                    Nombre_PDF = null;

                                                respenv = blec.EnviaFcturas(_Correo.Trim(), path, Nombre_XML, Nombre_PDF);

                                                if (respenv == "Ok")
                                                {
                                                    respenv = "La factura ya fue envíada a su correo. Factura : " + ID_Factura;
                                                    insertaLog("btnFacturar", "EnviaFcturas", "Ok", respenv, false);

                                                    cadenaconcatenada[0] = "Ok";
                                                    cadenaconcatenada[1] = respenv;
                                                    cadenaconcatenada[2] = "Timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;

                                                }
                                                else
                                                {
                                                    cadenaconcatenada[0] = "Error";
                                                    cadenaconcatenada[1] = "No se pudo enviar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                                    cadenaconcatenada[2] = "Verifique el metodo EnviaFacturas. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                insertaLog("btnFacturar", "EnviaFcturas", "Error", ex.Message, true);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            cadenaconcatenada[0] = "Error";
                                            cadenaconcatenada[1] = "No se pudo guardar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                            cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        cadenaconcatenada[0] = "Error";
                                        cadenaconcatenada[1] = "No guardo la factura en BD.";
                                        cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);
                                    cadenaconcatenada[0] = "Error";
                                    cadenaconcatenada[1] = ex.Message;
                                    cadenaconcatenada[2] = ex.InnerException.ToString() + ". Pero Si timbro la factura correctamente, anexo UUID: " + UUID; ;
                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                    Folio = "";

                                    return cadenaconcatenada;
                                }
                                #endregion
                            }
                            else
                            {
                                insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID, false);

                                cadenaconcatenada[0] = "Error";
                                cadenaconcatenada[1] = "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID;
                                cadenaconcatenada[2] = "Verificar con el PAC";
                                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                            }
                        }
                        else
                        {
                            insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener UUID, el PAC no lo proporciono.", false);

                            cadenaconcatenada[0] = "Error";
                            cadenaconcatenada[1] = "No se pudo Obtener UUID el PAC no lo proporciono";
                            cadenaconcatenada[2] = "Verificar con el PAC";
                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                        }
                    }
                    else
                    {
                        string allError = "";

                        allError = ws_fi_res.errorEspecifico + ", " + ws_fi_res.errorGeneral;

                        insertaLog("btnFacturar", "TimbrarTxT", "Error", allError, false);

                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = ws_fi_res.errorEspecifico;
                        cadenaconcatenada[2] = ws_fi_res.errorGeneral;
                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                    }
                    #endregion
                }
                else
                {
                    cadenaconcatenada[0] = strConceptos[0];
                    cadenaconcatenada[1] = strConceptos[1];
                    cadenaconcatenada[2] = strConceptos[2];
                    cadenaconcatenada[3] = "RFC: " + rfc ;
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + rfc ;
                Folio = "";
            }

            Folio = "";
            return cadenaconcatenada;
        }

        public string[] CreaTimbraCFDI_FacturaTickets(DataTable dtticket, string rfc, string nombre_razons, string _observaciones,string Origen_Factura, bool esProduccion)
        {
            string cadena01 = "";
            string cadena02 = "";
            string cadena03 = "";
            string cadena04A = "";
            string[] cadenaconcatenada = new string[4];
            string[] strConceptos = new string[5];

            try
            {
                strConceptos = CadenaConceptos(dtticket);

                if (strConceptos != null && strConceptos[0] != "Error")
                {
                    Folio = "";

                    #region Asignacion y armado de cadena 03: Conceptos
                    decimal TNeto = 0;
                    decimal TImpuesto = 0;
                    decimal Total = 0;
                    string MetododePago = "";

                    TNeto = Convert.ToDecimal(strConceptos[1].ToString());
                    TImpuesto = Convert.ToDecimal(strConceptos[2].ToString());
                    Total = TNeto + TImpuesto;
                    MetododePago = strConceptos[3].ToString();

                    cadena03 = strConceptos[0].ToString();
                    #endregion

                    #region  Asignacion y armado de cadena 01: Datos CFDI
                    DateTime dtAhora = new DateTime();
                    dtAhora = DateTime.Now.AddMinutes(-5);
                    cadena01 = CadenaDatosCFDI(dtAhora, TNeto, Total, MetododePago);

                    if (string.IsNullOrEmpty(cadena01))
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se genero la cadena01";
                        cadenaconcatenada[2] = "Error en el metodo CadenaDatosCFDI.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region Asignacion y armado de cadena 02: Datos Receptor
                    cadena02 = CadenaDatosReceptor(rfc, nombre_razons, false);
                    #endregion

                    #region Asignacion y armado de cadena 04A: Traslados
                    cadena04A = Cadena04A(TImpuesto);
                    #endregion

                    #region Asigancion para Timbrar
                    string userPAC = "";
                    string passPAC = "";

                    userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                    passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();

                    WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();
                    WS_FI_33.Peticion ws_fi_pet = new WS_FI_33.Peticion();

                    ws_fi_pet.rfcEmisor = ConfigurationManager.AppSettings["RFCEmisor"].ToString(); 
                    ws_fi_pet.cadenaTXT = cadena01 + cadena02 + cadena03 + cadena04A;
                    ws_fi_pet.usuario = userPAC;
                    ws_fi_pet.contrasena = passPAC;
                    ws_fi_pet.productivo = esProduccion;
                    ws_fi_pet.pdf = true;
                    ws_fi_pet.tipoCFDI = 1;

                    WS_FI_33.Respuesta ws_fi_res = new WS_FI_33.Respuesta();

                    ws_fi_res = ws_fi_v33.TimbrarTxt(ws_fi_pet);
                    #endregion

                    #region Repuesta del Timbrado
                    if (ws_fi_res.exito)
                    {
                        if (!string.IsNullOrEmpty(ws_fi_res.uuid))
                        {
                            string UUID = "";

                            UUID = ws_fi_res.uuid;

                            if (!string.IsNullOrEmpty(ws_fi_res.xmlTimbrado))
                            {
                                byte[] archivoXML = null;
                                byte[] archivoPDF = null;
                                string respenv = "";

                                if (esProduccion)
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        respenv = "El PAC no entrego el archivo PDF, Verificar con el PAC. UUID: " + UUID;
                                        insertaLog("btnFacturar", "ObtenerPDF-PAC", "Error", respenv, true);
                                    }
                                }
                                else
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        string _urlpdf = ConfigurationManager.AppSettings["PDF_Prueba"].ToString();

                                        archivoPDF = convierteaBits(_urlpdf);
                                    }
                                }

                                archivoXML = Encoding.UTF8.GetBytes(ws_fi_res.xmlTimbrado);

                                #region Guarda Factura
                                bool respIns = false;
                                int ID_Cliente = 0;
                                string Serie = "";
                                string Descripcion = "";
                                string Nombre_XML = "";
                                string Nombre_PDF = "";
                                string ID_Origen_Factura = "";
                                bool Valido = true;

                                ID_Cliente = Convert.ToInt32(ConfigurationManager.AppSettings["CteGeneral"].ToString()); 
                                Descripcion = _observaciones;
                                Nombre_XML = rfc.Trim() + "_" + UUID + ".xml";
                                Nombre_PDF = rfc.Trim() + "_" + UUID + ".pdf";
                                ID_Origen_Factura = Origen_Factura;

                                try
                                {
                                    respIns = GuardaFactura(ID_Cliente, UUID, dtAhora, Total, Folio, Serie,
                                        Descripcion, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF,
                                        ID_Origen_Factura, Valido);

                                    if (respIns)
                                    {
                                        int ID_Factura = 0;
                                        ID_Factura = InsertaTicketenFactura(ID_Cliente, UUID, dtticket);

                                        #region Almacena Factura
                                        respIns = AlmacenaFactura(dtAhora, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF);

                                        if (respIns)
                                        {
                                            #region Envía Correo
                                            string pathBobeda = "";
                                            string pathAnio = "";
                                            string pathMes = "";
                                            string pathDia = "";
                                            string path = "";
                                            string Anio = "";
                                            string Mes = "";
                                            string Dia = "";
                                            string _Correo = "";

                                            Anio = dtAhora.Year.ToString();
                                            Mes = dtAhora.Month.ToString();
                                            Dia = dtAhora.Day.ToString();

                                            Mes = Mes.Length > 1 ? Mes : "0" + Mes;
                                            Dia = Dia.Length > 1 ? Dia : "0" + Dia;

                                            pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
                                            pathAnio = pathBobeda + "/" + Anio;
                                            pathMes = pathBobeda + "/" + Anio + "/" + Mes;
                                            pathDia = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia;
                                            path = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia + "/";

                                            BLEnviaCorreo blec = new BLEnviaCorreo();
                                            try
                                            {
                                                if (archivoPDF == null)
                                                    Nombre_PDF = null;

                                                _Correo = ConfigurationManager.AppSettings["mailCC"].ToString();

                                                respenv = blec.EnviaFcturas(_Correo.Trim(), path, Nombre_XML, Nombre_PDF);

                                                if (respenv == "Ok")
                                                {
                                                    respenv = "La factura ya fue envíada a su correo. Factura : " + ID_Factura;
                                                    insertaLog("btnFacturar", "EnviaFcturas", "Ok", respenv, false);

                                                    cadenaconcatenada[0] = "Ok";
                                                    cadenaconcatenada[1] = respenv;
                                                    cadenaconcatenada[2] = "Timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                                    
                                                }
                                                else
                                                {
                                                    cadenaconcatenada[0] = "Error";
                                                    cadenaconcatenada[1] = "No se pudo enviar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                                    cadenaconcatenada[2] = "Verifique el metodo EnviaFcturas. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                insertaLog("btnFacturar", "EnviaFcturas", "Error", ex.Message, true);
                                            }
                                            #endregion                                            
                                        }
                                        else
                                        {
                                            cadenaconcatenada[0] = "Error";
                                            cadenaconcatenada[1] = "No se pudo guardar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                            cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        cadenaconcatenada[0] = "Error";
                                        cadenaconcatenada[1] = "No guardo la factura en BD.";
                                        cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);
                                    cadenaconcatenada[0] = "Error";
                                    cadenaconcatenada[1] = ex.Message;
                                    cadenaconcatenada[2] = ex.InnerException.ToString() + ". Pero Si timbro la factura correctamente, anexo UUID: " + UUID; ;
                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    Folio = "";

                                    return cadenaconcatenada;
                                }
                                #endregion
                            }
                            else
                            {
                                insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID, false);

                                cadenaconcatenada[0] = "Error";
                                cadenaconcatenada[1] = "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID;
                                cadenaconcatenada[2] = "Verificar con el PAC";
                                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                            }
                        }
                        else
                        {
                            insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener UUID, el PAC no lo proporciono.", false);

                            cadenaconcatenada[0] = "Error";
                            cadenaconcatenada[1] = "No se pudo Obtener UUID el PAC no lo proporciono";
                            cadenaconcatenada[2] = "Verificar con el PAC";
                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                        }
                    }
                    else
                    {
                        string allError = "";

                        allError = ws_fi_res.errorEspecifico + ", " + ws_fi_res.errorGeneral;

                        insertaLog("btnFacturar", "TimbrarTxT", "Error", allError, false);

                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = ws_fi_res.errorEspecifico;
                        cadenaconcatenada[2] = ws_fi_res.errorGeneral;
                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                    }
                    #endregion
                }
                else
                {
                    cadenaconcatenada[0] = strConceptos[0];
                    cadenaconcatenada[1] = strConceptos[1];
                    cadenaconcatenada[2] = strConceptos[2];
                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                Folio = "";
            }

            Folio = "";
            return cadenaconcatenada;
        }

        public string[] CreaTimbraCFDIWebService(DataTable dtticket, string rfc, string nombre_razons, int idCliente, string Origen_Factura, string correo, string _observaciones, bool esProduccion)
        {
            string cadena01 = "";
            string cadena02 = "";
            string cadena03 = "";
            string cadena04A = "";
            string[] cadenaconcatenada = new string[4];
            string[] strConceptos = new string[5];

            try
            {
                strConceptos = CadenaConceptos(dtticket);

                if (strConceptos != null && strConceptos[0] != "Error")
                {
                    Folio = "";

                    #region Asignacion y armado de cadena 03: Conceptos
                    decimal TNeto = 0;
                    decimal TImpuesto = 0;
                    decimal Total = 0;
                    string MetododePago = "";

                    TNeto = Convert.ToDecimal(strConceptos[1].ToString());
                    TImpuesto = Convert.ToDecimal(strConceptos[2].ToString());
                    Total = TNeto + TImpuesto;
                    MetododePago = strConceptos[3].ToString();

                    cadena03 = strConceptos[0].ToString();
                    #endregion

                    #region  Asignacion y armado de cadena 01: Datos CFDI
                    DateTime dtAhora = new DateTime();
                    dtAhora = DateTime.Now.AddMinutes(-5);
                    cadena01 = CadenaDatosCFDI(dtAhora, TNeto, Total, MetododePago);

                    if (string.IsNullOrEmpty(cadena01))
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se genero la cadena01";
                        cadenaconcatenada[2] = "Error en el metodo CadenaDatosCFDI.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region Asignacion y armado de cadena 02: Datos Receptor
                    cadena02 = CadenaDatosReceptor(rfc, nombre_razons, false);
                    #endregion

                    #region Asignacion y armado de cadena 04A: Traslados
                    cadena04A = Cadena04A(TImpuesto);
                    #endregion

                    #region Asigancion para Timbrar
                    string userPAC = "";
                    string passPAC = "";

                    userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                    passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();

                    WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();
                    WS_FI_33.Peticion ws_fi_pet = new WS_FI_33.Peticion();

                    ws_fi_pet.rfcEmisor = ConfigurationManager.AppSettings["RFCEmisor"].ToString(); 
                    ws_fi_pet.cadenaTXT = cadena01 + cadena02 + cadena03 + cadena04A;
                    ws_fi_pet.usuario = userPAC;
                    ws_fi_pet.contrasena = passPAC;
                    ws_fi_pet.productivo = esProduccion;
                    ws_fi_pet.pdf = true;
                    ws_fi_pet.tipoCFDI = 1;

                    WS_FI_33.Respuesta ws_fi_res = new WS_FI_33.Respuesta();

                    ws_fi_res = ws_fi_v33.TimbrarTxt(ws_fi_pet);
                    #endregion

                    #region Repuesta del Timbrado
                    if (ws_fi_res.exito)
                    {
                        if (!string.IsNullOrEmpty(ws_fi_res.uuid))
                        {
                            string UUID = "";

                            UUID = ws_fi_res.uuid;

                            if (!string.IsNullOrEmpty(ws_fi_res.xmlTimbrado))
                            {
                                byte[] archivoXML = null;
                                byte[] archivoPDF = null;
                                string respenv = "";

                                if (esProduccion)
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        respenv = "El PAC no entrego el archivo PDF, Verificar con el PAC. UUID: " + UUID;
                                        insertaLog("btnFacturar", "ObtenerPDF-PAC", "Error", respenv, true);
                                    }
                                }
                                else
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        string _urlpdf = ConfigurationManager.AppSettings["PDF_Prueba"].ToString();

                                        archivoPDF = convierteaBits(_urlpdf);
                                    }
                                }

                                archivoXML = Encoding.UTF8.GetBytes(ws_fi_res.xmlTimbrado);

                                #region Guarda Factura
                                bool respIns = false;
                                int ID_Cliente = 0;
                                string _Correo = "";
                                string Serie = "";
                                string Descripcion = "";
                                string Nombre_XML = "";
                                string Nombre_PDF = "";
                                string ID_Origen_Factura = "";
                                bool Valido = true;

                                ID_Cliente = idCliente;
                                _Correo = correo;
                                Descripcion = _observaciones;
                                Nombre_XML = rfc.Trim() + "_" + UUID + ".xml";
                                Nombre_PDF = rfc.Trim() + "_" + UUID + ".pdf";
                                ID_Origen_Factura = Origen_Factura;

                                try
                                {
                                    respIns = GuardaFactura(ID_Cliente, UUID, dtAhora, Total, Folio, Serie,
                                        Descripcion, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF,
                                        ID_Origen_Factura, Valido);

                                    if (respIns)
                                    {
                                        int ID_Factura = 0;
                                        ID_Factura = InsertaTicketenFactura(ID_Cliente, UUID, dtticket);

                                        #region Almacena Factura
                                        respIns = AlmacenaFactura(dtAhora, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF);

                                        if (respIns)
                                        {
                                            #region Envía Correo
                                            string pathBobeda = "";
                                            string pathAnio = "";
                                            string pathMes = "";
                                            string pathDia = "";
                                            string path = "";
                                            string Anio = "";
                                            string Mes = "";
                                            string Dia = "";

                                            Anio = dtAhora.Year.ToString();
                                            Mes = dtAhora.Month.ToString();
                                            Dia = dtAhora.Day.ToString();

                                            Mes = Mes.Length > 1 ? Mes : "0" + Mes;
                                            Dia = Dia.Length > 1 ? Dia : "0" + Dia;

                                            pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
                                            pathAnio = pathBobeda + "/" + Anio;
                                            pathMes = pathBobeda + "/" + Anio + "/" + Mes;
                                            pathDia = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia;
                                            path = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia + "/";

                                            BLEnviaCorreo blec = new BLEnviaCorreo();
                                            try
                                            {
                                                if (archivoPDF == null)
                                                    Nombre_PDF = null;

                                                respenv = blec.EnviaFcturas(_Correo.Trim(), path, Nombre_XML, Nombre_PDF);

                                                if (respenv == "Ok")
                                                {
                                                    respenv = "La factura ya fue envíada a su correo. Factura : " + ID_Factura;
                                                    insertaLog("btnFacturar", "EnviaFcturas", "Ok", respenv, false);

                                                    cadenaconcatenada[0] = "Ok";
                                                    cadenaconcatenada[1] = respenv;
                                                    cadenaconcatenada[2] = "Timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                                }
                                                else
                                                {
                                                    cadenaconcatenada[0] = "Error";
                                                    cadenaconcatenada[1] = "No se pudo enviar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                                    cadenaconcatenada[2] = "Verifique el metodo EnviaFcturas. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                insertaLog("btnFacturar", "EnviaFcturas", "Error", ex.Message, true);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            cadenaconcatenada[0] = "Error";
                                            cadenaconcatenada[1] = "No se pudo guardar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                            cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        cadenaconcatenada[0] = "Error";
                                        cadenaconcatenada[1] = "No guardo la factura en BD.";
                                        cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);
                                    cadenaconcatenada[0] = "Error";
                                    cadenaconcatenada[1] = ex.Message;
                                    cadenaconcatenada[2] = ex.InnerException.ToString() + ". Pero Si timbro la factura correctamente, anexo UUID: " + UUID; ;
                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                                    Folio = "";

                                    return cadenaconcatenada;
                                }
                                #endregion
                            }
                            else
                            {
                                insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID, false);

                                cadenaconcatenada[0] = "Error";
                                cadenaconcatenada[1] = "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID;
                                cadenaconcatenada[2] = "Verificar con el PAC";
                                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                            }
                        }
                        else
                        {
                            insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener UUID, el PAC no lo proporciono.", false);

                            cadenaconcatenada[0] = "Error";
                            cadenaconcatenada[1] = "No se pudo Obtener UUID el PAC no lo proporciono";
                            cadenaconcatenada[2] = "Verificar con el PAC";
                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                        }
                    }
                    else
                    {
                        string allError = "";

                        allError = ws_fi_res.errorEspecifico + ", " + ws_fi_res.errorGeneral;

                        insertaLog("btnFacturar", "TimbrarTxT", "Error", allError, false);

                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = ws_fi_res.errorEspecifico;
                        cadenaconcatenada[2] = ws_fi_res.errorGeneral;
                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                    }
                    #endregion
                }
                else
                {
                    cadenaconcatenada[0] = strConceptos[0];
                    cadenaconcatenada[1] = strConceptos[1];
                    cadenaconcatenada[2] = strConceptos[2];
                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + nombre_razons;
                Folio = "";
            }

            Folio = "";
            return cadenaconcatenada;
        }

        /// <summary>
        /// Metodo para Cancelar la Factura
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="rfc"></param>
        /// <returns></returns>
        public string[] CancelaFactura(string uuid, string rfc)
        {
            string userPAC = "";
            string passPAC = "";
            string UUID = "";
            string _rfc = "";
            string[] cadenaconcatenada = new string[4];

            try
            {
                userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();
                UUID = uuid;
                _rfc = rfc;

                WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();

                WS_FI_33.RespuestaCancelacion respuestaCancelacion = new WS_FI_33.RespuestaCancelacion();

                respuestaCancelacion = ws_fi_v33.CancelarCFDI(userPAC, passPAC, _rfc, UUID, "", "");

                if (respuestaCancelacion.OperacionExitosa)
                {

                    cadenaconcatenada[0] = "Ok";
                    cadenaconcatenada[1] = respuestaCancelacion.XMLAcuse;
                    cadenaconcatenada[2] = "RFC: " + _rfc;
                    cadenaconcatenada[3] = "UUI: " + UUID;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                insertaLog("btnCnacelar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + _rfc + "; UUID: " + UUID;
            }

            return cadenaconcatenada;
        }

        #region crea cadenas del cfdi
        /// <summary>
        /// Cadena 01
        /// </summary>
        /// <param name="dtahora"></param>
        /// <param name="tneto"></param>
        /// <param name="total"></param>
        /// <param name="metododepago"></param>
        /// <returns></returns>
        private string CadenaDatosCFDI(DateTime dtahora, decimal tneto, decimal total, string metododepago)
        {
            string respCadena = "";
            try
            {
                string lg_cp = ConfigurationManager.AppSettings["LugExp_CP"].ToString();
                int _folio = 0;

                _folio = DeterminaFolio();

                if (_folio > 0)
                {
                    Folio = _folio.ToString();

                    respCadena = "#01@|" +
                         //string.Format("{0:s}", dtahora) +
                         //dtahora.ToString("o") //1.-Fecha 
                         "|01|" //2.- Forma de Pago
                        + string.Format("{0:0.00}", tneto) // 3.- Subtotal
                        + "|||MXN|" // 4.- Descuento, 5.-Condiciones de Pago, 6.- Moneda
                        + string.Format("{0:0.00}", total) // 7.- Total
                        + "|I|" // 8.- Tipo de Comprobante
                        + metododepago // 9.- Metodo de Pago
                        + "|" + lg_cp // 10.- Lugar de Expedicion CP
                        + "||" // 11.- Serie
                        + Folio //12.- Folio
                        + "|1|||"; // 13.- Tipo de Cambio, 14.- Codigo de Confirmación, 15.- Cadena CfdiRelacionados
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "CadenaDatosCFDI", "Error", ex.Message, true);
            }

            return respCadena;
        }


        private string CadenaDatosCFDI_lay(DateTime dtahora, decimal tneto, decimal total, string metododepago,string formapago)
        {
            string respCadena = "";
            try
            {
                string lg_cp = ConfigurationManager.AppSettings["LugExp_CP"].ToString();
                int _folio = 0;

                _folio = DeterminaFolio();
              

                if (_folio > 0)
                {
                    Folio = _folio.ToString();

                    respCadena = "#01@|" +
                         //string.Format("{0:s}", dtahora) +
                         //dtahora.ToString("o") //1.-Fecha 
                         //"|01|" //2.- Forma de Pago
                         "|"+ formapago + "|" //2.- Forma de Pago
                        + string.Format("{0:0.00}", tneto) // 3.- Subtotal
                        + "|||MXN|" // 4.- Descuento, 5.-Condiciones de Pago, 6.- Moneda
                        + string.Format("{0:0.00}", total) // 7.- Total
                        + "|I|" // 8.- Tipo de Comprobante
                        + metododepago // 9.- Metodo de Pago
                        + "|" + lg_cp // 10.- Lugar de Expedicion CP
                        + "||" // 11.- Serie
                        + Folio //12.- Folio
                        + "|1|||"; // 13.- Tipo de Cambio, 14.- Codigo de Confirmación, 15.- Cadena CfdiRelacionados
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "CadenaDatosCFDI", "Error", ex.Message, true);
            }

            return respCadena;
        }

        /// <summary>
        /// Cadena 02
        /// </summary>
        /// <returns></returns>
        private string CadenaDatosReceptor(string rfc, string nombre_razons, bool buscarEnBase)
        {
            string respCadena = "";
            string domicilio = "";
            try
            {
                if (buscarEnBase)
                {
                    BLClientes blc = new BLClientes();
                    List<Clientes> csCliente = blc.buscaClienteRFC(rfc);
                    if (csCliente.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(csCliente[0].Razon_social))
                            domicilio = csCliente[0].Razon_social;

                        respCadena = "#02@|" + csCliente[0].RFC // 1.- RFC
                        + "|" + domicilio // 2.- Nombre
                        + "|||" + UsoCFDI + "| "; // 3.- Resisdencia Fiscal, 4.- NumRegIdTrib, 5.- Uso CFDI
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(nombre_razons))
                        domicilio = nombre_razons;

                    respCadena = "#02@|" + rfc // 1.- RFC
                        + "|" + domicilio // 2.- Nombre
                        + "|||" + UsoCFDI  + "| "; // 3.- Resisdencia Fiscal, 4.- NumRegIdTrib, 5.- Uso CFDI
                }
            }
            catch (Exception ex)
            {
                insertaLog("CadenaDatosReceptor", "buscaClienteRFC", "Error", ex.Message, true);
                throw ex;
            }

            return respCadena;
        }

        private Clientes BuscaCliente(string rfc)
        {
            Clientes csCliente = new Clientes();
            try
            {
                BLClientes blc = new BLClientes();

                csCliente = blc.buscaClienteRFC(rfc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                insertaLog("BuscaCliente", "buscaClienteRFC", "Error", ex.Message, true);
                throw ex;
            }

            return csCliente;
        }

        /// <summary>
        /// Cadena de Conceptos 03
        /// </summary>
        /// <param name="dttickets"></param>
        /// <returns></returns>
        private string[] CadenaConceptos(DataTable dttickets)
        {
            //El arreglo regresa CadenaConceptos, Sumatoria Valor Unitario, Sumatoria Impuesto, Metodo de Pago
            string[] returnResp = new string[5];
            decimal totalImpuesto = 0;
            decimal totalNeto = 0;
            string Concepto = "";
            int ctaRoes = 0;
            string Metodo_Pago = "";
            string respDesc = "";

            try
            {
                ctaRoes = dttickets.Rows.Count;

                for (int n = 0; n < ctaRoes; n++)
                {
                    string no_ticket = "";
                    string estacionamiento = "";
                    decimal importe = 0;
                    DateTime fticket = new DateTime();
                    decimal impuesto = 0;
                    decimal neto = 0;
                    string MP = "";
                    //string tra_ = "%TRA/16.00/002/Tasa/0.160000/";
                    string tra_ = "%TRA/";
                    //string ret_ = "%RET/16.00/002/Tasa/0.16/";

                    no_ticket = dttickets.Rows[n]["No_Ticket"].ToString();
                    estacionamiento = dttickets.Rows[n]["Nombre_Sistema"].ToString().Trim();
                    importe = Convert.ToDecimal(dttickets.Rows[n]["Importe_Total"].ToString());
                    fticket = Convert.ToDateTime(dttickets.Rows[n]["Fecha_Ticket"].ToString());
                    MP = dttickets.Rows[n]["Forma_Pago"].ToString();

                    if (respDesc == "")
                        respDesc = "Se facturan los tickets: " + no_ticket;
                    else
                        respDesc = respDesc + ", " + no_ticket;

                    if (importe > 0)
                    {
                        neto = importe / Convert.ToDecimal(1.16);
                        impuesto = importe - neto;
                        tra_ = tra_ + string.Format("{0:0.00}", neto) + "/002/Tasa/0.160000/" + string.Format("{0:0.00}", impuesto);
                        //ret_ = ret_ + (importe * Convert.ToDecimal(.16)).ToString("N2");
                    }

                    Concepto = Concepto + "#03@|1||Servicio de Estacionamiento: " + estacionamiento + ", " + fticket.ToShortDateString() + "|"
                               // 1.- Cantidad, 2.- Unidad de Medida, 3.- Descripcion
                               + string.Format("{0:0.00}", neto) + "|" + string.Format("{0:0.00}", neto) + "|" + no_ticket
                    // 4.- Valor Unitario, 5.- Importe, 6.- N° de Identificación

                    //           + "|78111807|E48|";
                    //// 7.- Clave Prod o Serv, 8.- Clave Unidad

                    + "|" + claveproducto + "|" + ClaveUnidad + "|| "
                    // 7.- Clave Prod o Serv, 8.- Clave Unidad, 9.- Descuento
                    //+ tra_ + ret_ + "|||||";
                    + tra_ + "||||";
                    //// 10.- Cadena de Impuestos, 11.- Informacion Aduanera, 12.- Cuenta Predial, 13.- Cadena Complemento Concepto
                    //// 14.- Cadena Parte


                    totalImpuesto = totalImpuesto + Convert.ToDecimal(string.Format("{0:0.00}", impuesto));
                    totalNeto = totalNeto + Convert.ToDecimal(string.Format("{0:0.00}", neto));
                    Metodo_Pago = "PUE";
                }

                returnResp[0] = Concepto;
                returnResp[1] = totalNeto.ToString("N2");
                returnResp[2] = totalImpuesto.ToString("N2");
                returnResp[3] = Metodo_Pago.ToString();
                returnResp[4] = respDesc;
            }
            catch (Exception ex)
            {
                returnResp[0] = "Error";
                returnResp[1] = "Error: " + ex.Message.ToString();
                returnResp[2] = "Error Interno: " + ex.InnerException.ToString();
                returnResp[3] = "";
                returnResp[4] = "";
                insertaLog("btnFacturar", "CadenaConceptos", "Error", ex.Message, true);
            }

            return returnResp;
        }

        private string[] CadenaConceptosCargaIngresos(DataTable _dtConceptos)
        {
            string[] returnResp = new string[10];
            string Conceptos = "";
            int ctaRoes = 0;

            string RFC = "";
            decimal Subtotal = 0;
            decimal IVA = 0;
            decimal Total = 0;
            string Correos = "";
            string _metodopago = string.Empty;
            string _formapago = string.Empty;
            string _claveunidad = string.Empty;
            string _razonsocial = string.Empty;

            try
            {
                ctaRoes = _dtConceptos.Rows.Count;

                for (int n = 0; n < ctaRoes; n++)
                {
                    decimal Cantidad = 0;
                    string Unidad = "";
                    string Descripcion = "";
                    decimal Precio_Unitario = 0;
                    decimal Importe = 0;
                    string tra_ = "%TRA/";

                    //string _clave = _dtConceptos.Rows[n][1].ToString();
                    //string _claveU = _dtConceptos.Rows[n]["UNIDAD"].ToString();

                    RFC = _dtConceptos.Rows[n]["RFC"].ToString();
                    Subtotal = Subtotal + Convert.ToDecimal(_dtConceptos.Rows[n]["SUBTOTAL"].ToString());
                    IVA = IVA + Convert.ToDecimal(_dtConceptos.Rows[n]["IVA"].ToString());
                    Total = Total + Convert.ToDecimal(_dtConceptos.Rows[n]["TOTAL"].ToString());
                    Correos = "hnunez@invexinfraestructura.com";
                    //Correos = _dtConceptos.Rows[n]["CORREOS"].ToString();    
                    Cantidad = Convert.ToDecimal(_dtConceptos.Rows[n]["CANTIDAD"].ToString());
                    Unidad = _dtConceptos.Rows[n]["UNIDAD"].ToString();
                    Descripcion = _dtConceptos.Rows[n]["DESCRIPCION"].ToString();
                    Precio_Unitario = Convert.ToDecimal(_dtConceptos.Rows[n]["PRECIO_UNITARIO"].ToString());
                    Importe = Convert.ToDecimal(_dtConceptos.Rows[n]["IMPORTE"].ToString());
                    string _clavep = Convert.ToString(_dtConceptos.Rows[n]["CLAVE_PRODUCTO"].ToString());
                    _metodopago = Convert.ToString(_dtConceptos.Rows[n]["METPAG"].ToString());
                    //_formapago = Convert.ToString(_dtConceptos.Rows[n]["FORMAPAG"].ToString());
                    _formapago = "01";
                    _claveunidad = ClaveUnidad;
                    //_claveunidad = Convert.ToString(_dtConceptos.Rows[n]["CLAVEUNIDAD"].ToString());
                    //_razonsocial = Convert.ToString(_dtConceptos.Rows[n]["RAZONSOCIAL"].ToString());

                    tra_ = tra_ + string.Format("{0:0.00}", Importe) + "/002/Tasa/0.160000/" + string.Format("{0:0.00}", (Importe * Convert.ToDecimal(".16")));

                    Conceptos = Conceptos + "#03@|" + string.Format("{0:0.00}", Cantidad) + "|" + Unidad + "|" + Descripcion + "|"
                                   // 1.- Cantidad, 2.- Unidad de Medida, 3.- Descripcion
                                   + string.Format("{0:0.00}", Precio_Unitario) + "|" + string.Format("{0:0.00}", Importe) + "|"
                          // 4.- Valor Unitario, 5.- Importe, 6.- N° de Identificación
                          //+ "|78111807|E48||"
                          //+ "|" + _clavep + "|" + ClaveUnidad + "|| "
                          + "|" + _clavep + "|" + _claveunidad + "|| "
                        //+ "|" + claveproducto + "|" + ClaveUnidad + "|| "
                        //+ "|" + _clave + "|" + _claveU + "|| "
                        // 7.- Clave Prod o Serv, 8.- Clave Unidad, 9.- Descuento
                        + tra_ + "||||";
                    //// 10.- Cadena de Impuestos, 11.- Informacion Aduanera, 12.- Cuenta Predial, 13.- Cadena Complemento Concepto 14.- Cadena Parte
                }

                returnResp[0] = Conceptos;
                returnResp[1] = Subtotal.ToString("N2");
                returnResp[2] = IVA.ToString("N2");
                returnResp[3] = Total.ToString("N2");
                returnResp[4] = RFC.ToString();
                returnResp[5] = Correos.ToString();
                returnResp[6] = _metodopago.ToString();
                returnResp[7] = _formapago.ToString();
                returnResp[8] = _claveunidad.ToString();
                returnResp[9] = _razonsocial.ToString();
            }
            catch (Exception ex)
            {
                returnResp[0] = "Error";
                returnResp[1] = "Error: " + ex.Message.ToString();
                returnResp[2] = "Error Interno: " + ex.InnerException.ToString();
                returnResp[3] = "";
                returnResp[4] = "";
                returnResp[5] = "";
                returnResp[6] = "";
                returnResp[7] = "";
                returnResp[8] = "";
                returnResp[9] = "";

                insertaLog("btnFacturar", "CadenaConceptos", "Error", ex.Message, true);
            }

            return returnResp;

        }

        /// <summary>
        /// Cadena 04A
        /// </summary>
        /// <param name="importe"></param>
        /// <returns></returns>
        private string Cadena04A(decimal importe)
        {
            decimal impuesto = 0;
            decimal neto = 0;
            string _04A = "#04A@|0.160000|002|";

            if (importe > 0)
            {
                neto = importe / Convert.ToDecimal("1.16");
                impuesto = importe - neto;
            }

            string _importe_tras = (importe / Convert.ToDecimal("1.16")).ToString();

            return _04A + string.Format("{0:0.00}", importe) + "|Tasa|#04TI@|" + string.Format("{0:0.00}", importe) + "||";
        }
        #endregion

        #region Metodos de guardar en base y de apoyo
        private int DeterminaFolio()
        {
            int respFolio = 0;
            BLFacturas blf = new BLFacturas();
            try
            {
                int noFac = 0;
                noFac = blf.cuentaFacturas() + 1;
                //Class1 aces = new Class1();

                respFolio = noFac;
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "determinaFolio", "Error", ex.Message, true);
            }
            return respFolio;
        }

        private int InsertaTicketenFactura(int _id_cliente, string _uuid, DataTable _dtTickets)
        {
            int respId_Factura = 0;
            BLFacturas blfac = new BLFacturas();
            try
            {
                List<DAL.Bean.Portal.Facturas> csFacturacion = blfac.buscaFacturas(_id_cliente, _uuid);

                if (csFacturacion.Count > 0)
                {
                    respId_Factura = csFacturacion[0].ID_Factura;

                    #region Inserta Ticket en Factura
                    int noReg = 0;
                    noReg = _dtTickets.Rows.Count;
                    for (int n = 0; n < noReg; n++)
                    {
                        string No_Ticket = "";
                        No_Ticket = _dtTickets.Rows[n]["No_Ticket"].ToString();
                        bool resp = false;
                        BLTickets_Facturados bltenf = new BLTickets_Facturados();
                        try
                        {
                            resp = bltenf.insertaTenF(No_Ticket, respId_Factura);
                            if (!resp)
                            {
                                string msj = "";
                                msj = "No se pudo insertar el Ticket : " + No_Ticket + ", en la factura : " + respId_Factura;
                                insertaLog("InsertaTicketenFactura", "TimbrarTxT", "Error", msj, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            insertaLog("InsertaTicketenFactura", "TimbrarTxT", "Error", ex.Message, true);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                insertaLog("InsertaTicketenFactura", "buscaFacturas", "Error", ex.Message, true);
            }
            return respId_Factura;
        }

        private bool AlmacenaFactura(DateTime _fecha, string _nombrexml, string _nombrepdf, byte[] _archivoxml, byte[] _archivopdf)
        {
            bool resp = false;
            string pathBobeda = "";
            string pathAnio = "";
            string pathMes = "";
            string pathDia = "";
            string Anio = "";
            string Mes = "";
            string Dia = "";

            Anio = _fecha.Year.ToString();
            Mes = _fecha.Month.ToString();
            Dia = _fecha.Day.ToString();

            Mes = Mes.Length > 1 ? Mes : "0" + Mes;
            Dia = Dia.Length > 1 ? Dia : "0" + Dia;

            //pathBobeda = Server.MapPath(@"./Boveda/");

            pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
            pathAnio = pathBobeda + Anio;
            pathMes = pathBobeda + Anio + "/" + Mes;
            pathDia = pathBobeda + Anio + "/" + Mes + "/" + Dia;

            try
            {
                if (!Directory.Exists(pathBobeda))
                    Directory.CreateDirectory(pathBobeda);

                if (!Directory.Exists(pathAnio))
                    Directory.CreateDirectory(pathAnio);

                if (!Directory.Exists(pathMes))
                    Directory.CreateDirectory(pathMes);

                if (!Directory.Exists(pathDia))
                    Directory.CreateDirectory(pathDia);

                FileStream fsxml = null;
                FileStream fspdf = null;
                try
                {
                    fsxml = new FileStream(pathDia + "/" + _nombrexml, FileMode.Create, FileAccess.Write);
                    fsxml.Write(_archivoxml, 0, _archivoxml.Length);
                    fsxml.Close();
                    fsxml.Dispose();

                    if (_archivopdf != null)
                    {
                        fspdf = new FileStream(pathDia + "/" + _nombrepdf, FileMode.Create, FileAccess.Write);
                        fspdf.Write(_archivopdf, 0, _archivopdf.Length);
                        fspdf.Close();
                        fspdf.Dispose();
                    }

                    resp = true;

                }
                catch (Exception ex)
                {
                    insertaLog("AlmacenaFactura", "Creaciondearchivos", "Error", ex.Message, true);
                }
            }
            catch (Exception ex)
            {
                insertaLog("AlmacenaFactura", "Creaciondecarpetas", "Error", ex.Message, true);
            }

            return resp;
        }

        private bool GuardaCorreo(int _id_cliente, string _correo)
        {
            bool resp = false;
            BLCorreo_Cliente blccbusca = new BLCorreo_Cliente();
            try
            {
                List<Correo_Cliente> csCC = blccbusca.BuscaCorreo(_id_cliente, _correo);
                if (csCC.Count > 0)
                {
                    int Id_Correo = 0;
                    Id_Correo = csCC[0].ID_Correo;
                    if (!csCC[0].Valido)
                    {
                        #region Actualiza Correo
                        BLCorreo_Cliente blccUpdate = new BLCorreo_Cliente();
                        try
                        {
                            resp = blccUpdate.ActualizaCorreoCliente(Id_Correo, true);
                            if (!resp)
                            {
                                insertaLog("GuardaCorreo", "ActualizaCorreoCliente", "Error", "No se pudo actualizar correo de Cliente : " + _id_cliente, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            insertaLog("GuardaCorreo", "ActualizaCorreoCliente", "Error", ex.Message, true);
                        }
                        #endregion
                    }
                    else
                        resp = true;
                }
                else
                {
                    #region Guarda Correo
                    BLCorreo_Cliente blccInsert = new BLCorreo_Cliente();
                    try
                    {
                        resp = blccInsert.insertaCorreoCliente(_id_cliente, _correo);
                        if (!resp)
                        {
                            insertaLog("GuardaCorreo", "insertaCorreoCliente", "Error", "No se pudo guardar correo de Cliente : " + _id_cliente, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        insertaLog("GuardaCorreo", "insertaCorreoCliente", "Error", ex.Message, true);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                insertaLog("GuardaCorreo", "TimbrarTxT", "Error", ex.Message, true);
            }
            return resp;
        }

        private bool GuardaFactura(int _idcliente, string _uuid, DateTime _dtahora, decimal _total, string _folio, string _serie,
                                   string _descripcion, string _nombre_xml, string _nombre_pdf, byte[] _archivo_xml, byte[] _archivo_pdf,
                                   string _origen, bool _valido)
        {
            bool guardafact_bd = false;

            BLFacturas blfacinert = new BLFacturas();
            try
            {

                guardafact_bd = blfacinert.insertaFactura(_idcliente, _uuid, _dtahora, _total, _folio, _serie, _descripcion, _nombre_xml,
                                                          _nombre_pdf, _archivo_xml, _archivo_pdf, _origen, _valido);
            }
            catch (Exception ex)
            {
                insertaLog("GuardaFactura", "insertaFactura", "Error", ex.Message, true);
            }

            return guardafact_bd;
        }

        private int InsertaRegistroFacturado(int _id_cliente, string _uuid, DataTable _dtConceptos, string _correos)
        {
            int respId_Factura = 0;
            BLFacturas blfac = new BLFacturas();
            try
            {
                List<DAL.Bean.Portal.Facturas> csFacturacion = blfac.buscaFacturas(_id_cliente, _uuid);
                if (csFacturacion.Count > 0)
                {
                    respId_Factura = csFacturacion[0].ID_Factura;

                    #region Insertamos los registros facturados
                    int noReg = 0;
                    noReg = _dtConceptos.Rows.Count;
                    for (int n = 0; n < noReg; n++)
                    {
                        string strResp = "";
                        decimal Cantidad = 0;
                        string Unidad = "";
                        string Descripción = "";
                        decimal Precio_Unitario = 0;
                        decimal Importe = 0;
                        DateTime Fecha = new DateTime();

                        BLPensiones blp = new BLPensiones();
                        try
                        {
                            #region Inserta pensión
                            Cantidad = Convert.ToDecimal(_dtConceptos.Rows[n]["CANTIDAD"].ToString());
                            Unidad = _dtConceptos.Rows[n]["UNIDAD"].ToString();
                            Descripción = _dtConceptos.Rows[n]["DESCRIPCION"].ToString();
                            Precio_Unitario = Convert.ToDecimal(_dtConceptos.Rows[n]["PRECIO_UNITARIO"].ToString());
                            Importe = Convert.ToDecimal(_dtConceptos.Rows[n]["IMPORTE"].ToString());
                            Fecha = DateTime.Now;


                            bool resp = blp.insertaPension(Cantidad, Unidad, Descripción, Precio_Unitario, Importe, _correos, Fecha, respId_Factura);
                            if (resp)
                            {
                                strResp = "Registro insertada correctamente, Pensión: " + Cantidad.ToString() + Unidad.ToString() + Descripción + ". Factura : " + respId_Factura.ToString();
                                insertaLog("InsertaRegistroFacturado", "insertaPension", "Ok", strResp, false);
                            }
                            else
                            {
                                strResp = "No se pudo insertar Pensión: " + Cantidad.ToString() + Unidad.ToString() + Descripción + ". Factura : " + respId_Factura.ToString();
                                insertaLog("InsertaRegistroFacturado", "insertaPension", "Error", strResp, false);
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            insertaLog("InsertaRegistroFacturado", "insertaPension", "Error", ex.Message, true);
                        }
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {
                insertaLog("InsertaRegistroFacturado", "csFacturacion", "Error", ex.Message, true);
            }

            return respId_Factura;
        }

        /// <summary>
        /// Metodo para convertir a cadena de Bits los archivos
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private byte[] convierteaBits(string _path)
        {
            byte[] resp = null;
            try
            {
                FileStream fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                resp = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
            }
            catch (Exception ex) { throw ex; }

            return resp;
        }
        #endregion

        #region Manejo de errores
        /// <summary>
        /// Metodo para insertar Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_metodo"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripcion"></param>
        /// <param name="_essistema"></param>
        private void insertaLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _essistema)
        {
            int idUsuario = 0;
            DateTime dtAhora = new DateTime();

            dtAhora = DateTime.Now;
            BLLogs bllog = new BLLogs();
            try
            {
                bool resp = false;
                resp = bllog.insertaLog("Facturacion", _evento, _metodo, dtAhora, _respuesta, _descripcion, _essistema, idUsuario);
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion


        private string CadenaDatosReceptorLayOut(string rfc, string nombre_razons, bool buscarEnBase, string Usoc)
        {
            string respCadena = "";
            string domicilio = "";
            try
            {
                if (buscarEnBase)
                {
                    BLClientes blc = new BLClientes();
                    List<Clientes> csCliente = blc.buscaClienteRFC(rfc);
                    if (csCliente.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(csCliente[0].Razon_social))
                            domicilio = csCliente[0].Razon_social;

                        respCadena = "#02@|" + csCliente[0].RFC // 1.- RFC
                        + "|" + domicilio // 2.- Nombre
                        + "|||" + Usoc + "| "; // 3.- Resisdencia Fiscal, 4.- NumRegIdTrib, 5.- Uso CFDI
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(nombre_razons))
                        domicilio = nombre_razons;

                    respCadena = "#02@|" + rfc // 1.- RFC
                        + "|" + domicilio // 2.- Nombre
                        + "|||" + Usoc + "| "; // 3.- Resisdencia Fiscal, 4.- NumRegIdTrib, 5.- Uso CFDI
                }
            }
            catch (Exception ex)
            {
                insertaLog("CadenaDatosReceptor", "buscaClienteRFC", "Error", ex.Message, true);
                throw ex;
            }

            return respCadena;
        }

        public string[] CreaTimbraCFDI_CargaIngresosLay(DataTable dtIngresos, string Origen_Factura, string nombreArchivo, bool esProduccion,int folio)
        {
            string usoC = "";
            string cadena01 = "";
            string cadena02 = "";
            string cadena03 = "";
            string cadena04A = "";
            string[] cadenaconcatenada = new string[4];
            string[] strConceptos = new string[10];
            string rfc = "";
            Clientes _cliente = new Clientes();

            try
            {
                usoC = dtIngresos.Rows[0].Field<string>("USO_CFDI");
                strConceptos = CadenaConceptosCargaIngresos(dtIngresos);

                if (strConceptos != null && strConceptos[0] != "Error")
                {
                    Folio = "";

                    #region Asignacion y armado de cadena 03: Conceptos
                    decimal TNeto = 0;
                    decimal TImpuesto = 0;
                    decimal Total = 0;
                    string MetododePago = "";
                    string FormadePago = "01";
                    string claveunidad = string.Empty;
                    string razonsocial = string.Empty;

                    cadena03 = strConceptos[0].ToString();

                    TNeto = Convert.ToDecimal(strConceptos[1].ToString());
                    TImpuesto = Convert.ToDecimal(strConceptos[2].ToString());
                    Total = Convert.ToDecimal(strConceptos[3].ToString());//TNeto + TImpuesto;
                    //MetododePago = "PUE";
                    MetododePago = strConceptos[6].ToString();
                    FormadePago = strConceptos[7].ToString();
                    claveunidad = strConceptos[8].ToString();
                    razonsocial = strConceptos[9].ToString();

                    rfc = strConceptos[4].ToString();

                    if (!string.IsNullOrEmpty(rfc))
                    {
                        _cliente = BuscaCliente(rfc);

                        if (_cliente == null)
                        {
                            //cadenaconcatenada[0] = "Error";
                            //cadenaconcatenada[1] = "No se encontro el Cliente con RFC: " + rfc;
                            //cadenaconcatenada[2] = "Error: RFC invalido.";
                            //cadenaconcatenada[3] = "";
                            //Folio = "";
                            string sql_ic = "Insert into TBL_Clientes(Rfc,Razon_Social) Values(@Rfc,@Razon_Social)";
                            SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());
                            cadena.Open();
                            SqlCommand cmd = cadena.CreateCommand();
                            cmd.CommandText = sql_ic;
                            cmd.Parameters.AddWithValue("@Rfc", rfc);
                            cmd.Parameters.AddWithValue("@Razon_Social", razonsocial);
                            cmd.ExecuteNonQuery();

                            cadenaconcatenada[0] = strConceptos[0];
                            cadenaconcatenada[1] = strConceptos[1];
                            cadenaconcatenada[2] = strConceptos[2];
                            cadenaconcatenada[3] = "RFC: " + rfc;

                            return cadenaconcatenada;
                        }
                    }
                    else
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se encontro el Cliente.";
                        cadenaconcatenada[2] = "Error: RFC invalido.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region  Asignacion y armado de cadena 01: Datos CFDI
                    DateTime dtAhora = new DateTime();
                    dtAhora = DateTime.Now.AddMinutes(-5);
                    //cadena01 = CadenaDatosCFDI(dtAhora, TNeto, Total, MetododePago);
                    cadena01 = CadenaDatosCFDI_lay1(dtAhora, TNeto, Total, MetododePago, FormadePago,folio);

                    if (string.IsNullOrEmpty(cadena01))
                    {
                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = "No se genero la cadena01";
                        cadenaconcatenada[2] = "Error en el metodo CadenaDatosCFDI.";
                        cadenaconcatenada[3] = "";
                        Folio = "";

                        return cadenaconcatenada;
                    }
                    #endregion

                    #region Asignacion y armado de cadena 02: Datos Receptor
                    cadena02 = CadenaDatosReceptorLayOut(rfc, _cliente.Razon_social, false, usoC);
                    #endregion

                    #region Asignacion y armado de cadena 04A: Traslados
                    //cadena04A = Cadena04A(Total);
                    cadena04A = Cadena04A(TImpuesto);
                    #endregion

                    #region Asigancion para Timbrar
                    string userPAC = "";
                    string passPAC = "";

                    userPAC = ConfigurationManager.AppSettings["UsuarioPac"].ToString();
                    passPAC = ConfigurationManager.AppSettings["PasswordPac"].ToString();

                    WS_FI_33.WsCFDI33Client ws_fi_v33 = new WS_FI_33.WsCFDI33Client();
                    WS_FI_33.Peticion ws_fi_pet = new WS_FI_33.Peticion();

                    ws_fi_pet.rfcEmisor = ConfigurationManager.AppSettings["RFCEmisor"].ToString();
                    ws_fi_pet.cadenaTXT = cadena01 + cadena02 + cadena03 + cadena04A;
                    ws_fi_pet.usuario = userPAC;
                    ws_fi_pet.contrasena = passPAC;
                    ws_fi_pet.productivo = esProduccion;
                    ws_fi_pet.pdf = true;
                    ws_fi_pet.tipoCFDI = 1;

                    WS_FI_33.Respuesta ws_fi_res = new WS_FI_33.Respuesta();

                    ws_fi_res = ws_fi_v33.TimbrarTxt(ws_fi_pet);
                    #endregion

                    #region Repuesta del Timbrado
                    if (ws_fi_res.exito)
                    {
                        if (!string.IsNullOrEmpty(ws_fi_res.uuid))
                        {
                            string UUID = "";

                            UUID = ws_fi_res.uuid;

                            if (!string.IsNullOrEmpty(ws_fi_res.xmlTimbrado))
                            {
                                byte[] archivoXML = null;
                                byte[] archivoPDF = null;
                                string respenv = "";

                                if (esProduccion)
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        respenv = "El PAC no entrego el archivo PDF, Verificar con el PAC. UUID: " + UUID;
                                        insertaLog("btnFacturar", "ObtenerPDF-PAC", "Error", respenv, true);
                                    }
                                }
                                else
                                {
                                    if (ws_fi_res.pdf != null)
                                        archivoPDF = ws_fi_res.pdf;
                                    else
                                    {
                                        string _urlpdf = ConfigurationManager.AppSettings["PDF_Prueba"].ToString();

                                        archivoPDF = convierteaBits(_urlpdf);
                                    }
                                }

                                archivoXML = Encoding.UTF8.GetBytes(ws_fi_res.xmlTimbrado);

                                #region Guarda Factura
                                bool respIns = false;
                                int ID_Cliente = 0;
                                string _Correo = "";
                                string Serie = "";
                                string Descripcion = "";
                                string Nombre_XML = "";
                                string Nombre_PDF = "";
                                string ID_Origen_Factura = "";
                                bool Valido = true;

                                ID_Cliente = _cliente.ID_Cliente;
                                _Correo = strConceptos[5].ToString();
                                if (Origen_Factura == "G")
                                {
                                    Descripcion = "Facturación Global";
                                }
                                else
                                {
                                    Descripcion = "Facturación de archivo: " + nombreArchivo;
                                }

                                Nombre_XML = rfc.Trim() + "_" + UUID + ".xml";
                                Nombre_PDF = rfc.Trim() + "_" + UUID + ".pdf";
                                ID_Origen_Factura = Origen_Factura;

                                try
                                {
                                    respIns = GuardaFactura(ID_Cliente, UUID, dtAhora, Total, Folio, Serie,
                                        Descripcion, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF,
                                        ID_Origen_Factura, Valido);

                                    if (respIns)
                                    {
                                        int ID_Factura = 0;
                                        ID_Factura = InsertaRegistroFacturado(ID_Cliente, UUID, dtIngresos, _Correo);

                                        #region Almacena Factura
                                        respIns = AlmacenaFactura(dtAhora, Nombre_XML, Nombre_PDF, archivoXML, archivoPDF);

                                        if (respIns)
                                        {
                                            #region Envía Correo
                                            string pathBobeda = "";
                                            string pathAnio = "";
                                            string pathMes = "";
                                            string pathDia = "";
                                            string path = "";
                                            string Anio = "";
                                            string Mes = "";
                                            string Dia = "";

                                            Anio = dtAhora.Year.ToString();
                                            Mes = dtAhora.Month.ToString();
                                            Dia = dtAhora.Day.ToString();

                                            Mes = Mes.Length > 1 ? Mes : "0" + Mes;
                                            Dia = Dia.Length > 1 ? Dia : "0" + Dia;

                                            pathBobeda = ConfigurationManager.AppSettings["FacturasTimbradas"].ToString();
                                            pathAnio = pathBobeda + "/" + Anio;
                                            pathMes = pathBobeda + "/" + Anio + "/" + Mes;
                                            pathDia = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia;
                                            path = pathBobeda + "/" + Anio + "/" + Mes + "/" + Dia + "/";

                                            BLEnviaCorreo blec = new BLEnviaCorreo();
                                            try
                                            {
                                                if (archivoPDF == null)
                                                    Nombre_PDF = null;

                                                respenv = blec.EnviaFcturas(_Correo.Trim(), path, Nombre_XML, Nombre_PDF);

                                                if (respenv == "Ok")
                                                {
                                                    respenv = "La factura ya fue envíada a su correo. Factura : " + ID_Factura;
                                                    insertaLog("btnFacturar", "EnviaFcturas", "Ok", respenv, false);

                                                    cadenaconcatenada[0] = "Ok";
                                                    cadenaconcatenada[1] = respenv;
                                                    cadenaconcatenada[2] = "Timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;

                                                }
                                                else
                                                {
                                                    cadenaconcatenada[0] = "Error";
                                                    cadenaconcatenada[1] = "No se pudo enviar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                                    cadenaconcatenada[2] = "Verifique el metodo EnviaFacturas. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                insertaLog("btnFacturar", "EnviaFcturas", "Error", ex.Message, true);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            cadenaconcatenada[0] = "Error";
                                            cadenaconcatenada[1] = "No se pudo guardar la factura. Favor de envíar correo al administrador con el siguiente numero: " + ID_Factura;
                                            cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        cadenaconcatenada[0] = "Error";
                                        cadenaconcatenada[1] = "No guardo la factura en BD.";
                                        cadenaconcatenada[2] = "Verifique el metodo AlmacenaFactura. Pero Si timbro la factura correctamente, anexo UUID: " + UUID;
                                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);
                                    cadenaconcatenada[0] = "Error";
                                    cadenaconcatenada[1] = ex.Message;
                                    cadenaconcatenada[2] = ex.InnerException.ToString() + ". Pero Si timbro la factura correctamente, anexo UUID: " + UUID; ;
                                    cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                                    Folio = "";

                                    return cadenaconcatenada;
                                }
                                #endregion
                            }
                            else
                            {
                                insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID, false);

                                cadenaconcatenada[0] = "Error";
                                cadenaconcatenada[1] = "No se pudo Obtener el XML, el PAC no lo proporciono, pero si mando el UUID: " + UUID;
                                cadenaconcatenada[2] = "Verificar con el PAC";
                                cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                            }
                        }
                        else
                        {
                            insertaLog("btnFacturar", "TimbrarTxT", "Error", "No se pudo Obtener UUID, el PAC no lo proporciono.", false);

                            cadenaconcatenada[0] = "Error";
                            cadenaconcatenada[1] = "No se pudo Obtener UUID el PAC no lo proporciono";
                            cadenaconcatenada[2] = "Verificar con el PAC";
                            cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                        }
                    }
                    else
                    {
                        string allError = "";

                        allError = ws_fi_res.errorEspecifico + ", " + ws_fi_res.errorGeneral;

                        insertaLog("btnFacturar", "TimbrarTxT", "Error", allError, false);

                        cadenaconcatenada[0] = "Error";
                        cadenaconcatenada[1] = ws_fi_res.errorEspecifico;
                        cadenaconcatenada[2] = ws_fi_res.errorGeneral;
                        cadenaconcatenada[3] = "RFC: " + rfc + "; Nombre: " + _cliente.Razon_social;
                    }
                    #endregion
                }
                else
                {
                    cadenaconcatenada[0] = strConceptos[0];
                    cadenaconcatenada[1] = strConceptos[1];
                    cadenaconcatenada[2] = strConceptos[2];
                    cadenaconcatenada[3] = "RFC: " + rfc;
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "TimbrarTxT", "Error", ex.Message, true);

                cadenaconcatenada[0] = "Error";
                cadenaconcatenada[1] = ex.Message;
                cadenaconcatenada[2] = ex.InnerException.ToString();
                cadenaconcatenada[3] = "RFC: " + rfc;
                Folio = "";
            }

            Folio = "";
            return cadenaconcatenada;
        }

        private string CadenaDatosCFDI_lay1(DateTime dtahora, decimal tneto, decimal total, string metododepago, string formapago, int folio)
        {
            string respCadena = "";
            try
            {
                string lg_cp = ConfigurationManager.AppSettings["LugExp_CP"].ToString();
                int _folio = 0;

                //_folio = DeterminaFolio();
                _folio = folio;

                if (_folio > 0)
                {
                    Folio = _folio.ToString();

                    respCadena = "#01@|" +
                         //string.Format("{0:s}", dtahora) +
                         //dtahora.ToString("o") //1.-Fecha 
                         //"|01|" //2.- Forma de Pago
                         "|" + formapago + "|" //2.- Forma de Pago
                        + string.Format("{0:0.00}", tneto) // 3.- Subtotal
                        + "|||MXN|" // 4.- Descuento, 5.-Condiciones de Pago, 6.- Moneda
                        + string.Format("{0:0.00}", total) // 7.- Total
                        + "|I|" // 8.- Tipo de Comprobante
                        + metododepago // 9.- Metodo de Pago
                        + "|" + lg_cp // 10.- Lugar de Expedicion CP
                        + "||" // 11.- Serie
                        + Folio //12.- Folio
                        + "|1|||"; // 13.- Tipo de Cambio, 14.- Codigo de Confirmación, 15.- Cadena CfdiRelacionados
                }
            }
            catch (Exception ex)
            {
                insertaLog("btnFacturar", "CadenaDatosCFDI", "Error", ex.Message, true);
            }

            return respCadena;
        }

    }

 
}
