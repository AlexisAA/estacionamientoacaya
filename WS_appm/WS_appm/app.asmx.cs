﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WS_appm
{
    /// <summary>
    /// Descripción breve de app
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class app : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string login(string imei,string pass)
        {
            string respuesta = string.Empty;
            EncripDesencrip desc = new EncripDesencrip();
            string passdesencripta = desc.DesencriptarCadenaDeCaracteres(pass, "keyFacturacionEspacia");
            Class1 busca = new Class1();
            bool sino = busca.buscar("Select * from TBL_Usuarios where contrasenia ='" + passdesencripta + "'");
            if (sino)
            {
                respuesta = "si";
            }
            else {
                respuesta = "no";
            }

            return respuesta;
        }
    }

}
