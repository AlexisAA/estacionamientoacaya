﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor_Skidata
{
    public partial class Form1 : Form
    {
        private static string directorioEspacia = "C:\\Espacia\\Config";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime fechaInicioProceso = new DateTime();
            //DateTime fechaFinProceso = new DateTime();
            DateTime fechaFinConsulta = new DateTime();

            fechaInicioProceso = DateTime.Now;
            fechaFinConsulta = fechaInicioProceso;
            DataTable dtConsulta = new DataTable();
            //lblAccion.Text = "Inicia consulta";
            dtConsulta = realizaConsulta(DateTime.Now);

            if (dtConsulta.Rows.Count > 0)
            {
                int ctaReg = 0;
                ctaReg = dtConsulta.Rows.Count;
                wsEnviaTickets.ServiceEspacia ws = new wsEnviaTickets.ServiceEspacia();
                BLSeguridad bls = new BLSeguridad();
                try
                {
                    string resp = "";
                    string Password = "";
                    Password = bls.encriptar("Prime@123");
                    resp = ws.recibeTickets("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                    //resp = ws.recibeTicketsSinEncriptar("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                    if (resp.IndexOf("insertaron") > 0)
                    {
                        //lblAccion.Text = "";
                        lst_error.Items.Clear();
                    }

                }
                catch (Exception ex)
                {
                    int noLineas = 0;
                    //noLineas = cuentaLineas() + 1;
                    string result = "";
                    result = noLineas + "|" + fechaInicioProceso + "|" + DateTime.Now + "|" + "wsEnviaTickets" + "|" + "Error" + "|" + ex.Message;
                    //guardaLog(result);
                    lblMensaje.Text = ex.Message;
                    lblMensaje.Visible = true;

                }
                //if (dtConsulta != null)
                //{
                //    int ctaReg = 0;
                //    ctaReg = dtConsulta.Rows.Count;
                //    if (ctaReg > 0)
                //    {
                //        string resp = "";

                //        wsEnviaTickets.ServiceEspacia ws = new wsEnviaTickets.ServiceEspacia();
                //        BLSeguridad bls = new BLSeguridad();
                //        try
                //        {
                //            string xRes = "Error";
                //            string Password = "";
                //            Password = bls.encriptar("Prime@123");
                //            //Password = bls.encriptar("prime123");
                //            resp = ws.recibeTickets("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                //            //resp = ws.recibeTicketsSinEncriptar("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                //            if (resp.IndexOf("insertaron") > 0)
                //            {
                //                xRes = "Ok";
                //                //lblAccion.Text = "";
                //                lst_error.Items.Clear();
                //            }

                //            int noLineas = 0;
                //            //noLineas = cuentaLineas() + 1;
                //            string result = "";
                //            result = noLineas + "|" + fechaInicioProceso + "|" + DateTime.Now + "|" + "wsEnviaTickets" + "|" + xRes + "|" + resp;
                //            //guardaLog(result);

                //        }
                //        catch (Exception ex)
                //        {
                //            int noLineas = 0;
                //            //noLineas = cuentaLineas() + 1;
                //            string result = "";
                //            result = noLineas + "|" + fechaInicioProceso + "|" + DateTime.Now + "|" + "wsEnviaTickets" + "|" + "Error" + "|" + ex.Message;
                //            //guardaLog(result);
                //            lblMensaje.Text = ex.Message;
                //            lblMensaje.Visible = true;
                //        }
                //    }

                //}
                //}


            }
        }


        private DataTable realizaConsulta(DateTime _fechafin)
        {
            DataTable dt = new DataTable();
            dt.TableName = "TicketsDT";
            dt.Columns.Add(new DataColumn("Número de Recibo", typeof(string)));
            dt.Columns.Add(new DataColumn("Codigo Sistema", typeof(string)));
            dt.Columns.Add(new DataColumn("Nombre Sistema", typeof(string)));
            dt.Columns.Add(new DataColumn("Fecha de Recibo", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Importe Total", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Forma de Pago", typeof(string)));
            dt.Columns.Add(new DataColumn("Últimos 4 dígitos Tarjeta", typeof(string)));

            Application.DoEvents();
            //lblAccion.Text = "Inicia consulta a la base de datos";

            DateTime FechaInicio = DateTime.Now;
            DateTime FechaFin = new DateTime();

            FechaFin = _fechafin;
            int anio = FechaFin.Year;
            int mes = FechaFin.Month;
            int dia = FechaFin.Day;
            int hora = FechaFin.Hour;
            int minutos = FechaFin.Minute;
            int segundos = FechaFin.Second;

            //FechaInicio = new DateTime(anio, mes, dia, hora, minutos, segundos);
            if (lst_error.Items.Count > 0)
            {
                char[] separadorcad = { '{' };
                char[] separadorcadR = { '}' };
                string Valeerror = lst_error.Items[0].ToString();
                string[] campos = Valeerror.Split(separadorcad);

                if (campos.Count() > 0)
                {
                    string[] campos1 = campos[1].Split(separadorcadR);
                    String fechar = campos1[0];
                    FechaInicio = Convert.ToDateTime(campos1[0].ToString());
                }
            }
            else
            {
                FechaInicio = new DateTime(anio, mes, dia, 0, 0, 0);
            }

            //FechaInicio = new DateTime(anio, mes, dia, 0, 0, 0);
            FechaFin = new DateTime(anio, mes, dia, hora, minutos, segundos);

            Application.DoEvents();
            //lblAccion.Text = "Consulta a la base de datos";
            string cadenaConexion = "";
            cadenaConexion = extraeCadenadeConexion();
            if (cadenaConexion != "")
            {
                string consulta = "";
                consulta = "SELECT CardNo, FacilityNo, CarparkDesig, Time, Amount, CardValue, DeviceDesig FROM PARK_DB.dbo.ParkingMovements ";
                consulta += "WHERE MovementTypeDesig = 'Transacción' AND MovementType = 1 AND ArticleNo = 1 ";
                consulta += "AND Time BETWEEN '02/02/2018 00:01' AND '02/02/2018 23:29'";

                //SqlConnection cadena = new SqlConnection(ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ToString());
                SqlConnection conexion = new SqlConnection();
                conexion.ConnectionString = cadenaConexion;
                SqlCommand cmd = new SqlCommand(consulta, conexion);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                conexion.Open();
                adapter.Fill(dt);
                conexion.Close();

            }

            return dt;
        }


        private string extraeCadenadeConexion()
        {
            string strPath = "";
            //strPath = directorioEspacia;
            string Cadena = "Data Source=201.150.42.58;Initial Catalog=PARK_DB;User ID=online;Password=Papalote_2017";

            return Cadena;
        }
    }
}

