﻿namespace Monitor_SAP
{
    partial class config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.btn_lectura = new Telerik.WinControls.UI.RadButton();
            this.txt_lectura = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.txt_folder = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.email = new Telerik.WinControls.UI.RadTextBox();
            this.admin = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tcheca = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cminutos = new System.Windows.Forms.ComboBox();
            this.choras = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_lectura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_lectura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_folder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.email)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.admin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(53, 346);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(110, 24);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Cerrar";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Location = new System.Drawing.Point(12, 12);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.btn_lectura);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.txt_lectura);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radLabel2);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radButton2);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.txt_folder);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radLabel5);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.email);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.admin);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radLabel4);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radLabel3);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radLabel1);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tcheca);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.label6);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.cminutos);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.choras);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.label7);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(265, 323);
            this.radScrollablePanel1.Size = new System.Drawing.Size(267, 325);
            this.radScrollablePanel1.TabIndex = 1;
            this.radScrollablePanel1.Click += new System.EventHandler(this.radScrollablePanel1_Click);
            // 
            // btn_lectura
            // 
            this.btn_lectura.Location = new System.Drawing.Point(240, 247);
            this.btn_lectura.Name = "btn_lectura";
            this.btn_lectura.Size = new System.Drawing.Size(16, 20);
            this.btn_lectura.TabIndex = 60;
            this.btn_lectura.Text = ";;";
            this.btn_lectura.Click += new System.EventHandler(this.btn_lectura_Click);
            // 
            // txt_lectura
            // 
            this.txt_lectura.Location = new System.Drawing.Point(6, 247);
            this.txt_lectura.Name = "txt_lectura";
            this.txt_lectura.Size = new System.Drawing.Size(234, 20);
            this.txt_lectura.TabIndex = 59;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 223);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(103, 18);
            this.radLabel2.TabIndex = 58;
            this.radLabel2.Text = "Carpeta de lectura::";
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(239, 197);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(16, 20);
            this.radButton2.TabIndex = 57;
            this.radButton2.Text = ";;";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // txt_folder
            // 
            this.txt_folder.Location = new System.Drawing.Point(5, 197);
            this.txt_folder.Name = "txt_folder";
            this.txt_folder.Size = new System.Drawing.Size(234, 20);
            this.txt_folder.TabIndex = 56;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(4, 173);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(103, 18);
            this.radLabel5.TabIndex = 55;
            this.radLabel5.Text = "Carpeta de destino:";
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(4, 147);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(251, 20);
            this.email.TabIndex = 54;
            // 
            // admin
            // 
            this.admin.Location = new System.Drawing.Point(3, 99);
            this.admin.Name = "admin";
            this.admin.Size = new System.Drawing.Size(251, 20);
            this.admin.TabIndex = 53;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(3, 123);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(35, 18);
            this.radLabel4.TabIndex = 52;
            this.radLabel4.Text = "Email:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(3, 75);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(80, 18);
            this.radLabel3.TabIndex = 51;
            this.radLabel3.Text = "Administrador:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(131, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Configuración de tiempo";
            // 
            // tcheca
            // 
            this.tcheca.AutoSize = true;
            this.tcheca.Checked = true;
            this.tcheca.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tcheca.Location = new System.Drawing.Point(156, 51);
            this.tcheca.Name = "tcheca";
            this.tcheca.Size = new System.Drawing.Size(99, 17);
            this.tcheca.TabIndex = 50;
            this.tcheca.Text = "Activar tiempo";
            this.tcheca.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(153, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "horas";
            // 
            // cminutos
            // 
            this.cminutos.FormattingEnabled = true;
            this.cminutos.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.cminutos.Location = new System.Drawing.Point(208, 24);
            this.cminutos.Name = "cminutos";
            this.cminutos.Size = new System.Drawing.Size(46, 21);
            this.cminutos.TabIndex = 47;
            this.cminutos.SelectedIndexChanged += new System.EventHandler(this.cminutos_SelectedIndexChanged);
            // 
            // choras
            // 
            this.choras.FormattingEnabled = true;
            this.choras.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.choras.Location = new System.Drawing.Point(156, 24);
            this.choras.Name = "choras";
            this.choras.Size = new System.Drawing.Size(46, 21);
            this.choras.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(205, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "minutos";
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(169, 346);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(110, 24);
            this.radButton3.TabIndex = 2;
            this.radButton3.Text = "Guardar";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 382);
            this.Controls.Add(this.radButton3);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.radButton1);
            this.MaximizeBox = false;
            this.Name = "config";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.config_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_lectura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_lectura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_folder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.email)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.admin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cminutos;
        private System.Windows.Forms.ComboBox choras;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox tcheca;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadTextBox txt_folder;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox email;
        private Telerik.WinControls.UI.RadTextBox admin;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton btn_lectura;
        private Telerik.WinControls.UI.RadTextBox txt_lectura;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}
