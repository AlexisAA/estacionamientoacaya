﻿namespace Monitor_SAP
{
    partial class RadForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm1));
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.fech1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.fech2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.fbusca = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lbl_t = new Telerik.WinControls.UI.RadLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.NotificaIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.horas = new Telerik.WinControls.UI.RadTextBox();
            this.minutos = new Telerik.WinControls.UI.RadTextBox();
            this.checa = new System.Windows.Forms.CheckBox();
            this.folder = new Telerik.WinControls.UI.RadTextBox();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.procesados = new Telerik.WinControls.UI.RadTextBox();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rlabeltime = new Telerik.WinControls.UI.RadLabel();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fech1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fech2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fbusca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_t)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procesados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlabeltime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton1
            // 
            this.radButton1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radButton1.Location = new System.Drawing.Point(12, 43);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(194, 24);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Procesar";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(434, 62);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(45, 24);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "crea base de datos";
            this.radButton2.Visible = false;
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radGridView1
            // 
            this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView1.Location = new System.Drawing.Point(27, 92);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.ShowGroupPanel = false;
            this.radGridView1.Size = new System.Drawing.Size(0, 0);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.Click += new System.EventHandler(this.radGridView1_Click);
            // 
            // fech1
            // 
            this.fech1.Location = new System.Drawing.Point(40, 19);
            this.fech1.Name = "fech1";
            this.fech1.Size = new System.Drawing.Size(201, 20);
            this.fech1.TabIndex = 4;
            this.fech1.TabStop = false;
            this.fech1.Text = "sábado, 09 de diciembre de 2017";
            this.fech1.Value = new System.DateTime(2017, 12, 9, 5, 16, 47, 939);
            // 
            // fech2
            // 
            this.fech2.Location = new System.Drawing.Point(268, 19);
            this.fech2.Name = "fech2";
            this.fech2.Size = new System.Drawing.Size(195, 20);
            this.fech2.TabIndex = 5;
            this.fech2.TabStop = false;
            this.fech2.Text = "sábado, 09 de diciembre de 2017";
            this.fech2.Value = new System.DateTime(2017, 12, 9, 5, 16, 47, 939);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(-4, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(25, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Del:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(247, 19);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(15, 18);
            this.radLabel2.TabIndex = 7;
            this.radLabel2.Text = "al";
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(469, 18);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(65, 24);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "Aceptar";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton5
            // 
            this.radButton5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radButton5.Location = new System.Drawing.Point(12, 73);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(194, 24);
            this.radButton5.TabIndex = 10;
            this.radButton5.Text = "Cerrar";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // fbusca
            // 
            this.fbusca.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fbusca.Location = new System.Drawing.Point(12, 17);
            this.fbusca.Name = "fbusca";
            this.fbusca.Size = new System.Drawing.Size(194, 20);
            this.fbusca.TabIndex = 11;
            this.fbusca.TabStop = false;
            this.fbusca.Text = "sábado, 09 de diciembre de 2017";
            this.fbusca.Value = new System.DateTime(2017, 12, 9, 5, 16, 47, 939);
            // 
            // lbl_t
            // 
            this.lbl_t.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_t.Location = new System.Drawing.Point(142, 114);
            this.lbl_t.Name = "lbl_t";
            this.lbl_t.Size = new System.Drawing.Size(2, 2);
            this.lbl_t.TabIndex = 12;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // NotificaIcon
            // 
            this.NotificaIcon.ContextMenuStrip = this.contextMenuStrip1;
            this.NotificaIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotificaIcon.Icon")));
            this.NotificaIcon.Text = "Minimizado";
            this.NotificaIcon.Visible = true;
            this.NotificaIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotificaIcon_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(98, 26);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // radButton4
            // 
            this.radButton4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radButton4.Location = new System.Drawing.Point(12, 103);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(194, 24);
            this.radButton4.TabIndex = 14;
            this.radButton4.Text = "Configuración";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // horas
            // 
            this.horas.Location = new System.Drawing.Point(187, 61);
            this.horas.Name = "horas";
            this.horas.Size = new System.Drawing.Size(31, 20);
            this.horas.TabIndex = 15;
            this.horas.Visible = false;
            // 
            // minutos
            // 
            this.minutos.Location = new System.Drawing.Point(224, 60);
            this.minutos.Name = "minutos";
            this.minutos.Size = new System.Drawing.Size(31, 20);
            this.minutos.TabIndex = 16;
            this.minutos.Visible = false;
            // 
            // checa
            // 
            this.checa.AutoSize = true;
            this.checa.Location = new System.Drawing.Point(262, 62);
            this.checa.Name = "checa";
            this.checa.Size = new System.Drawing.Size(31, 17);
            this.checa.TabIndex = 17;
            this.checa.Text = "c";
            this.checa.UseVisualStyleBackColor = true;
            this.checa.Visible = false;
            // 
            // folder
            // 
            this.folder.Location = new System.Drawing.Point(299, 60);
            this.folder.Name = "folder";
            this.folder.Size = new System.Drawing.Size(63, 20);
            this.folder.TabIndex = 18;
            this.folder.Visible = false;
            // 
            // radButton6
            // 
            this.radButton6.Location = new System.Drawing.Point(256, 247);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(75, 24);
            this.radButton6.TabIndex = 19;
            this.radButton6.Text = "enviar mail";
            this.radButton6.Visible = false;
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click_1);
            // 
            // radButton7
            // 
            this.radButton7.Location = new System.Drawing.Point(337, 247);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(80, 24);
            this.radButton7.TabIndex = 20;
            this.radButton7.Text = "ller txt";
            this.radButton7.Visible = false;
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(37, 244);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Exportar a Excel";
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // procesados
            // 
            this.procesados.Location = new System.Drawing.Point(368, 61);
            this.procesados.Name = "procesados";
            this.procesados.Size = new System.Drawing.Size(63, 20);
            this.procesados.TabIndex = 23;
            this.procesados.Visible = false;
            // 
            // radButton8
            // 
            this.radButton8.Location = new System.Drawing.Point(434, 247);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(61, 24);
            this.radButton8.TabIndex = 24;
            this.radButton8.Text = "radButton8";
            this.radButton8.Visible = false;
            this.radButton8.Click += new System.EventHandler(this.radButton8_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radGridView1);
            this.panel1.Controls.Add(this.radButton3);
            this.panel1.Controls.Add(this.procesados);
            this.panel1.Controls.Add(this.radButton8);
            this.panel1.Controls.Add(this.folder);
            this.panel1.Controls.Add(this.fech1);
            this.panel1.Controls.Add(this.checa);
            this.panel1.Controls.Add(this.radButton7);
            this.panel1.Controls.Add(this.minutos);
            this.panel1.Controls.Add(this.fech2);
            this.panel1.Controls.Add(this.horas);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radButton6);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radButton2);
            this.panel1.Location = new System.Drawing.Point(612, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(92, 56);
            this.panel1.TabIndex = 25;
            // 
            // rlabeltime
            // 
            this.rlabeltime.Location = new System.Drawing.Point(12, 147);
            this.rlabeltime.Name = "rlabeltime";
            this.rlabeltime.Size = new System.Drawing.Size(55, 18);
            this.rlabeltime.TabIndex = 0;
            this.rlabeltime.Text = "radLabel3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 13);
            this.button1.TabIndex = 26;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RadForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 187);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rlabeltime);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radButton4);
            this.Controls.Add(this.lbl_t);
            this.Controls.Add(this.fbusca);
            this.Controls.Add(this.radButton5);
            this.Controls.Add(this.radButton1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RadForm1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Monitor Portal de Facturación - SAP";
            this.Load += new System.EventHandler(this.RadForm1_Load);
            this.Resize += new System.EventHandler(this.RadForm1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fech1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fech2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fbusca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_t)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.folder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procesados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rlabeltime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadDateTimePicker fech1;
        private Telerik.WinControls.UI.RadDateTimePicker fech2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadDateTimePicker fbusca;
        private Telerik.WinControls.UI.RadLabel lbl_t;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon NotificaIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadTextBox horas;
        private Telerik.WinControls.UI.RadTextBox minutos;
        private System.Windows.Forms.CheckBox checa;
        private Telerik.WinControls.UI.RadTextBox folder;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadButton radButton7;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadTextBox procesados;
        private Telerik.WinControls.UI.RadButton radButton8;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel rlabeltime;
        private System.Windows.Forms.Button button1;
    }
}