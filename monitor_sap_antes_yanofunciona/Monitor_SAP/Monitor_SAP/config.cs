﻿using System;
using System.Windows.Forms;

namespace Monitor_SAP
{
    public partial class config : Telerik.WinControls.UI.RadForm
    {
        public config()
        {
            InitializeComponent();
        }

        public string phora { get; set; }
        public string pminuto { get; set; }
        public string pcheck { get; set; }
        public string pfolder { get; set; }

        private void radButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cminutos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderBrowserDialog
            {
                //Description = Resources.DefaultLocationDialogHelp,
                Description = "Seleccione la carpeta donde se alojarán los archivos.",
                ShowNewFolderButton = true
            };

            if (DialogResult.OK == folderDialog.ShowDialog())
            {
                txt_folder.Text = folderDialog.SelectedPath;
            }
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            if (txt_folder.Text == "")
            {
                MessageBox.Show("Por favor capture la ruta donde se almacenarán los archivos");
                return;
            }
            if (admin.Text == "")
            {
                MessageBox.Show("Por favor capture el nombre del Administrador");
                return;
            }
            if (email.Text == "")
            {
                MessageBox.Show("Por favor capture el e-mail del administrador");
                return;
            }
            if (txt_lectura.Text == "")
            {
                MessageBox.Show("Por favor capture la ruta donde se almacenarán los archivos");
                return;
            }
            if (tcheca.Checked)
            {
                if (choras.Text == "")
                {
                    MessageBox.Show("Por favor capturela hora");
                    return;
                }
                if (cminutos.Text == "")
                {
                    MessageBox.Show("Por favor capture los minutos");
                    return;
                }
            }

                var MyIni = new IniFile("Settings.ini");


            MyIni.Write("sourcefile", txt_folder.Text);
            MyIni.Write("hora", choras.Text);
            MyIni.Write("minuto", cminutos.Text);
            MyIni.Write("admin", admin.Text);
            MyIni.Write("email", email.Text);
            MyIni.Write("lectura_procesados", txt_lectura.Text);
            if (tcheca.Checked)
            {
                MyIni.Write("checa", "1");
                pcheck = "1";
            }
            else
            {
                MyIni.Write("checa", "0");
                pcheck = "0";
            }

            phora = choras.Text;
            pminuto = cminutos.Text;
            pfolder = txt_folder.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void config_Load(object sender, EventArgs e)
        {
            IniFile inif = new IniFile("Settings.ini");
            String tfuente = inif.Read("sourcefile");            
            string xhora = inif.Read("hora");
            string xminuto = inif.Read("minuto");
            string xcheca = inif.Read("checa");
            string xadmin = inif.Read("admin");
            string xemail = inif.Read("email");
            string xproce = inif.Read("lectura_procesados");
            
            txt_folder.Text = tfuente;
            choras.Text = xhora;
            cminutos.Text = xminuto;
            admin.Text = xadmin;
            email.Text = xemail;
            txt_lectura.Text = xproce;
            if (xcheca == "1")
            {
                tcheca.Checked = true;
            }
            else
            {
                tcheca.Checked = false;
            }


        }

        private void btn_lectura_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderBrowserDialog
            {
                //Description = Resources.DefaultLocationDialogHelp,
                Description = "Seleccione la carpeta de donde se leeran los archivos.",
                ShowNewFolderButton = true
            };

            if (DialogResult.OK == folderDialog.ShowDialog())
            {
                txt_lectura.Text = folderDialog.SelectedPath;
            }
        }

        private void radScrollablePanel1_Click(object sender, EventArgs e)
        {

        }
    }
}
