﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.Export;
using System.Net.Mail;



namespace Monitor_SAP
{
    public partial class RadForm1 : Telerik.WinControls.UI.RadForm
    {

        //private SQLiteDataAdapter DB;
        //private DataSet DS = new DataSet();
        //private DataTable DT = new DataTable();
        //string pata = Application.StartupPath + "\\facturacion.sqlite";  //ExecutablePath;
        int tiemposegundos = 0;

        public RadForm1()
        {
            InitializeComponent();
        }

        private void RadForm1_Load(object sender, EventArgs e)
        {
            fech1.Text = DateTime.Now.ToString();
            fech2.Text = DateTime.Now.ToString();

            System.DateTime today = System.DateTime.Now;
            System.DateTime answer = today.AddDays(-1);

            fbusca.Text = answer.ToString();

            IniFile inif = new IniFile("Settings.ini");

            horas.Text = inif.Read("hora");
            minutos.Text = inif.Read("minuto");

            folder.Text = inif.Read("sourcefile");
            procesados.Text = inif.Read("lectura_procesados");

            if (inif.Read("checa") == "1")
            {
                checa.Checked = true;
            }
            else
            {
                checa.Checked = false;
            }
        }

        public void updatehor()
        {
            IniFile inif = new IniFile("Settings.ini");

            horas.Text = inif.Read("hora");
        }

        private void RadForm1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                NotificaIcon.Visible = true; ;
                NotificaIcon.BalloonTipText = "Minimizado a notificaciones";
                NotificaIcon.BalloonTipTitle = "Descarga de Archivos";
                NotificaIcon.BalloonTipIcon = ToolTipIcon.Info;
                NotificaIcon.ShowBalloonTip(5000);
            }
        }


        private void radButton1_Click(object sender, EventArgs e)
        {
            preocess();
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            //SQLiteConnection.CreateFile(pata);

            //SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + pata + "; Version=3;");
            //m_dbConnection.Open();

            //string sql = "create table facturas(id_sistema varchar(20), tipo varchar(2),rfc varchar(13),Razon_social varchar(50),Fecha_Factura datetime,UUID varchar(36) UNIQUE,iva decimal(16,2),importe decimal(16,2))";
            //SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //command.ExecuteNonQuery();

            //string sql1 = "create table log(id INTEGER PRIMARY KEY AUTOINCREMENT, fecha varchar(20),error varchar(50),comentario varchar(150))";
            //SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            //command1.ExecuteNonQuery();

            //m_dbConnection.Close();
        }


        private void Add(string id_sistema, string tipo, string rfc, string Razon_social, string Fecha_Factura, string UUID, string iva, string importe)
        {
            //try
            //{

            //    SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + pata + "; Version=3;");
            //    m_dbConnection.Open();

            //    string sql = "insert into facturas(id_sistema,tipo,rfc,Razon_social,Fecha_Factura,UUID,iva,importe) values ('" + id_sistema + "','" + tipo + "','" + rfc + "','" + Razon_social + "','" + Fecha_Factura + "','" + UUID + "'," + iva + "," + importe + ")";

            //    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //    command.ExecuteNonQuery();


            //    m_dbConnection.Close();
            //}
            //catch (Exception e)
            //{
            //    Addlog(e.Message, UUID);
            //}
        }

        private void Addlog(string error, string comentario)
        {
            //try
            //{
            //    SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + pata + "; Version=3;");
            //    m_dbConnection.Open();
            //    string fecha = DateTime.Now.ToString();
            //    string sql = "insert into log(fecha,error,comentario) values ('" + fecha + "','" + error + "','" + comentario + "')";

            //    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //    command.ExecuteNonQuery();


            //    m_dbConnection.Close();
            //}
            //catch (Exception e)
            //{
            //    MessageBox.Show(e.Message);
            //}
        }

        private void buscareg()
        {

            //radGridView1.DataSource = null;
            //this.radGridView1.Rows.Clear();
            //this.radGridView1.SummaryRowsBottom.Clear();

            //SQLiteCommand sql_cmd;
            //SQLiteDataAdapter DB;
            //DataTable DT = new DataTable();

            //SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + pata + "; Version=3;");
            //m_dbConnection.Open();

            //sql_cmd = m_dbConnection.CreateCommand();


            //string fech1a = fech1.Value.ToString("yyyyMMdd");
            //string fech2a = fech2.Value.ToString("yyyyMMdd");

            //string sql = "select * from facturas where Fecha_Factura BETWEEN '" + fech1a + "' AND '" + fech2a + "'";

            //DB = new SQLiteDataAdapter(sql, m_dbConnection);
            //DB.Fill(DT);

            //radGridView1.DataSource = DT;

            //radGridView1.Columns[6].TextAlignment = ContentAlignment.MiddleRight;
            //radGridView1.Columns[7].TextAlignment = ContentAlignment.MiddleRight;

            //this.radGridView1.Columns[4].FormatString = "{0:dd/MM/yyyy}";

            //this.radGridView1.Columns[6].FormatString = "{0:$#,###0.00;($#,###0.00);0}";
            //this.radGridView1.Columns[7].FormatString = "{0:$#,###0.00;($#,###0.00);0}";

            //GridViewSummaryRowItem sumary = new GridViewSummaryRowItem();
            //sumary.Add(new GridViewSummaryItem("importe", "{0:$#,###0.00;($#,###0.00);0}", GridAggregateFunction.Sum));
            //sumary.Add(new GridViewSummaryItem("iva", "{0:$#,###0.00;($#,###0.00);0}", GridAggregateFunction.Sum));
            //this.radGridView1.SummaryRowsBottom.Add(sumary);

            //radGridView1.BestFitColumns();

            //if (DT.Rows.Count > 0)
            //{
            //    lbl_result.Text = "Total de registros " + DT.Rows.Count;
            //}
            //else
            //{
            //    lbl_result.Text = "No existen registros";
            //}

            //// Clean up
            //m_dbConnection.Close();

        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            //buscareg();
        }



        private void radButton5_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            rlabeltime.Text = DateTime.Now.ToString();
            tiemposegundos = tiemposegundos + 1;
            if (tiemposegundos == 1800)
            {
                tiemposegundos = 0;
                procesaarchivos();
            }

            if (tiemposegundos > 1800)
            {
                tiemposegundos = 0;
            }

            if (checa.Checked)
            {
                int hr = DateTime.Now.Hour;
                int min = DateTime.Now.Minute;
                int sec = DateTime.Now.Second;
                int hrb = int.Parse(horas.Text.ToString());
                int minb = int.Parse(minutos.Text);
                if (hr == hrb && min == minb && sec == 0)
                {
                    timer1.Enabled = false;
                    preocesstiempo();
                    Application.DoEvents();
                    timer1.Enabled = true;
                }
            }
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

            NotificaIcon.Visible = false;
        }

        private void NotificaIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

            NotificaIcon.Visible = false;
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            config f = new Monitor_SAP.config();

            if (f.ShowDialog() == DialogResult.OK)
            {

                horas.Text = f.phora;
                minutos.Text = f.pminuto;
                folder.Text = f.pfolder;
                if (f.pcheck == "1")
                {
                    checa.Checked = true;
                }
                else
                {
                    checa.Checked = false;
                }

            }
        }

        private void radGridView1_Click(object sender, EventArgs e)
        {

        }

        private void generaArchivo(string fecha1, string fecha2)
        {
            //string respuesta = "";
            ////SQLiteCommand sql_cmd;
            ////SQLiteDataAdapter DB;
            ////DataTable DT = new DataTable();

            //DateTime dta = DateTime.Parse(fecha1.ToString());
            //string ok = dta.ToString("yyyyMMdd");
            //string filename = dta.ToString("yyyyMMdd");

            

            //foreach (DataRow row in DT.Rows)
            //{
            //    DateTime feca = DateTime.Parse(row[4].ToString());
            //    string zfeca = feca.ToString("yyyyMMdd");
            //    string ziva = decimal.Parse(row[6].ToString()).ToString("#0.00");
            //    string zimporte = decimal.Parse(row[7].ToString()).ToString("#0.00");

            //    respuesta += row[0].ToString() + "|" + row[1].ToString() + "|" + row[2].ToString() + "|" + row[3].ToString() + "|" + zfeca + "|" + row[5].ToString() + "|" + ziva + "|" + zimporte + Environment.NewLine;
            //}

            ////string path = @"C:\FacturasPortal\Procesadas\" + filename + ".txt";
            //string path = @folder.Text + "\\" + filename + ".txt";
            //if (respuesta != "")
            //{
            //    if (!File.Exists(path))
            //    {
            //        File.WriteAllText(path, respuesta);
            //        lbl_result.Text = "Total de registros " + DT.Rows.Count;
            //    }
            //    else
            //    {
            //        File.Delete(path);
            //        Application.DoEvents();
            //        File.WriteAllText(path, respuesta);
            //        lbl_result.Text = "Total de registros " + DT.Rows.Count;
            //    }
            //}
            //else
            //{
            //    lbl_result.Text = "No existen registros";
            //}


        }

        private void radButton6_Click(object sender, EventArgs e)
        {

        }

        private void radButton6_Click_1(object sender, EventArgs e)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.1and1.mx");

                mail.From = new MailAddress("factura@espacia.mx");
                mail.To.Add("jtrume@gmail.com");
                mail.Subject = "Ocurrio un error";
                mail.Body = "Ocurrio un error en la transferencia del archivo de polizas al portal de facturación archivo ";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("factura@espacia.mx", "Facturacion2017");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            int counter = 0;
            string line;
 
            System.IO.StreamReader file =
                new System.IO.StreamReader(@"C:\FacturasPortal\20171201.txt");
            while ((line = file.ReadLine()) != null)
            {
                System.Console.WriteLine(line);
                counter++;
            }

            file.Close();
            System.Console.WriteLine("There were {0} lines.", counter);
            // Suspend the screen.  
            System.Console.ReadLine();
        }



        private void pictureBox1_Click(object sender, EventArgs e)
        {

            int cuantos = radGridView1.MasterView.Rows.Count();


            if (cuantos > 0)
            {
                Stream myStream;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Archivo de Excel|*.xmls";
                saveFileDialog1.Title = "Guardar archivo de Excel";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        GridViewSpreadExport spreadExporter = new GridViewSpreadExport(this.radGridView1);
                        spreadExporter.FreezeHeaderRow = true;
                        spreadExporter.FreezePinnedColumns = true;
                        spreadExporter.FreezePinnedRows = true;
                        spreadExporter.SheetMaxRows = ExcelMaxRows._1048576;
                        spreadExporter.ExportFormat = SpreadExportFormat.Xlsx;
                        spreadExporter.FileExportMode = FileExportMode.CreateOrOverrideFile;
                        spreadExporter.RunExport(@saveFileDialog1.FileName, new SpreadExportRenderer());
                        myStream.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("No existen registros para generar el reporte");
            }

        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pictureBox1, "YExportar a Excel");
        }

        private void radButton8_Click(object sender, EventArgs e)
        {
            procesaarchivos();
        }

        public void leefile(string archivo)
        {
            int counter = 0;
            string line; 
            System.IO.StreamReader file =
                new System.IO.StreamReader(procesados.Text + "\\" + archivo);
            string pasavar = "";
            pasavar += "{'results':[";
            while ((line = file.ReadLine()) != null)
            {
                string s = line;
                string[] words = s.Split('|');
                string xuuid = words[0];
                string CODEJ = words[1];
                string MSGEJ = words[2];
                string POLIZA = words[3];
                string EJERCICIO = words[4];
                pasavar += "{'uuid':'" + xuuid + "','CODEJ':'" + CODEJ + "','MSGEJ':'" + MSGEJ + "','POLIZA':'" + POLIZA + "','EJERCICIO':'" + EJERCICIO + "'},";
                //savepoli(xuuid, CODEJ, MSGEJ, POLIZA, EJERCICIO);
                System.Console.WriteLine(line);
                counter++;
            }
            pasavar = pasavar.TrimEnd(',');
            pasavar += "]}";
            wsservice.ws_sap soli = new wsservice.ws_sap();

            //ws_sapSoapClient soli = new ws_sapSoapClient();
            bool sino = soli.recipol(pasavar);
            file.Close();
            if (sino)
            {

                File.Move(procesados.Text + "\\" + archivo, procesados.Text + "\\Enviadas\\" + archivo);
            }
            else
            {
                File.Move(procesados.Text + "\\" + archivo, procesados.Text + "\\Error\\" + archivo);
                sendemail(archivo);
            }

           
        }

        public void savepoli(string uuid, string CODEJ, string MSGEJ, string POLIZA, string EJERCICIO)
        {
            //try
            //{
            //    SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + pata + "; Version=3;");
            //    m_dbConnection.Open();
            //    string fecha = DateTime.Now.ToString();
            //    string sql = "insert into contabilizados(uuid,codej,msgej,poliza,ejercicio) values ('" + uuid + "','" + CODEJ + "','" + MSGEJ + "','" + POLIZA + "','" + EJERCICIO + "')";

            //    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            //    command.ExecuteNonQuery();

            //    m_dbConnection.Close();
            //}
            //catch { }
        }

        public void sendemail(string archivo)
        {
            try
            {
                IniFile inif = new IniFile("Settings.ini");

                string nameadmin = inif.Read("admin");
                string correoadmin = inif.Read("email");

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.1and1.mx");

                mail.From = new MailAddress("factura@espacia.mx");
                mail.To.Add(correoadmin);
                mail.Subject = "Ocurrio un error";
                mail.Body = "Estimado (a) " + nameadmin + " Ocurrio un error en la transferencia del archivo de polizas al portal de facturación archivo: " + archivo;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("factura@espacia.mx", "Facturacion2017");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("mail Send");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        public void procesaarchivos()
        {
            DirectoryInfo di = new DirectoryInfo(procesados.Text);
            var fia = di.GetFiles();
            int cuantos = fia.Count();
            if (cuantos > 0)
            {
                foreach (var fi in di.GetFiles())
                {
                    leefile(fi.Name);
                }
            }
        }

        public void conectasrv()
        {
            System.DateTime today = System.DateTime.Now;
            System.DateTime answer = today.AddDays(-1);
            fbusca.Text = answer.ToString();

            lbl_t.Text = "";
            //lbl_result.Text = "";
            string fb1 = fbusca.Value.ToString("yyyy-MM-dd 00:00");
            string fb2 = fbusca.Value.ToString("yyyy-MM-dd 23:59");

            wsservice.ws_sap soli = new wsservice.ws_sap();

            //ws_sapSoapClient soli = new ws_sapSoapClient();
            var respu = soli.regresa(fb1, fb2);
            

            DateTime dt = DateTime.Parse(fbusca.Value.ToString("yyyy-MM-dd 00:00"));
            string ok = dt.ToString("yyyyMMdd");
            string filename = dt.ToString("yyyyMMdd");

            var jsonlist = JsonConvert.DeserializeObject<List<facturas>>(respu);
            for (int i = 0; i < jsonlist.Count; i++)
            {
                Add(jsonlist[i].id_sistema, jsonlist[i].tipo, jsonlist[i].rfc, jsonlist[i].Razon_social, jsonlist[i].Fecha_Factura, jsonlist[i].UUID, jsonlist[i].iva, jsonlist[i].Importe);
            }

            generaArchivo(fb1, fb2);
        }

        public void preocess()
        {
                 
            lbl_t.Text = "";
            string fb1 = fbusca.Value.ToString("yyyy-MM-dd 00:00");
            string fb2 = fbusca.Value.ToString("yyyy-MM-dd 23:59");

            wsservice.ws_sap servi = new wsservice.ws_sap();
            var respu = servi.regresa(fb1,fb2);


            DateTime dt = DateTime.Parse(fbusca.Value.ToString("yyyy-MM-dd 00:00"));
            string ok = dt.ToString("yyyyMMdd"); 
            string filename = dt.ToString("yyyyMMdd");

            var jsonlist = JsonConvert.DeserializeObject<List<facturas>>(respu);

            string path = @folder.Text + "\\" + filename + ".txt";
            using (var sw = new StreamWriter(path))
            {
                for (var i = 0; i < jsonlist.Count; i++)
                {
                    sw.WriteLine(jsonlist[i].id_sistema + "|" + jsonlist[i].tipo + "|" + jsonlist[i].rfc + "|" + jsonlist[i].Razon_social + "|" + jsonlist[i].Fecha_Factura + "|" + jsonlist[i].UUID + "|" + jsonlist[i].iva + "|" + jsonlist[i].Importe);
                }
            }           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            preocesstiempo();
        }

        public void preocesstiempo()
        {
            DateTime diaanterior = DateTime.Now.AddDays(-1);
            fbusca.Value = diaanterior;
            lbl_t.Text = "";
            string fb1 = fbusca.Value.ToString("yyyy-MM-dd 00:00");
            string fb2 = fbusca.Value.ToString("yyyy-MM-dd 23:59");

            //ws_sapSoapClient soli = new ws_sapSoapClient();
            wsservice.ws_sap soli = new wsservice.ws_sap();
            var respu = soli.regresa(fb1, fb2);

            DateTime dt = DateTime.Parse(fbusca.Value.ToString("yyyy-MM-dd 00:00"));
            string ok = dt.ToString("yyyyMMdd");
            string filename = dt.ToString("yyyyMMdd");

            var jsonlist = JsonConvert.DeserializeObject<List<facturas>>(respu);

            string path = @folder.Text + "\\" + filename + ".txt";
            using (var sw = new StreamWriter(path))
            {
                for (var i = 0; i < jsonlist.Count; i++)
                {
                    sw.WriteLine(jsonlist[i].id_sistema + "|" + jsonlist[i].tipo + "|" + jsonlist[i].rfc + "|" + jsonlist[i].Razon_social + "|" + jsonlist[i].Fecha_Factura + "|" + jsonlist[i].UUID + "|" + jsonlist[i].iva + "|" + jsonlist[i].Importe);
                }
            }
        }
    }
}


public class facturas
{
    public string id_sistema { get; set; }
    public string tipo { get; set; }
    public string rfc { get; set; }
    public string Razon_social { get; set; }
    public string Fecha_Factura { get; set; }
    public string UUID { get; set; }
    public string iva { get; set; }
    public string Importe { get; set; }
}