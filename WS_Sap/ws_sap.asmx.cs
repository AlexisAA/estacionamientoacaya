﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data;

namespace WS_Sap
{
    /// <summary>
    /// Descripción breve de ws_sap
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ws_sap : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string reporte()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string ultimos()
        {

            List<ultimost> listultimos = new List<ultimost>();



            string cs = ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_verifica";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ultimost Tickets = new ultimost();
                    Tickets.id_sistema = rdr["Nombre_Sistema"].ToString();

                    Tickets.fecha = rdr["Fecha_Ticket"].ToString();

                    listultimos.Add(Tickets);
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            return new JavaScriptSerializer().Serialize(listultimos);            
        }

        [WebMethod]
        public string regresa(string fecha_ini, string fecha_fin)
        {
            List<facturas> listfactu = new List<facturas>();



            string cs = ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_RepSAP1";
                cmd.Parameters.AddWithValue("@Fecha_Inicio", fecha_ini);
                cmd.Parameters.AddWithValue("@Fecha_Fin", fecha_fin);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    facturas factur = new facturas();
                    factur.id_sistema = rdr["id_sistema"].ToString();
                    string tipofactura = rdr["tipo"].ToString();
                    if (tipofactura == "App de estacionamientos")
                    {
                        factur.tipo = "04";
                    }
                    else if (tipofactura == "Portal de Facturación Carga de archivos")
                    {
                        factur.tipo = "03";
                    }
                    else if (tipofactura == "Portal de Facturación Publico en general")
                    {
                        factur.tipo = "02";
                    }                   
                    else
                    {
                        factur.tipo = "01";
                    }
                    factur.rfc = rdr["rfc"].ToString();
                    factur.Razon_social = rdr["Razon_social"].ToString();
                    DateTime dt = DateTime.Parse(rdr["Fecha_Factura"].ToString()); //"2017-11-29 00:00";
                    string ok = dt.ToString("yyyyMMdd");
                    factur.Fecha_Factura = dt.ToString("yyyyMMdd");
                    factur.UUID = rdr["UUID"].ToString();
                    factur.iva = rdr["iva"].ToString();
                    factur.Importe = rdr["Importe"].ToString();
                    listfactu.Add(factur);
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            return new JavaScriptSerializer().Serialize(listfactu);
        }


        [WebMethod]
        public bool recipol(string poliza)
        {

            DataTable dt2 = new DataTable();
            try
            {
                var json = poliza;
                dt2 = Tabulate(json);

                foreach (DataRow row in dt2.Rows)
                {
                    string xuuid = row["uuid"].ToString();
                    int xCODEJ = Int32.Parse(row["CODEJ"].ToString());
                    string MSGEJ = row["MSGEJ"].ToString();
                    int POLIZA = Int32.Parse(row["POLIZA"].ToString());
                    int EJERCICIO = Int32.Parse(row["EJERCICIO"].ToString());
                    GuardaReg(xuuid, xCODEJ, MSGEJ, POLIZA, EJERCICIO);
                }
                return true;
            }             
            catch {
                return false;
            }
           
        }

        public static DataTable Tabulate(string json)
        {
            var jsonLinq = JObject.Parse(json);

            // Find the first array using Linq
            var srcArray = jsonLinq.Descendants().Where(d => d is JArray).First();
            var trgArray = new JArray();
            foreach (JObject row in srcArray.Children<JObject>())
            {
                var cleanRow = new JObject();
                foreach (JProperty column in row.Properties())
                {
                    // Only include JValue types
                    if (column.Value is JValue)
                    {
                        cleanRow.Add(column.Name, column.Value);
                    }
                }

                trgArray.Add(cleanRow);
            }

            return JsonConvert.DeserializeObject<DataTable>(trgArray.ToString());
        }

        private void GuardaReg(string uuid,int CODEJ,string MSGEJ, int POLIZA,int EJERCICIO)
        {
            string cs = ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_polizas";
                cmd.Parameters.AddWithValue("@uuid", uuid);
                cmd.Parameters.AddWithValue("@CODEJ", CODEJ);
                cmd.Parameters.AddWithValue("@MSGEJ", MSGEJ);
                cmd.Parameters.AddWithValue("@POLIZA", POLIZA);
                cmd.Parameters.AddWithValue("@EJERCICIO", EJERCICIO);
                con.Open();
                cmd.ExecuteReader();
            }
        }

        [WebMethod]
        public string AutoIngre(string dia, string mes,string anio)
        {
            List<autoingresos> listfactu = new List<autoingresos>();

            string xticket = string.Empty;

            string cs = ConfigurationManager.ConnectionStrings["Espacia_ConnString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "SP_MONTOS_FACTURACION_DAY";
                cmd.Parameters.AddWithValue("@DIA", dia);
                cmd.Parameters.AddWithValue("@MES", mes);
                cmd.Parameters.AddWithValue("@AÑO", anio);
                con.Open();
                //cmd.ExecuteNonQuery();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    autoingresos factur = new autoingresos();
                    factur.fecha = dia+"."+mes+"."+anio;
                    if (rdr["tickets"].ToString() == "")
                    {
                        xticket = "0";
                    }
                    else {
                        xticket = rdr["tickets"].ToString();
                    }
                    factur.nombre = rdr["alias"].ToString().Trim();
                    factur.tickets = xticket;
                    
                    listfactu.Add(factur);
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            return new JavaScriptSerializer().Serialize(listfactu);
        }
    }
}


public class results
{
    public string xuuid { get; set; }
    public string CODEJ { get; set; }
    public string MSGEJ { get; set; }
    public string poliza { get; set; }
    public string ejercicio { get; set; }
}

public class facturas
{
    public string id_sistema { get; set; }
    public string tipo { get; set; }
    public string rfc { get; set; }
    public string Razon_social { get; set; }
    public string Fecha_Factura { get; set; }
    public string UUID { get; set; }
    public string iva { get; set; }
    public string Importe { get; set; }
}

public class ultimost
{
    public string id_sistema { get; set; }
    public string fecha { get; set; }
}

public class autoingresos
{
    public string fecha { get; set; }
    public string nombre { get; set; }
    public string tickets { get; set; }
    
   
}
