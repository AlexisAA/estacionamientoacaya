﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Catalogos
{
    /// <summary>
    /// Clase Tickets
    /// </summary>
    public class Tickets : IBean
    {

        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Tickets());
        private Dictionary<string, object> _values;

        private string _no_Ticket;
        private string _iD_Sistema;
        private string _nombre_Sistema;
        private DateTime _fecha_Ticket;
        private decimal _importe_Total;
        private string _forma_Pago;
        private string _no_Tarjeta;
        private DateTime _fecha_Carga;
        #endregion

        #region Constructor
        public Tickets()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public string No_Ticket
        {
            get { return _no_Ticket; }
            set { _no_Ticket = value; Values["No_Ticket"] = value; }
        }
        public string ID_Sistema
        {
            get { return _iD_Sistema; }
            set { _iD_Sistema = value; Values["ID_Sistema"] = value; }
        }
        public string Nombre_Sistema
        {
            get { return _nombre_Sistema; }
            set { _nombre_Sistema = value; Values["Nombre_Sistema"] = value; }
        }
        public DateTime Fecha_Ticket
        {
            get { return _fecha_Ticket; }
            set { _fecha_Ticket = value; Values["Fecha_Ticket"] = value; }
        }
        public decimal Importe_Total
        {
            get { return _importe_Total; }
            set { _importe_Total = value; Values["Importe_Total"] = value; }
        }
        public string Forma_Pago
        {
            get { return _forma_Pago; }
            set { _forma_Pago = value; Values["Forma_Pago"] = value; }
        }
        public string No_Tarjeta
        {
            get { return _no_Tarjeta; }
            set { _no_Tarjeta = value; Values["No_Tarjeta"] = value; }
        }
        public DateTime Fecha_Carga
        {
            get { return _fecha_Carga; }
            set { _fecha_Carga = value; Values["Fecha_Carga"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("No_Ticket"))
                No_Ticket = Convert.ToString(values["No_Ticket"]);
            if (values.ContainsKey("ID_Sistema"))
                ID_Sistema = Convert.ToString(values["ID_Sistema"]);
            if (values.ContainsKey("Nombre_Sistema"))
                Nombre_Sistema = Convert.ToString(values["Nombre_Sistema"]);
            if (values.ContainsKey("Fecha_Ticket"))
                Fecha_Ticket = Convert.ToDateTime(values["Fecha_Ticket"]);
            if (values.ContainsKey("Importe_Total"))
                Importe_Total = Convert.ToDecimal(values["Importe_Total"]);
            if (values.ContainsKey("Forma_Pago"))
                Forma_Pago = Convert.ToString(values["Forma_Pago"]);
            if (values.ContainsKey("No_Tarjeta"))
                No_Tarjeta = Convert.ToString(values["No_Tarjeta"]);
            if (values.ContainsKey("Fecha_Carga"))
                Fecha_Carga = Convert.ToDateTime(values["Fecha_Carga"]);
        }

        #endregion

    }
}
