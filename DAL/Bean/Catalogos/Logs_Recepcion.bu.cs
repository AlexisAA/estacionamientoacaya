﻿using System.Collections.Generic;

using DAL.DataAccess.Catalogos;

namespace DAL.Bean.Catalogos
{
    public class BuLogs_Recepcion
    {
        #region Metodos Standar
        public static List<Logs_Recepcion> Buscar(Logs_Recepcion lr)
        {
            return DALLogs_Recepcion.Instance.Buscar(lr);
        }
        public static bool Eliminar(Logs_Recepcion lr)
        {
            return DALLogs_Recepcion.Instance.Eliminar(lr);
        }
        public static bool Actualizar(Logs_Recepcion lr)
        {
            return DALLogs_Recepcion.Instance.Actualizar(lr);
        }
        public static bool Insertar(Logs_Recepcion lr)
        {
            return DALLogs_Recepcion.Instance.Insertar(lr);
        }
        #endregion
    }
}
