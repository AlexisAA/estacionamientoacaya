﻿using System.Collections.Generic;

using DAL.DataAccess.Catalogos;

namespace DAL.Bean.Catalogos
{
    public class BULogs
    {
        #region Metodos Standar
        public static List<Logs> Buscar(Logs logs)
        {
            return DALLogs.Instance.Buscar(logs);
        }
        public static bool Eliminar(Logs logs)
        {
            return DALLogs.Instance.Eliminar(logs);
        }
        public static bool Actualizar(Logs logs)
        {
            return DALLogs.Instance.Actualizar(logs);
        }
        public static bool Insertar(Logs logs)
        {
            return DALLogs.Instance.Insertar(logs);
        }
        #endregion
    }
}
