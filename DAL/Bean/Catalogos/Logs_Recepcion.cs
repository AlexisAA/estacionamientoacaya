﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Catalogos
{
    /// <summary>
    /// Clase Usuarios
    /// </summary>
    public class Logs_Recepcion : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Logs_Recepcion());
        private Dictionary<string, object> _values;

        private int _id_Log_Recepcion;
        private int _id_Usuario;
        private DateTime _fecha;
        private int _reg_Recibidos;
        private int _reg_Insertados;
        private int _reg_Actualizados;
        private int _reg_Error;
        private string _observaciones;
        #endregion

        #region Constructor
        public Logs_Recepcion()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int Id_Log_Recepcion
        {
            get { return _id_Log_Recepcion; }
            set { _id_Log_Recepcion = value; Values["Id_Log_Recepcion"] = value; }
        }
        public int Id_Usuario
        {
            get { return _id_Usuario; }
            set { _id_Usuario = value; Values["Id_Usuario"] = value; }
        }
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; Values["Fecha"] = value; }
        }
        public int Reg_Recibidos
        {
            get { return _reg_Recibidos; }
            set { _reg_Recibidos = value; Values["Reg_Recibidos"] = value; }
        }
        public int Reg_Insertados
        {
            get { return _reg_Insertados; }
            set { _reg_Insertados = value; Values["Reg_Insertados"] = value; }
        }
        public int Reg_Actualizados
        {
            get { return _reg_Actualizados; }
            set { _reg_Actualizados = value; Values["Reg_Actualizados"] = value; }
        }
        public int Reg_Error
        {
            get { return _reg_Error; }
            set { _reg_Error = value; Values["Reg_Error"] = value; }
        }
        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; Values["Observaciones"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("Id_Log_Recepcion"))
                Id_Log_Recepcion = Convert.ToInt32(values["Id_Log_Recepcion"]);
            if (values.ContainsKey("Id_Usuario"))
                Id_Usuario = Convert.ToInt32(values["Id_Usuario"]);
            if (values.ContainsKey("Fecha"))
                Fecha = Convert.ToDateTime(values["Fecha"]);
            if (values.ContainsKey("Reg_Recibidos"))
                Reg_Recibidos = Convert.ToInt32(values["Reg_Recibidos"]);
            if (values.ContainsKey("Reg_Insertados"))
                Reg_Insertados = Convert.ToInt32(values["Reg_Insertados"]);
            if (values.ContainsKey("Reg_Actualizados"))
                Reg_Actualizados = Convert.ToInt32(values["Reg_Actualizados"]);
            if (values.ContainsKey("Reg_Error"))
                Reg_Error = Convert.ToInt32(values["Reg_Error"]);
            if (values.ContainsKey("Observaciones"))
                Observaciones = Convert.ToString(values["Observaciones"]);
        }

        #endregion

    }
}
