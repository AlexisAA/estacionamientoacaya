﻿using System.Collections.Generic;

using DAL.DataAccess.Catalogos;

namespace DAL.Bean.Catalogos
{
    public class BuTickets
    {
        #region Metodos Standar
        public static List<Tickets> Buscar(Tickets ticket)
        {
            return DALTickets.Instance.Buscar(ticket);
        }
        public static bool Eliminar(Tickets ticket)
        {
            return DALTickets.Instance.Eliminar(ticket);
        }
        public static bool Actualizar(Tickets ticket)
        {
            return DALTickets.Instance.Actualizar(ticket);
        }
        public static bool Insertar(Tickets ticket)
        {
            return DALTickets.Instance.Insertar(ticket);
        }
        #endregion
    }
}
