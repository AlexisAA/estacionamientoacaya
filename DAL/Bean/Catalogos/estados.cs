﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Common;

namespace DAL.Bean.Catalogos
{
    public class estados : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new estados());
        private Dictionary<string, object> _values;

        private int _clave;
        private string _nombre;
        
        #endregion

        #region Contructor
        public estados()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int clave
        {
            get { return _clave; }
            set { _clave = value; Values["clave"] = value; }
        }
        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; Values["nombre"] = value; }
        }

        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("clave"))
                clave = Convert.ToInt32(values["clave"]);
            if (values.ContainsKey("nombre"))
                nombre = Convert.ToString(values["nombre"]);
        }
        #endregion
    }
}
