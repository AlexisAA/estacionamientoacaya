﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Catalogos
{
    /// <summary>
    /// Clase Logs
    /// </summary>
    public class Logs : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Logs());
        private Dictionary<string, object> _values;

        private int _iD_Log;
        private string _formulario;
        private string _evento;
        private string _metodo;
        private DateTime _fecha;
        private string _respuesta;
        private string _descripcion;
        private bool _esSistema;
        private int _iD_Usuario;
        #endregion

        #region Contructor
        public Logs()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Log
        {
            get { return _iD_Log; }
            set { _iD_Log = value; Values["ID_Log"] = value; }
        }
        public string Formulario
        {
            get { return _formulario; }
            set { _formulario = value; Values["Formulario"] = value; }
        }
        public string Evento
        {
            get { return _evento; }
            set { _evento = value; Values["Evento"] = value; }
        }
        public string Metodo
        {
            get { return _metodo; }
            set { _metodo = value; Values["Metodo"] = value; }
        }
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; Values["Fecha"] = value; }
        }
        public string Respuesta
        {
            get { return _respuesta; }
            set { _respuesta = value; Values["Respuesta"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public bool EsSistema
        {
            get{ return _esSistema; }
            set{ _esSistema = value; Values["EsSistema"] = value; }
        }
        public int ID_Usuario
        {
            get { return _iD_Usuario; }
            set { _iD_Usuario = value; Values["ID_Usuario"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Log"))
                ID_Log = Convert.ToInt32(values["ID_Log"]);
            if (values.ContainsKey("Formulario"))
                Formulario = Convert.ToString(values["Formulario"]);
            if (values.ContainsKey("Evento"))
                Evento = Convert.ToString(values["Evento"]);
            if (values.ContainsKey("Metodo"))
                Metodo = Convert.ToString(values["Metodo"]);
            if (values.ContainsKey("Fecha"))
                Fecha = Convert.ToDateTime(values["Fecha"]);
            if (values.ContainsKey("Respuesta"))
                Respuesta = Convert.ToString(values["Respuesta"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("EsSistema"))
                EsSistema = Convert.ToBoolean(values["EsSistema"]);
            if (values.ContainsKey("ID_Usuario"))
                ID_Usuario = Convert.ToInt32(values["ID_Usuario"]);
        }
        #endregion
    }
}
