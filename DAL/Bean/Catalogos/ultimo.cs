﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DAL.Common;

namespace DAL.Bean.Catalogos
{
    public class ultimo : IBean
    {
        #region Atributos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new estados());
        private Dictionary<string, object> _values;


        private string _ultimo;

        #endregion

        #region Contructor
        public ultimo()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public string ultimos
        {
            get { return _ultimo; }
            set { _ultimo = value; Values["ultimo"] = value; }
        }

        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ultimo"))
                ultimos = Convert.ToString(values["ultimo"]);
        }
        #endregion
    }
}
