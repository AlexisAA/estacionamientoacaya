﻿using System.Collections.Generic;

using DAL.DataAccess.Catalogos;

namespace DAL.Bean.Catalogos
{
    public class BuUsuarios
    {
        #region Metodos Standar
        public static List<Usuarios> Buscar(Usuarios usuario)
        {
            return DALUsuarios.Instance.Buscar(usuario);
        }
        public static bool Eliminar(Usuarios usuario)
        {
            return DALUsuarios.Instance.Eliminar(usuario);
        }
        public static bool Actualizar(Usuarios usuario)
        {
            return DALUsuarios.Instance.Actualizar(usuario);
        }
        public static bool Insertar(Usuarios usuario)
        {
            return DALUsuarios.Instance.Insertar(usuario);
        }
        #endregion
    }
}
