﻿using DAL.DataAccess.Portal;
using System.Collections.Generic;

namespace DAL.Bean.Portal
{
    public class BUIngresos
    {
        #region Metodos Standar
        public static List<Ingresos> Buscar(Ingresos ing_pen)
        {
            return DALIngresos.Instance.Buscar(ing_pen);
        }
        public static bool Eliminar(Ingresos ing_pen)
        {
            return DALIngresos.Instance.Eliminar(ing_pen);
        }
        public static bool Actualizar(Ingresos ing_pen)
        {
            return DALIngresos.Instance.Actualizar(ing_pen);
        }
        public static bool Insertar(Ingresos ing_pen)
        {
            return DALIngresos.Instance.Insertar(ing_pen);
        }
        #endregion
    }
}
