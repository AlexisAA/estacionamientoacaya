﻿using System.Collections.Generic;

using DAL.DataAccess.Portal;

namespace DAL.Bean.Portal
{
    public class BuFacturas
    {
        #region Metodos Standar
        public static List<Facturas> Buscar(Facturas fac)
        {
            return DALFacturas.Instance.Buscar(fac);
        }
        public static bool Eliminar(Facturas fac)
        {
            return DALFacturas.Instance.Eliminar(fac);
        }
        public static bool Actualizar(Facturas fac)
        {
            return DALFacturas.Instance.Actualizar(fac);
        }
        public static bool Insertar(Facturas fac)
        {
            return DALFacturas.Instance.Insertar(fac);
        }
        #endregion
    }
}
