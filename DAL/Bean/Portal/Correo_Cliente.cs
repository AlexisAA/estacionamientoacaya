﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    /// <summary>
    /// Clase 
    /// </summary>
    public class Correo_Cliente : IBean
    {

        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Correo_Cliente());
        private Dictionary<string, object> _values;

        private int _iD_Corre;
        private int _iD_Cliente;
        private string _correo;
        private bool _valido;
        #endregion

        #region Constructor
        public Correo_Cliente()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Correo
        {
            get { return _iD_Corre; }
            set { _iD_Corre = value; Values["ID_Correo"] = value; }
        }
        public int ID_Cliente
        {
            get { return _iD_Cliente; }
            set { _iD_Cliente = value; Values["ID_Cliente"] = value; }
        }
        public string Correo
        {
            get { return _correo; }
            set { _correo = value; Values["Correo"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Correo"))
                ID_Correo = Convert.ToInt32(values["ID_Correo"]);
            if (values.ContainsKey("ID_Cliente"))
                ID_Cliente = Convert.ToInt32(values["ID_Cliente"]);
            if (values.ContainsKey("Correo"))
                Correo = Convert.ToString(values["Correo"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion

    }
}
