﻿using DAL.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Bean.Portal
{
    public class Facturas_Canceladas:IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Facturas_Canceladas());
        private Dictionary<string, object> _values;

        private int _iD_Factura_Cancelada;
        private int _iD_Factura;
        private int _iD_Cliente;
        private string _uUID;
        private DateTime _fecha_Factura;
        private string _nombre_XML;
        private byte[] _archivo_XML;
        private string _descripcion;
        private bool _valido;
        #endregion

        #region Constructor
        public Facturas_Canceladas()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Factura_Cancelada
        {
            get { return _iD_Factura_Cancelada; }
            set { _iD_Factura_Cancelada = value; Values["ID_Factura_Cancelada"] = value; }
        }

        public int ID_Factura
        {
            get { return _iD_Factura; }
            set { _iD_Factura = value; Values["ID_Factura"] = value; }
        }
        public int ID_Cliente
        {
            get { return _iD_Cliente; }
            set { _iD_Cliente = value; Values["ID_Cliente"] = value; }
        }
        public string UUID
        {
            get { return _uUID; }
            set { _uUID = value; Values["UUID"] = value; }
        }
        public DateTime Fecha_Factura
        {
            get { return _fecha_Factura; }
            set { _fecha_Factura = value; Values["Fecha_Factura"] = value; }
        }
        public string Nombre_XML
        {
            get { return _nombre_XML; }
            set { _nombre_XML = value; Values["Nombre_XML"] = value; }
        }
        public byte[] Archivo_XML
        {
            get { return _archivo_XML; }
            set { _archivo_XML = value; Values["Archivo_XML"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Factura_Cancelada"))
                ID_Factura_Cancelada = Convert.ToInt32(values["ID_Factura_Cancelada"]);

            if (values.ContainsKey("ID_Factura"))
                ID_Factura = Convert.ToInt32(values["ID_Factura"]);

            if (values.ContainsKey("ID_Cliente"))
                ID_Cliente = Convert.ToInt32(values["ID_Cliente"]);

            if (values.ContainsKey("UUID"))
                UUID = Convert.ToString(values["UUID"]);

            if (values.ContainsKey("Fecha_Factura"))
                Fecha_Factura = Convert.ToDateTime(values["Fecha_Factura"]);            
            
            if (values.ContainsKey("Nombre_XML"))
                Nombre_XML = Convert.ToString(values["Nombre_XML"]);

            if (values.ContainsKey("Archivo_XML"))
                Archivo_XML = (byte[])values["Archivo_XML"];           

            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion
    }
}
