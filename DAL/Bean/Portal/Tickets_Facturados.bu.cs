﻿using System.Collections.Generic;

using DAL.DataAccess.Portal;

namespace DAL.Bean.Portal
{
    public class BuTickets_Facturados
    {
        #region Metodos Standar
        public static List<Tickets_Facturados> Buscar(Tickets_Facturados tf)
        {
            return DALTickets_Facturados.Instance.Buscar(tf);
        }
        public static bool Eliminar(Tickets_Facturados tf)
        {
            return DALTickets_Facturados.Instance.Eliminar(tf);
        }
        public static bool Actualizar(Tickets_Facturados tf)
        {
            return DALTickets_Facturados.Instance.Actualizar(tf);
        }
        public static bool Insertar(Tickets_Facturados tf)
        {
            return DALTickets_Facturados.Instance.Insertar(tf);
        }
        #endregion
    }
}
