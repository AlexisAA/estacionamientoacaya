﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    /// <summary>
    /// Clase 
    /// </summary>
    public class Clientes : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Clientes());
        private Dictionary<string, object> _values;

        private int _iD_Cliente;
        private string _rFC;
        private string _razon_social;
        private string _calle;
        private string _no_Exterior;
        private string _no_Interior;
        private string _codigo_Postal;
        private string _col_Pob_Cd;
        private string _del_Muni;
        private string _ciudad;
        private string _estado;
        #endregion

        #region Constructor
        public Clientes()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Cliente
        {
            get { return _iD_Cliente; }
            set { _iD_Cliente = value; Values["ID_Cliente"] = value; }
        }
        public string RFC
        {
            get { return _rFC; }
            set { _rFC = value; Values["RFC"] = value; }
        }
        public string Razon_social
        {
            get { return _razon_social; }
            set { _razon_social = value; Values["Razon_social"] = value; }
        }
        public string Calle
        {
            get { return _calle; }
            set { _calle = value; Values["Calle"] = value; }
        }
        public string No_Exterior
        {
            get { return _no_Exterior; }
            set { _no_Exterior = value; Values["No_Exterior"] = value; }
        }
        public string No_Interior
        {
            get { return _no_Interior; }
            set { _no_Interior = value; Values["No_Interior"] = value; }
        }
        public string Codigo_Postal
        {
            get { return _codigo_Postal; }
            set { _codigo_Postal = value; Values["Codigo_Postal"] = value; }
        }
        public string Col_Pob_Cd
        {
            get { return _col_Pob_Cd; }
            set { _col_Pob_Cd = value; Values["Col_Pob_Cd"] = value; }
        }
        public string Del_Muni
        {
            get { return _del_Muni; }
            set { _del_Muni = value; Values["Del_Muni"] = value; }
        }
        public string Ciudad
        {
            get { return _ciudad; }
            set { _ciudad = value; Values["Ciudad"] = value; }
        }
        public string Estado
        {
            get { return _estado; }
            set { _estado = value; Values["Estado"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Cliente"))
                ID_Cliente = Convert.ToInt32(values["ID_Cliente"]);
            if (values.ContainsKey("RFC"))
                RFC = Convert.ToString(values["RFC"]);
            if (values.ContainsKey("Razon_social"))
                Razon_social = Convert.ToString(values["Razon_social"]);
            if (values.ContainsKey("Calle"))
                Calle = Convert.ToString(values["Calle"]);
            if (values.ContainsKey("No_Exterior"))
                No_Exterior = Convert.ToString(values["No_Exterior"]);
            if (values.ContainsKey("No_Interior"))
                No_Interior = Convert.ToString(values["No_Interior"]);
            if (values.ContainsKey("Codigo_Postal"))
                Codigo_Postal = Convert.ToString(values["Codigo_Postal"]);
            if (values.ContainsKey("Col_Pob_Cd"))
                Col_Pob_Cd = Convert.ToString(values["Col_Pob_Cd"]);
            if (values.ContainsKey("Del_Muni"))
                Del_Muni = Convert.ToString(values["Del_Muni"]);
            if (values.ContainsKey("Ciudad"))
                Ciudad = Convert.ToString(values["Ciudad"]);
            if (values.ContainsKey("Estado"))
                Estado = Convert.ToString(values["Estado"]);
        }
        #endregion
    }
}
