﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    public class Pensiones_Facturadas : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Pensiones_Facturadas());
        private Dictionary<string, object> _values;

        private int _iD_PenF;
        private int _iD_Pensiones;
        private int _iD_Factura;
        private bool _valido;
        #endregion

        #region Constructor
        public Pensiones_Facturadas()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_PenF
        {
            get { return _iD_PenF; }
            set { _iD_PenF = value; Values["ID_PenF"] = value; }
        }
        public int ID_Pensiones
        {
            get { return _iD_Pensiones; }
            set { _iD_Pensiones = value; Values["ID_Pensiones"] = value; }
        }
        public int ID_Factura
        {
            get { return _iD_Factura; }
            set { _iD_Factura = value; Values["ID_Factura"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_PenF"))
                ID_PenF = Convert.ToInt32(values["ID_PenF"]);
            if (values.ContainsKey("ID_Pensiones"))
                ID_Pensiones = Convert.ToInt32(values["ID_Pensiones"]);
            if (values.ContainsKey("ID_Factura"))
                ID_Factura = Convert.ToInt32(values["ID_Factura"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion

    }
}
