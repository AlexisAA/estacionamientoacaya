﻿using System.Collections.Generic;

using DAL.DataAccess.Portal;

namespace DAL.Bean.Portal
{
    public class BuClientes
    {
        #region Metodos Standar
        public static List<Clientes> Buscar(Clientes cliente)
        {
            return DALClientes.Instance.Buscar(cliente);
        }
        public static bool Eliminar(Clientes cliente)
        {
            return DALClientes.Instance.Eliminar(cliente);
        }
        public static bool Actualizar(Clientes cliente)
        {
            return DALClientes.Instance.Actualizar(cliente);
        }
        public static bool Insertar(Clientes cliente)
        {
            return DALClientes.Instance.Insertar(cliente);
        }
        #endregion
    }
}
