﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    /// <summary>
    /// Clase 
    /// </summary>
    public class Facturas : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Facturas());
        private Dictionary<string, object> _values;

        private int _iD_Factura;
        private int _iD_Cliente;
        private string _uUID;
        private DateTime _fecha_Factura;
        private decimal _importe;
        private string _folio;
        private string _serie;
        private string _descripcion;
        private string _nombre_XML;
        private string _nombre_PDF;
        private byte[] _archivo_XML;
        private byte[] _archivo_PDF;
        private string _iD_Origen_Factura;
        private bool _valido;
        #endregion

        #region Constructor
        public Facturas()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Factura
        {
            get { return _iD_Factura; }
            set { _iD_Factura = value; Values["ID_Factura"] = value; }
        }
        public int ID_Cliente
        {
            get { return _iD_Cliente; }
            set { _iD_Cliente = value; Values["ID_Cliente"] = value; }
        }
        public string UUID
        {
            get { return _uUID; }
            set { _uUID = value; Values["UUID"] = value; }
        }
        public DateTime Fecha_Factura
        {
            get { return _fecha_Factura; }
            set { _fecha_Factura = value; Values["Fecha_Factura"] = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; Values["Importe"] = value; }
        }
        public string Folio
        {
            get { return _folio; }
            set { _folio = value; Values["Folio"] = value; }
        }
        public string Serie
        {
            get { return _serie; }
            set { _serie = value; Values["Serie"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public string Nombre_XML
        {
            get { return _nombre_XML; }
            set { _nombre_XML = value; Values["Nombre_XML"] = value; }
        }
        public string Nombre_PDF
        {
            get { return _nombre_PDF; }
            set { _nombre_PDF = value; Values["Nombre_PDF"] = value; }
        }
        public byte[] Archivo_XML
        {
            get { return _archivo_XML; }
            set { _archivo_XML = value; Values["Archivo_XML"] = value; }
        }
        public byte[] Archivo_PDF
        {
            get { return _archivo_PDF; }
            set { _archivo_PDF = value; Values["Archivo_PDF"] = value; }
        }
        public string ID_Origen_Factura
        {
            get { return _iD_Origen_Factura; }
            set { _iD_Origen_Factura = value; Values["ID_Origen_Factura"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Factura"))
                ID_Factura = Convert.ToInt32(values["ID_Factura"]);
            if (values.ContainsKey("ID_Cliente"))
                ID_Cliente = Convert.ToInt32(values["ID_Cliente"]);
            if (values.ContainsKey("UUID"))
                UUID = Convert.ToString(values["UUID"]);
            if (values.ContainsKey("Fecha_Factura"))
                Fecha_Factura = Convert.ToDateTime(values["Fecha_Factura"]);
            if (values.ContainsKey("Importe"))
                Importe = Convert.ToDecimal(values["Importe"]);
            if (values.ContainsKey("Folio"))
                Folio = Convert.ToString(values["Folio"]);
            if (values.ContainsKey("Serie"))
                Serie = Convert.ToString(values["Serie"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Nombre_XML"))
                Nombre_XML = Convert.ToString(values["Nombre_XML"]);
            if (values.ContainsKey("Nombre_PDF"))
                Nombre_PDF = Convert.ToString(values["Nombre_PDF"]);
            if (values.ContainsKey("Archivo_XML"))
                Archivo_XML = (byte[])values["Archivo_XML"];           //Convert.ToUInt64(values["Archivo_XML"]);
            if (values.ContainsKey("Archivo_PDF"))
                Archivo_PDF = (byte[])values["Archivo_PDF"];           //Convert.ToUInt64(values["Archivo_XML"]);
            if (values.ContainsKey("ID_Origen_Factura"))
                ID_Origen_Factura = Convert.ToString(values["ID_Origen_Factura"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion

    }
}
