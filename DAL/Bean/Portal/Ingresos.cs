﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    public class Ingresos : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Ingresos());
        private Dictionary<string, object> _values;

        private int _iD_IngresoPensiones;
        private int _iD_Cliente;
        private decimal _subtotal;
        private decimal _total;
        private string _forma_Pago;
        private string _descripcion;
        private DateTime _fecha_Creacion;
        #endregion

        #region Constructor
        public Ingresos()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }

        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_IngresoPensiones
        {
            get { return _iD_IngresoPensiones; }
            set { _iD_IngresoPensiones = value; Values["ID_IngresoPensiones"] = value; }
        }

        public int ID_Cliente
        {
            get { return _iD_Cliente; }
            set { _iD_Cliente = value; Values["ID_Cliente"] = value; }
        }

        public decimal Subtotal
        {
            get { return _subtotal; }
            set { _subtotal = value; Values["Subtotal"] = value; }
        }
        public decimal Total
        {
            get { return _total; }
            set { _total = value; Values["Total"] = value; }
        }

        public string Forma_Pago
        {
            get { return _forma_Pago; }
            set { _forma_Pago = value; Values["Forma_Pago"] = value; }
        }

        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }

        public DateTime Fecha_Creacion
        {
            get { return _fecha_Creacion; }
            set { _fecha_Creacion = value; Values["Fecha_Creacion"] = value; }
        }

        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_IngresoPensiones"))
                ID_IngresoPensiones = Convert.ToInt32(values["ID_IngresoPensiones"]);
            if (values.ContainsKey("ID_Cliente"))
                ID_Cliente = Convert.ToInt32(values["ID_Cliente"]);
            if (values.ContainsKey("_subtotal"))
                Subtotal = Convert.ToDecimal(values["Subtotal"]);
            if (values.ContainsKey("Total"))
                Total = Convert.ToDecimal(values["Total"]);
            if (values.ContainsKey("Forma_Pago"))
                Forma_Pago = Convert.ToString(values["Forma_Pago"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Fecha_Creacion"))
                Fecha_Creacion = Convert.ToDateTime(values["Fecha_Creacion"]);
        }
        #endregion
    }
}
