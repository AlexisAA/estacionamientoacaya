﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    /// <summary>
    /// Clase 
    /// </summary>
    public class Tickets_Facturados : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Tickets_Facturados());
        private Dictionary<string, object> _values;

        private int _iD_TenF;
        private string _no_Ticket;
        private int _iD_Factura;
        private bool _valido;
        #endregion

        #region Constructor
        public Tickets_Facturados()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_TenF
        {
            get { return _iD_TenF; }
            set { _iD_TenF = value; Values["ID_TenF"] = value; }
        }
        public string No_Ticket
        {
            get { return _no_Ticket; }
            set { _no_Ticket = value; Values["No_Ticket"] = value; }
        }
        public int ID_Factura
        {
            get { return _iD_Factura; }
            set { _iD_Factura = value; Values["ID_Factura"] = value; }
        }
        public bool Valido
        {
            get { return _valido; }
            set { _valido = value; Values["Valido"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_TenF"))
                ID_TenF = Convert.ToInt32(values["ID_TenF"]);
            if (values.ContainsKey("No_Ticket"))
                No_Ticket = Convert.ToString(values["No_Ticket"]);
            if (values.ContainsKey("ID_Factura"))
                ID_Factura = Convert.ToInt32(values["ID_Factura"]);
            if (values.ContainsKey("Valido"))
                Valido = Convert.ToBoolean(values["Valido"]);
        }
        #endregion

    }
}
