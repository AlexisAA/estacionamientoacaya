﻿using DAL.DataAccess.Portal;
using System.Collections.Generic;

namespace DAL.Bean.Portal
{
    public class BUPensiones
    {
        #region Metodos Standar
        public static List<Pensiones> Buscar(Pensiones ing_pen)
        {
            return DALPensiones.Instance.Buscar(ing_pen);
        }
        public static bool Eliminar(Pensiones ing_pen)
        {
            return DALPensiones.Instance.Eliminar(ing_pen);
        }
        public static bool Actualizar(Pensiones ing_pen)
        {
            return DALPensiones.Instance.Actualizar(ing_pen);
        }
        public static bool Insertar(Pensiones ing_pen)
        {
            return DALPensiones.Instance.Insertar(ing_pen);
        }
        #endregion
    }
}
