﻿using System;
using System.Collections.Generic;

using DAL.Common;

namespace DAL.Bean.Portal
{
    public class Pensiones : IBean
    {
        #region Atrinutos
        private static List<Propiedad> _propiedades = BeanHelper.obtenPropiedades(new Pensiones());
        private Dictionary<string, object> _values;

        private int _iD_Pensiones;
        private decimal _cantidad;
        private string _unidad;
        private string _descripcion;
        private decimal _precio_Unitario;
        private decimal _importe;
        private decimal _subtotal;
        private decimal _iVA;
        private decimal _total;
        private string _metodo_Pago;
        private string _correos;
        private DateTime _fecha_Creacion;
        private int _iD_Factura;
        #endregion

        #region Constructor
        public Pensiones()
        {
            _values = BeanHelper.getListValues(_propiedades);
        }
        #endregion

        #region Propiedades
        public IList<Propiedad> Propiedades
        {
            get { return _propiedades.AsReadOnly(); }
        }
        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public int ID_Pensiones
        {
            get { return _iD_Pensiones; }
            set { _iD_Pensiones = value; Values["ID_Pensiones"] = value; }
        }
        public decimal Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; Values["Cantidad"] = value; }
        }
        public string Unidad
        {
            get { return _unidad; }
            set { _unidad = value; Values["Unidad"] = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; Values["Descripcion"] = value; }
        }
        public decimal Precio_Unitario
        {
            get { return _precio_Unitario; }
            set { _precio_Unitario = value; Values["Precio_Unitario"] = value; }
        }
        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; Values["Importe"] = value; }
        }
        public decimal Subtotal
        {
            get { return _subtotal; }
            set { _subtotal = value; Values["Subtotal"] = value; }
        }
        public decimal IVA
        {
            get { return _iVA; }
            set { _iVA = value; Values["IVA"] = value; }
        }
        public decimal Total
        {
            get { return _total; }
            set { _total = value; Values["Total"] = value; }
        }
        public string Metodo_Pago
        {
            get { return _metodo_Pago; }
            set { _metodo_Pago = value; Values["Metodo_Pago"] = value; }
        }
        public string Correos
        {
            get { return _correos; }
            set { _correos = value; Values["Correos"] = value; }
        }
        public DateTime Fecha_Creacion
        {
            get { return _fecha_Creacion; }
            set { _fecha_Creacion = value; Values["Fecha_Creacion"] = value; }
        }
        public int ID_Factura
        {
            get { return _iD_Factura; }
            set { _iD_Factura = value; Values["ID_Factura"] = value; }
        }
        #endregion

        #region Metodo
        public void BindFromValues(Dictionary<string, object> values)
        {
            if (values.ContainsKey("ID_Pensiones"))
                ID_Pensiones = Convert.ToInt32(values["ID_Pensiones"]);
            if (values.ContainsKey("Cantidad"))
                Cantidad = Convert.ToDecimal(values["Cantidad"]);
            if (values.ContainsKey("Unidad"))
                Unidad = Convert.ToString(values["Unidad"]);
            if (values.ContainsKey("Descripcion"))
                Descripcion = Convert.ToString(values["Descripcion"]);
            if (values.ContainsKey("Precio_Unitario"))
                Precio_Unitario = Convert.ToDecimal(values["Precio_Unitario"]);
            if (values.ContainsKey("Importe"))
                Importe = Convert.ToDecimal(values["Importe"]);
            if (values.ContainsKey("Subtotal"))
                Subtotal = Convert.ToDecimal(values["Subtotal"]);
            if (values.ContainsKey("IVA"))
                IVA = Convert.ToDecimal(values["IVA"]);
            if (values.ContainsKey("Total"))
                Total = Convert.ToDecimal(values["Total"]);
            if (values.ContainsKey("Metodo_Pago"))
                Metodo_Pago = Convert.ToString(values["Metodo_Pago"]);
            if (values.ContainsKey("Correos"))
                Correos = Convert.ToString(values["Correos"]);
            if (values.ContainsKey("Fecha_Creacion"))
                Fecha_Creacion = Convert.ToDateTime(values["Fecha_Creacion"]);
            if (values.ContainsKey("ID_Factura"))
                ID_Factura = Convert.ToInt32(values["ID_Factura"]);
        }
        #endregion

    }
}
