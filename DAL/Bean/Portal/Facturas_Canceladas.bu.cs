﻿using DAL.DataAccess.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Bean.Portal
{
    public class BUFacturas_Canceladas
    {
        #region Metodos Standar
        public static List<Facturas_Canceladas> Buscar(Facturas_Canceladas fac)
        {
            return DALFacturas_Canceladas.Instance.Buscar(fac);
        }
        public static bool Eliminar(Facturas_Canceladas fac)
        {
            return DALFacturas_Canceladas.Instance.Eliminar(fac);
        }
        public static bool Actualizar(Facturas_Canceladas fac)
        {
            return DALFacturas_Canceladas.Instance.Actualizar(fac);
        }
        public static bool Insertar(Facturas_Canceladas fac)
        {
            return DALFacturas_Canceladas.Instance.Insertar(fac);
        }
        #endregion
    }
}
