﻿using System.Collections.Generic;

using DAL.DataAccess.Portal;

namespace DAL.Bean.Portal
{
    public class BUCorreo_Cliente
    {
        #region Metodos Standar
        public static List<Correo_Cliente> Buscar(Correo_Cliente cc)
        {
            return DALCorreo_Cliente.Instance.Buscar(cc);
        }
        public static bool Eliminar(Correo_Cliente cc)
        {
            return DALCorreo_Cliente.Instance.Eliminar(cc);
        }
        public static bool Actualizar(Correo_Cliente cc)
        {
            return DALCorreo_Cliente.Instance.Actualizar(cc);
        }
        public static bool Insertar(Correo_Cliente cc)
        {
            return DALCorreo_Cliente.Instance.Insertar(cc);
        }
        #endregion
    }
}
