﻿using System.Collections.Generic;

using DAL.DataAccess.Portal;

namespace DAL.Bean.Portal
{
    public class BUPensiones_Facturadas
    {
        #region Metodos Standar
        public static List<Pensiones_Facturadas> Buscar(Pensiones_Facturadas pf)
        {
            return DALPensiones_Facturadas.Instance.Buscar(pf);
        }
        public static bool Eliminar(Pensiones_Facturadas pf)
        {
            return DALPensiones_Facturadas.Instance.Eliminar(pf);
        }
        public static bool Actualizar(Pensiones_Facturadas pf)
        {
            return DALPensiones_Facturadas.Instance.Actualizar(pf);
        }
        public static bool Insertar(Pensiones_Facturadas pf)
        {
            return DALPensiones_Facturadas.Instance.Insertar(pf);
        }
        #endregion
    }
}
