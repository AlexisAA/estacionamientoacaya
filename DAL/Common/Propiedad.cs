﻿using System;

namespace DAL.Common
{
    /// <summary>
    /// Clase Propiedades de los elementos
    /// </summary>
    public class Propiedad
    {
        public string Nombre;
        public Type Tipo;
    }

}
