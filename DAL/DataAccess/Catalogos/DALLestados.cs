﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Catalogos;
using System;

namespace DAL.DataAccess.Catalogos
{
    public class DALLestados1
    {

        public static DataTable DALestad()
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_estados]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);


            return dtArchivos;
        }

        public static DataTable DALmunic(int estadoid)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_municipios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@estado_id", DbType.Int32, estadoid);
            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);


            return dtArchivos;
        }

        public static DataTable DALlocal(int municipioid)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_localidad]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@municipio_id", DbType.Int32, municipioid);
            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);


            return dtArchivos;
        }

        internal sealed class DALLestados : DataAccess<estados>
        {
            private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
            private static readonly DALLestados1 _instance = new DALLestados1();

            internal static DALLestados1 Instance
            {
                get { return _instance; }
            }

            public override List<estados> Buscar(IBean estados)
            {
                DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_estados]");
                GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
                PopulateParameters(estados, comm);
                DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

                return PopulateObjectsFromDataTable(dtResultado);
            }

            public override bool Eliminar(IBean objeto)
            {
                throw new NotImplementedException();
            }

            public override bool Actualizar(IBean objeto)
            {
                throw new NotImplementedException();
            }

            public override bool Insertar(IBean objeto)
            {
                throw new NotImplementedException();
            }

        }
    }
}
