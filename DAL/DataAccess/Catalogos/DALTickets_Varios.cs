﻿using System;

using System.Data;
using System.Data.Common;

namespace DAL.DataAccess.Catalogos
{
    public class DALTickets_Varios
    {

        public static DataTable DALBuscaEstacionamiento()
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets_Varios1]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, DateTime.Now);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Fin_in", DbType.DateTime, DateTime.Now);
            GenericDataAccess.AddInParameter(comm, "@Nombre_Sistema_in", DbType.String, "0");
            GenericDataAccess.AddInParameter(comm, "@Forma_Pago_in", DbType.String, "0");

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

        public static DataTable DALBuscaFormaPago()
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 2);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, DateTime.Now);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Fin_in", DbType.DateTime, DateTime.Now);
            GenericDataAccess.AddInParameter(comm, "@Nombre_Sistema_in", DbType.String, "0");
            GenericDataAccess.AddInParameter(comm, "@Forma_Pago_in", DbType.String, "0");

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

        public static DataTable DALBuscaTickets(DateTime finicio, DateTime ffin, string esta, string fpago)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 3);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, finicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Fin_in", DbType.DateTime, ffin);
            GenericDataAccess.AddInParameter(comm, "@Nombre_Sistema_in", DbType.String, esta);
            GenericDataAccess.AddInParameter(comm, "@Forma_Pago_in", DbType.String, fpago);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

    }
}
