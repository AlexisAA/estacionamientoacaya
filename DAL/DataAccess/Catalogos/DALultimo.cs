﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Catalogos;
using System;


namespace DAL.DataAccess.Catalogos
{
    public class DALultimo1
    {
        public static DataTable DALultimos()
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_ultimoticket]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);
            
            return dtArchivos;
        }
    }
}
