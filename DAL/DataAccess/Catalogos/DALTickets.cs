﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Catalogos;

namespace DAL.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Usuarios
    /// </summary>
    internal sealed class DALTickets : DataAccess<Tickets>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALTickets _instance = new DALTickets();

        internal static DALTickets Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Tickets> Buscar(IBean usuario)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(usuario, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean usuario)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(usuario, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean usuario)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(usuario, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean usuario)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Tickets]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(usuario, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
