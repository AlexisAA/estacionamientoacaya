﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Catalogos;

namespace DAL.DataAccess.Catalogos
{
    /// <summary>
    /// Clase para los movimientos de la clase Logs
    /// </summary>
    internal sealed class DALLogs : DataAccess<Logs>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALLogs _instance = new DALLogs();

        internal static DALLogs Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Logs
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Logs> Buscar(IBean logs)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Logs]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(logs, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Logs
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean logs)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Logs]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(logs, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Logs
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean logs)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Logs]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(logs, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Logs
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean logs)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Logs]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(logs, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
