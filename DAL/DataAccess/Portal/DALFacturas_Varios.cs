﻿using System;

using System.Data;
using System.Data.Common;

namespace DAL.DataAccess.Portal
{
    public class DALFacturas_Varios
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="finicio"></param>
        /// <param name="ffin"></param>
        /// <returns></returns>
        public static DataTable DALBuscaFacturas(int id_cliente, DateTime finicio, DateTime ffin)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);
            GenericDataAccess.AddInParameter(comm, "@ID_Cliente_in", DbType.Int32, id_cliente);            
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, finicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, ffin);
            GenericDataAccess.AddInParameter(comm, "@ID_Origen_Factura_in", DbType.String, "");

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

        public static DataTable DALBuscaFacturas(int id_cliente, DateTime finicio, DateTime ffin, string OrigenFactura)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Varios]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 2);
            GenericDataAccess.AddInParameter(comm, "@ID_Cliente_in", DbType.Int32, id_cliente);
            GenericDataAccess.AddInParameter(comm, "@Fecha_Ini_in", DbType.DateTime, finicio);
            GenericDataAccess.AddInParameter(comm, "@Fecha_fin_in", DbType.DateTime, ffin);
            GenericDataAccess.AddInParameter(comm, "@ID_Origen_Factura_in", DbType.String, OrigenFactura);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }

    }
}
