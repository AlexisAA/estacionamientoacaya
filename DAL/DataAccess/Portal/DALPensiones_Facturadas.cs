﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Portal;

namespace DAL.DataAccess.Portal
{
    internal sealed class DALPensiones_Facturadas : DataAccess<Pensiones_Facturadas>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALPensiones_Facturadas _instance = new DALPensiones_Facturadas();

        internal static DALPensiones_Facturadas Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Pensiones_Facturadas> Buscar(IBean pf)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones_Facturadas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(pf, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean pf)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones_Facturadas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(pf, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean pf)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones_Facturadas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(pf, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean pf)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones_Facturadas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(pf, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
