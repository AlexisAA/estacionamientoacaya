﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Portal;

namespace DAL.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Usuarios
    /// </summary>
    internal sealed class DALFacturas : DataAccess<Facturas>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALFacturas _instance = new DALFacturas();

        internal static DALFacturas Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Facturas> Buscar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(fac, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
