﻿using DAL.Bean;
using DAL.Bean.Portal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Facturas Canceladas
    /// </summary>
    internal sealed class DALFacturas_Canceladas : DataAccess<Facturas_Canceladas>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALFacturas_Canceladas _instance = new DALFacturas_Canceladas();

        internal static DALFacturas_Canceladas Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Facturas_Canceladas
        /// </summary>
        /// <param name="Facturas_Canceladas">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Facturas_Canceladas> Buscar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Canceladas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(fac, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Facturas_Canceladas
        /// </summary>
        /// <param name="Facturas_Canceladas">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Canceladas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Facturas_Canceladas
        /// </summary>
        /// <param name="Facturas_Canceladas">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Canceladas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Facturas_Canceladas
        /// </summary>
        /// <param name="Facturas_Canceladas">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean fac)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Facturas_Canceladas]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(fac, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
