﻿using System.Collections.Generic;

using DAL.Bean.Portal;
using DAL.Bean;
using System.Data.Common;
using System.Data;

namespace DAL.DataAccess.Portal
{
    internal sealed class DALPensiones : DataAccess<Pensiones>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = -1000, Update = 4, Inner = 5 };
        private static readonly DALPensiones _instance = new DALPensiones();

        internal static DALPensiones Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Ingresos_Pensiones
        /// </summary>
        /// <param name="Ingresos_Pensiones">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Pensiones> Buscar(IBean pensiones)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(pensiones, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina ingreso_pensiones
        /// </summary>
        /// <param name="ingreso_pensiones">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean pensiones)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(pensiones, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza ingreso_pensiones
        /// </summary>
        /// <param name="ingreso_pensiones">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean pensiones)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(pensiones, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta ingreso_pensiones
        /// </summary>
        /// <param name="ingreso_pensiones">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean pensiones)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Pensiones]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(pensiones, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
