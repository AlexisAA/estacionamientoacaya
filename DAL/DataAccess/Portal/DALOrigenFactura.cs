﻿using System.Data;
using System.Data.Common;

namespace DAL.DataAccess.Portal
{
    public class DALOrigenFactura
    {
        public static DataTable DALBuscaOFactura()
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_OrigenFactura]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, 1);

            DataTable dtArchivos = GenericDataAccess.ExecuteSelectCommand(comm);

            return dtArchivos;
        }
    }
}
