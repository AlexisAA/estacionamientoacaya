﻿using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using DAL.Bean;
using DAL.Bean.Portal;

namespace DAL.DataAccess.Portal
{
    /// <summary>
    /// Clase para los movimientos de la clase Usuarios
    /// </summary>
    internal sealed class DALClientes : DataAccess<Clientes>
    {
        private enum _operaciones { Select = 1, Insert = 2, Delete = 3, Update = 4 };
        private static readonly DALClientes _instance = new DALClientes();

        internal static DALClientes Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Busca Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Lista</returns>
        public override List<Clientes> Buscar(IBean clientes)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Clientes]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Select);
            PopulateParameters(clientes, comm);
            DataTable dtResultado = GenericDataAccess.ExecuteSelectCommand(comm);

            return PopulateObjectsFromDataTable(dtResultado);
        }

        /// <summary>
        /// Elimina Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Eliminar(IBean clientes)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Clientes]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Delete);
            PopulateParameters(clientes, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Actualiza Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Actualizar(IBean clientes)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Clientes]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Update);
            PopulateParameters(clientes, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }

        /// <summary>
        /// Inserta Usuario
        /// </summary>
        /// <param name="usuario">Objeto</param>
        /// <returns>Registros afectados</returns>
        public override bool Insertar(IBean clientes)
        {
            DbCommand comm = GenericDataAccess.CreateCommandSP("[sp_Clientes]");
            GenericDataAccess.AddInParameter(comm, "@operacion", DbType.Int32, _operaciones.Insert);
            PopulateParameters(clientes, comm);

            return GenericDataAccess.ExecuteNonQuery(comm) > 0;
        }
    }
}
