﻿namespace EnviaTickets
{
    partial class frmEnviaTickets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnviaTickets));
            this.dgvLogs = new System.Windows.Forms.DataGridView();
            this.btnConfigurar = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.tmrEjecuta = new System.Windows.Forms.Timer(this.components);
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.lblTituloAccion = new System.Windows.Forms.Label();
            this.lblAccion = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.chbEjecutado = new System.Windows.Forms.CheckBox();
            this.lst_error = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogs)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLogs
            // 
            this.dgvLogs.AllowUserToAddRows = false;
            this.dgvLogs.AllowUserToDeleteRows = false;
            this.dgvLogs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvLogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLogs.Location = new System.Drawing.Point(12, 57);
            this.dgvLogs.Name = "dgvLogs";
            this.dgvLogs.ReadOnly = true;
            this.dgvLogs.Size = new System.Drawing.Size(814, 372);
            this.dgvLogs.TabIndex = 0;
            // 
            // btnConfigurar
            // 
            this.btnConfigurar.Location = new System.Drawing.Point(751, 12);
            this.btnConfigurar.Name = "btnConfigurar";
            this.btnConfigurar.Size = new System.Drawing.Size(75, 39);
            this.btnConfigurar.TabIndex = 1;
            this.btnConfigurar.Text = "Configurar";
            this.btnConfigurar.UseVisualStyleBackColor = true;
            this.btnConfigurar.Click += new System.EventHandler(this.btnConfigurar_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.Color.Red;
            this.lblMensaje.Location = new System.Drawing.Point(12, 25);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(26, 13);
            this.lblMensaje.TabIndex = 11;
            this.lblMensaje.Text = "Msj";
            this.lblMensaje.Visible = false;
            // 
            // tmrEjecuta
            // 
            this.tmrEjecuta.Tick += new System.EventHandler(this.tmrEjecuta_Tick);
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(751, 531);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 39);
            this.btnEjecutar.TabIndex = 12;
            this.btnEjecutar.Text = "Ejecución Manual";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // lblTituloAccion
            // 
            this.lblTituloAccion.AutoSize = true;
            this.lblTituloAccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloAccion.Location = new System.Drawing.Point(12, 527);
            this.lblTituloAccion.Name = "lblTituloAccion";
            this.lblTituloAccion.Size = new System.Drawing.Size(58, 17);
            this.lblTituloAccion.TabIndex = 13;
            this.lblTituloAccion.Text = "Accion :";
            // 
            // lblAccion
            // 
            this.lblAccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccion.Location = new System.Drawing.Point(76, 527);
            this.lblAccion.Name = "lblAccion";
            this.lblAccion.Size = new System.Drawing.Size(669, 17);
            this.lblAccion.TabIndex = 14;
            this.lblAccion.Text = "Inicio";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 547);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(733, 23);
            this.progressBar1.TabIndex = 15;
            // 
            // chbEjecutado
            // 
            this.chbEjecutado.AutoSize = true;
            this.chbEjecutado.Location = new System.Drawing.Point(665, 25);
            this.chbEjecutado.Name = "chbEjecutado";
            this.chbEjecutado.Size = new System.Drawing.Size(80, 17);
            this.chbEjecutado.TabIndex = 16;
            this.chbEjecutado.Text = "checkBox1";
            this.chbEjecutado.UseVisualStyleBackColor = true;
            this.chbEjecutado.Visible = false;
            // 
            // lst_error
            // 
            this.lst_error.FormattingEnabled = true;
            this.lst_error.Location = new System.Drawing.Point(12, 458);
            this.lst_error.Name = "lst_error";
            this.lst_error.Size = new System.Drawing.Size(814, 69);
            this.lst_error.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 442);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Consultas pendientes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(714, 436);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "0";
            // 
            // frmEnviaTickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 580);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lst_error);
            this.Controls.Add(this.chbEjecutado);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblAccion);
            this.Controls.Add(this.lblTituloAccion);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.btnConfigurar);
            this.Controls.Add(this.dgvLogs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmEnviaTickets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Envío de Tickets";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEnviaTickets_FormClosing);
            this.Load += new System.EventHandler(this.frmEnviaTickets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLogs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLogs;
        private System.Windows.Forms.Button btnConfigurar;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Timer tmrEjecuta;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Label lblTituloAccion;
        private System.Windows.Forms.Label lblAccion;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox chbEjecutado;
        private System.Windows.Forms.ListBox lst_error;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}