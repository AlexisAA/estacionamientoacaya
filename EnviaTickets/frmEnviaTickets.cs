﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Management;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Configuration;

namespace EnviaTickets
{
    public partial class frmEnviaTickets : Form
    {

        string lbl = "";
        string path = "";
        string nombreApp = "EnviaTickets"; 

        /// <summary>
        /// Nombre de la carpeta para la configuración de la base de datos
        /// </summary>
        private static string directorioEspacia = "C:\\Espacia\\Config";
        
        /// <summary>
        /// Nombre del archivo de configuración de la base de datos
        /// </summary>
        private static string archivoConfig = "fileConfig.txt";
        private static string archivoLogs = "fileLogs.txt";
        private string minEjecucion;
        private string NoEstacionamiento;
        private String valorbody;

        public frmEnviaTickets()
        {
            InitializeComponent();
        }

        private void frmEnviaTickets_Load(object sender, EventArgs e)
        {
            //para guardar registro en regedit

           

            lbl = Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Remove(0, 6) + "\\" + lbl;
            nombreApp = "EnviaTickets";

            RegistryKey objKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            objKey.SetValue(nombreApp, path);

            //RegistryKey runK = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);
            //// añadirlo al registro
            //// Si el path contiene espacios se debería incluir entre comillas dobles
            //if (nombreApp.StartsWith("\"") == false && nombreApp.IndexOf(" ") > -1)
            //{
            //    nombreApp = "\"" + nombreApp + "\"";
            //}
            //runK.SetValue(path, nombreApp);

            //Primero determinamos si existe un archivo de configuración.
            minEjecucion = "";
            bool confExist = false;
            string pathConfig = "";

            #region Vonsulta logs
            pathConfig = directorioEspacia;
            if (Directory.Exists(pathConfig))
            {
                if (File.Exists(pathConfig + "\\" + archivoConfig))
                {
                    confExist = true;
                }
            }
            #endregion

            #region Valida Configuración
            if (!confExist)
            {
                //Como no se encontro archivo de configuración
                string message = "Primero debes configurar la aplicación";
                string caption = "Aplicacion no configurada";
                MessageBoxButtons button = MessageBoxButtons.OK;
                MessageBox.Show(message, caption, button);
            }
            else
            {
                //Inicializa timer si encuentra archivo de configuración
                int intPer = 0;
                int intInervalo = 0;
                minEjecucion = extraePeriodicidad();
                NoEstacionamiento = extraeestacionamiento();
                intPer = Convert.ToInt32(minEjecucion);
                intInervalo = (intPer * 60) * 1000;

                tmrEjecuta.Interval = intInervalo;
                tmrEjecuta.Tick += new EventHandler(tmrEjecuta_Tick);
                tmrEjecuta.Enabled = true;
                chbEjecutado.Checked = false;
                llenadgvLogs(directorioEspacia + "\\" + archivoLogs);
            }
            #endregion

        }

     
        private void btnConfigurar_Click(object sender, EventArgs e)
        {
            tmrEjecuta.Stop();
            frmConfiguracion frmConfig = new frmConfiguracion();
            frmConfig.Focus();
            frmConfig.Show();
        }

        private void tmrEjecuta_Tick(object sender, EventArgs e)
        {
            label2.Text = label2.Text + 1;
            if (AccesoInternet("facturacion.espacia.mx"))
            {
                enviaDatos();
            }
            else
            {

                MessageBox.Show("Sin conexion", "Titulo");
            }
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            if (AccesoInternet("facturacion.espacia.mx"))
            {
                enviaDatos();
            } else {
                if (lst_error.Items.Count > 0)
                {
                    // no hago nada porque ya esta el aviso
                }
                else {
                    lst_error.Items.Add("ERROR no se detecto conexión con el servidor {" + DateTime.Now + "}");
                    //MessageBox.Show("Sin conexion","Titulo");
                    //bool mesg = enviamsg();

                    
                }          
            }
                
        }

        #region Métodos y eventos del Load
        /// <summary>
        /// Metodo para determinar el numero del estacionamiento.
        /// </summary>
        /// <returns></returns>
        private string extraeestacionamiento()
        {
            string Estacionamiento = "";
            string strPath = "";
            strPath = directorioEspacia;

            if (Directory.Exists(strPath))
            {
                if (File.Exists(strPath + "\\" + archivoConfig))
                {
                    try
                    {
                        #region Lectura del archivo
                        StreamReader sr = new StreamReader(strPath + "\\" + archivoConfig);
                        char[] separador = { '|' };
                        //Contamos el numero de lineas del archivo
                        while (sr.Peek() != -1)
                        {
                            string text = "";
                            text = sr.ReadLine();
                            string[] campos = text.Split(separador);
                            if (campos.Count() > 0)
                            {
                                Estacionamiento = campos[0];
                            }
                        }
                        sr.Close();
                        sr.Dispose();
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = ex.Message;
                    }
                }
                else
                {
                    lblMensaje.Text = "No se encontro el archivo de configuración";
                    lblMensaje.Visible = true;
                }
            }
            else
            {
                lblMensaje.Text = "No se encontro la carpeta de Espacia";
                lblMensaje.Visible = true;
            }

            return Estacionamiento;

        }





        /// <summary>
        /// Metodo para determinar la periodicidad de la ejecución.
        /// </summary>
        /// <returns></returns>
        private string extraePeriodicidad()
        {
            string Periodicidad = "";
            string strPath = "";
            strPath = directorioEspacia;

            if (Directory.Exists(strPath))
            {
                if (File.Exists(strPath + "\\" + archivoConfig))
                {
                    try
                    {
                        #region Lectura del archivo
                        StreamReader sr = new StreamReader(strPath + "\\" + archivoConfig);
                        char[] separador = { '|' };
                        //Contamos el numero de lineas del archivo
                        while (sr.Peek() != -1)
                        {
                            string text = "";
                            text = sr.ReadLine();
                            string[] campos = text.Split(separador);
                            if (campos.Count() > 0)
                            {
                                Periodicidad = campos[5];
                            }
                        }
                        sr.Close();
                        sr.Dispose();
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = ex.Message;
                    }
                }
                else
                {
                    lblMensaje.Text = "No se encontro el archivo de configuración";
                    lblMensaje.Visible = true;
                }
            }
            else
            {
                lblMensaje.Text = "No se encontro la carpeta de Espacia";
                lblMensaje.Visible = true;
            }

            return Periodicidad;

        }
        #endregion

        #region Metodos y eventos comunes.
        /// <summary>
        /// Metodo para llenar el Data GridView
        /// </summary>
        /// <param name="_pathLogs"></param>
        private void llenadgvLogs(string _pathLogs)
        {
            if (File.Exists(_pathLogs))
            {
                #region Inicializa Tabla
                DataTable dtLog = new DataTable();
                dtLog.Columns.Add(new DataColumn("ID", typeof(Int32)));
                dtLog.Columns.Add(new DataColumn("Inicio", typeof(DateTime)));
                dtLog.Columns.Add(new DataColumn("Fin", typeof(DateTime)));
                dtLog.Columns.Add(new DataColumn("Accion", typeof(string)));
                dtLog.Columns.Add(new DataColumn("Resultado", typeof(string)));
                dtLog.Columns.Add(new DataColumn("Comentarios", typeof(string)));
                #endregion

                #region Lectura del Log
                StreamReader sr = new StreamReader(_pathLogs);
                char[] separador = { '|' };
                //Contamos el numero de lineas del archivo
                while (sr.Peek() != -1)
                {
                    Application.DoEvents();
                    string text = "";
                    text = sr.ReadLine();
                    string[] campos = text.Split(separador);
                    if (campos.Count() > 0)
                    {
                        DataRow drLog = dtLog.NewRow();
                        drLog["ID"] = Convert.ToInt32(campos[0]);
                        drLog["Inicio"] = Convert.ToDateTime(campos[1]);
                        drLog["Fin"] = Convert.ToDateTime(campos[2]);
                        drLog["Accion"] = campos[3];
                        drLog["Resultado"] = campos[4];
                        drLog["Comentarios"] = campos[5];

                        dtLog.Rows.Add(drLog);
                    }
                }

                sr.Close();
                sr.Dispose();
                #endregion

                #region Llenado de Grid
                dgvLogs.DataSource = dtLog;

                int ctaRows = -1;
                ctaRows = dgvLogs.Rows.Count;
                dgvLogs.Rows[ctaRows - 1].Selected = true;
                #endregion

            }
        }

        private void enviaDatos()
        {
            string pathLogs = "";
            pathLogs = directorioEspacia + "\\" + archivoLogs;
            Application.DoEvents();

            if (chbEjecutado.Checked == false)
            {
                DateTime fechaInicioProceso = new DateTime();
                //DateTime fechaFinProceso = new DateTime();
                DateTime fechaFinConsulta = new DateTime();

                fechaInicioProceso = DateTime.Now;
                fechaFinConsulta = fechaInicioProceso;
                DataTable dtConsulta = new DataTable();
                lblAccion.Text = "Inicia consulta";
                dtConsulta = realizaConsulta(fechaFinConsulta);
                if (dtConsulta != null)
                {
                    int ctaReg = 0;
                    ctaReg = dtConsulta.Rows.Count;
                    if (ctaReg > 0)
                    {
                        string resp = "";

                        wsEnviaTickets.ServiceEspacia ws = new wsEnviaTickets.ServiceEspacia();
                        BLSeguridad bls = new BLSeguridad();
                        try
                        {
                            string xRes = "Error";
                            string Password = "";
                            Password = bls.encriptar("Prime@123");
                            //Password = bls.encriptar("prime123");
                            resp = ws.recibeTickets("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                            //resp = ws.recibeTicketsSinEncriptar("Sistemas", Password, DateTime.Now, ctaReg, dtConsulta);
                            if (resp.IndexOf("insertaron") > 0)
                            {
                                xRes = "Ok";
                                lblAccion.Text = "";
                                lst_error.Items.Clear();
                            }

                            int noLineas = 0;
                            noLineas = cuentaLineas() + 1;
                            string result = "";
                            result = noLineas + "|" + fechaInicioProceso + "|" + DateTime.Now + "|" + "wsEnviaTickets" + "|" + xRes + "|" + resp;
                            guardaLog(result);

                        }
                        catch (Exception ex)
                        {
                            int noLineas = 0;
                            noLineas = cuentaLineas() + 1;
                            string result = "";
                            result = noLineas + "|" + fechaInicioProceso + "|" + DateTime.Now + "|" + "wsEnviaTickets" + "|" + "Error" + "|" + ex.Message;
                            guardaLog(result);
                            lblMensaje.Text = ex.Message;
                            lblMensaje.Visible = true;
                        }
                    }

                }
                chbEjecutado.Checked = true;
            }
            else
                chbEjecutado.Checked = false;

            Application.DoEvents();
            lblAccion.Text = "Llena Logs";
            llenadgvLogs(directorioEspacia + "\\" + archivoLogs);
            lblAccion.Text = "";
        }

        /// <summary>
        /// Metodo para hacer la consulta a la base de datos
        /// </summary>
        /// <param name="_fechafin"></param>
        /// <returns></returns>
        private DataTable realizaConsulta(DateTime _fechafin)
        {
            #region Crea Tabla
            DataTable dt = new DataTable();
            dt.TableName = "TicketsDT";
            dt.Columns.Add(new DataColumn("Número de Recibo", typeof(string)));
            dt.Columns.Add(new DataColumn("Codigo Sistema", typeof(string)));
            dt.Columns.Add(new DataColumn("Nombre Sistema", typeof(string)));
            dt.Columns.Add(new DataColumn("Fecha de Recibo", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("Importe Total", typeof(decimal)));
            dt.Columns.Add(new DataColumn("Forma de Pago", typeof(string)));
            dt.Columns.Add(new DataColumn("Últimos 4 dígitos Tarjeta", typeof(string)));
            #endregion

            Application.DoEvents();
            lblAccion.Text = "Inicia consulta a la base de datos";
            #region Determina periodo de consulta
            DateTime FechaInicio = DateTime.Now;
            DateTime FechaFin = new DateTime();

            FechaFin = _fechafin;
            int anio = FechaFin.Year;
            int mes = FechaFin.Month;
            int dia = FechaFin.Day;
            int hora = FechaFin.Hour;
            int minutos = FechaFin.Minute;
            int segundos = FechaFin.Second;

            //FechaInicio = new DateTime(anio, mes, dia, hora, minutos, segundos);
            if (lst_error.Items.Count > 0)
            {
                char[] separadorcad = { '{' };
                char[] separadorcadR = { '}' };
                string Valeerror = lst_error.Items[0].ToString();
                string[] campos = Valeerror.Split(separadorcad);

                if (campos.Count() > 0)
                {
                    string[] campos1 = campos[1].Split(separadorcadR);
                    String fechar = campos1[0];
                    FechaInicio = Convert.ToDateTime(campos1[0].ToString());
                }
            }
            else
            {
                FechaInicio = new DateTime(anio, mes, dia, 0, 0, 0);
            }

            //FechaInicio = new DateTime(anio, mes, dia, 0, 0, 0);
            FechaFin = new DateTime(anio, mes, dia, hora, minutos, segundos);
            #endregion

            Application.DoEvents();
            lblAccion.Text = "Consulta a la base de datos";
            #region Realiza consulta
            string cadenaConexion = "";
            cadenaConexion = extraeCadenadeConexion();
            if (cadenaConexion != "")
            {
                string consulta = "";
                //consulta = "sp_DetalleRecibidos";
                consulta = "[MeyparAdmin].[SP_DetalleRecibos]";

                SqlConnection conexion = new SqlConnection();
                conexion.ConnectionString = cadenaConexion;
                SqlCommand comando = new SqlCommand(consulta, conexion);
                comando.CommandType = CommandType.StoredProcedure;
                //comando.Parameters.Add("@Fecha_Inicio", SqlDbType.DateTime).Value = FechaInicio.ToString("yyyy-MM-dd HH':'mm':'ss");
                //comando.Parameters.Add("@Fecha_Fin", SqlDbType.DateTime).Value = FechaFin.ToString("yyyy-MM-dd HH':'mm':'ss");
                comando.Parameters.Add("@Fecha_Inicial", SqlDbType.DateTime).Value = FechaInicio.ToString("yyyy-MM-dd HH':'mm':'ss");
                comando.Parameters.Add("@Fecha_Final", SqlDbType.DateTime).Value = FechaFin.ToString("yyyy-MM-dd HH':'mm':'ss");
                SqlDataAdapter adapter = new SqlDataAdapter(comando);
                try
                {
                    conexion.Open();
                    adapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    int noLineas = 0;
                    noLineas = cuentaLineas() + 1;
                    string result = "";
                    result = noLineas + "|" + _fechafin + "|" + DateTime.Now + "|" + "realizaConsulta" + "|" + "Error" + "|" + ex.Message;
                    guardaLog(result);
                    lblMensaje.Text = ex.Message;
                    lblMensaje.Visible = true;
                }
                finally
                {
                    conexion.Close();
                    conexion.Dispose();
                }
            }
            #endregion

            return dt;

        }

        /// <summary>
        /// Metodo para extrae la cadena de conexion
        /// </summary>
        /// <returns>Cadena de conexión</returns>
        private string extraeCadenadeConexion()
        {
            string Cadena = "";
            string Servidor = "";
            string Tabla = "";
            string Usuario = "";
            string Password = "";

            string strPath = "";
            strPath = directorioEspacia;

            if (Directory.Exists(strPath))
            {
                if (File.Exists(strPath + "\\" + archivoConfig))
                {
                    try
                    {
                        #region Lectura del archivo
                        StreamReader sr = new StreamReader(strPath + "\\" + archivoConfig);
                        char[] separador = { '|' };
                        //Contamos el numero de lineas del archivo
                        BLSeguridad bls = new BLSeguridad();
                        while (sr.Peek() != -1)
                        {
                            string text = "";
                            text = sr.ReadLine();
                            string[] campos = text.Split(separador);
                            if (campos.Count() > 0)
                            {
                                Servidor = campos[1];
                                Tabla = campos[2];
                                Usuario = campos[3];
                                Password = bls.desencriptar(campos[4]);
                            }
                        }
                        sr.Close();
                        sr.Dispose();
                        #endregion

                        Cadena = "Data Source=" + Servidor + ";Initial Catalog=" + Tabla + ";User ID=" + Usuario + ";Password=" + Password + "";

                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = ex.Message;
                    }
                }
            }
            else
            {
                lblMensaje.Visible = true;
                lblMensaje.Text = "No se encontro el archivo de configuración";
            }

            return Cadena;
        }
        
        #endregion

        private void determinaEjecución()
        {
            //Extrae información de la periodicidad
            string strPeriodicidad = "";
            int intPer = 0;
            int ctaVeces = 0;
            int sumVeces = 0;
            string strResul = "";
            strPeriodicidad = extraePeriodicidad();
            intPer = Convert.ToInt32(strPeriodicidad);
            ctaVeces = 60 / intPer;
            for(int n = 0; n < ctaVeces; n++)
            {
                string strVeces = "";
                if(n == 0)
                {
                    strVeces = "00";
                }
                else
                {
                    sumVeces = sumVeces + intPer;
                    if (sumVeces.ToString().Length == 1)
                        strVeces = "0" + sumVeces.ToString();
                    else
                    {
                        if (strVeces != "60")
                            strVeces = sumVeces.ToString();
                    }
                }
                strVeces = strVeces + ", ";

                strResul = strResul + strVeces;
                    
            }

            minEjecucion = strResul;

        }

        #region Guarda Logs
        /// <summary>
        /// Metodo para contar el numero de lineas el Log
        /// </summary>
        /// <returns>Numero de lineas</returns>
        private int cuentaLineas()
        {
            int noLineas = 0;
            string pathLogs = "";
            pathLogs = directorioEspacia + "\\" + archivoLogs;
            try
            {
                if (File.Exists(pathLogs))
                {
                    StreamReader sr = new StreamReader(pathLogs);
                    while (sr.Peek() != -1)
                    {
                        sr.ReadLine();
                        noLineas = noLineas + 1;
                    }

                    sr.Close();
                    sr.Dispose();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lblMensaje.Visible = true;
            }

            return noLineas;
        }

        /// <summary>
        /// Metodo para guardar registro en el Log
        /// </summary>
        /// <param name="resultado"></param>
        private void guardaLog(string resultado)
        {
            string path = "";
            try
            {
                path = directorioEspacia;
                if (Directory.Exists(path))
                {
                    if (File.Exists(path + "\\" + archivoLogs))
                    {
                        StreamWriter sw = File.AppendText(path + "\\" + archivoLogs);
                        sw.WriteLine(resultado);
                        sw.Close();
                        sw.Dispose();
                    }
                    else
                    {
                        using (StreamWriter escribir = new StreamWriter(path + "\\" + archivoLogs))
                        {
                            escribir.WriteLine(resultado);
                            escribir.Close();
                            escribir.Dispose();
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(path);
                    using (StreamWriter escribir = new StreamWriter(path + "\\" + archivoLogs))
                    {
                        escribir.WriteLine(resultado);
                        escribir.Close();
                        escribir.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lblMensaje.Visible = true;
            }
        }
        #endregion

        private void frmEnviaTickets_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show("No esta autorizado para cerrar esta aplicación","Envio de Tickets");
            {
                e.Cancel = true;
            }
        }

        private bool AccesoInternet(string mURL)
        {

            try
            {
                System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(mURL);
                return true;

            }
            catch
            {

                return false;
            }

        }

        private bool enviamsg()
        {
            //Datos para la configuración del correo
            //string strMensaje = "";
            string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();  
            string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
            string strCC = ConfigurationManager.AppSettings["mailCC"].ToString();
            string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
            int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
            bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

            SmtpClient datosSmtp = new SmtpClient();
            datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
            datosSmtp.Host = strServer;
            datosSmtp.Port = intPuerto;
            datosSmtp.EnableSsl = ssl;

            //Datos del correo
            MailMessage objetoMail = new MailMessage();
            MailAddress mailRemitente = new MailAddress(strRemitente);
            objetoMail.From = mailRemitente;
            //objetoMail.AlternateViews.Add(plainView);
            //objetoMail.AlternateViews.Add(htmlView);
            
            MailAddress mailDestinatario = new MailAddress(strRemitente);
            //objetoMail.To.Add(mailDestinatario);
            objetoMail.To.Add("jtrume@gmail.com");
            objetoMail.Subject = "Mensaje de error en el estacionamiento {" + NoEstacionamiento + "}";
            valorbody = "El estacionamiento {" + NoEstacionamiento + "} no pudo enviar los tickets, fecha: {" + DateTime.Now + "}";
            objetoMail.Body = valorbody;

        
            //Datos de las copias
            #region Agrega copias
            //string[] addMailsCC = correo.Split('|');

            //foreach (string mail in addMailsCC)
            //{
            //    if (mail != "")
            //    {
            //        MailAddress mailCC = new MailAddress(mail);
            //        objetoMail.CC.Add(mailCC);
            //    }
            //}


            #endregion

            #region Adjunta archivos
            //try
            //{
            //    Attachment adjunaXML = new Attachment(_path + _nombreXML);
            //    objetoMail.Attachments.Add(adjunaXML);

            //    Attachment adjunaPDF = new Attachment(_path + _nombrePDF);
            //    objetoMail.Attachments.Add(adjunaPDF);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            #endregion


            try
            {
                datosSmtp.Send(objetoMail);
                datosSmtp.Dispose();
                //resp = "Ok";
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

    }
}
