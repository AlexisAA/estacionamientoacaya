﻿using System;
using System.Linq;
using System.Windows.Forms;

using System.IO;
using System.Data.SqlClient;

namespace EnviaTickets
{
    public partial class frmConfiguracion : Form
    {
        /// <summary>
        /// Nombre de la carpeta para la configuración de la base de datos
        /// </summary>
        private static string nombreDirectorio = "C:\\Espacia\\Config";
        /// <summary>
        /// Nombre del archivo de configuración de la base de datos
        /// </summary>
        private static string nombreArchivo = "fileConfig.txt";

        public frmConfiguracion()
        {
            InitializeComponent();
        }

        private void frmConfiguracion_Load(object sender, EventArgs e)
        {
            this.Focus();
            //Buscamos el archivo de configuración
            string strPath = "";
            strPath = nombreDirectorio;

            if(Directory.Exists(strPath))
            {
                if (File.Exists(strPath + "\\" + nombreArchivo))
                {
                    try
                    {
                        BLSeguridad bls = new BLSeguridad();
                        #region Lectura del archivo
                        StreamReader sr = new StreamReader(strPath + "\\" + nombreArchivo);
                        char[] separador = { '|' };
                        //Contamos el numero de lineas del archivo
                        while (sr.Peek() != -1)
                        {
                            string text = "";
                            text = sr.ReadLine();
                            string[] campos = text.Split(separador);
                            if (campos.Count() > 0)
                            {
                                txtNoEtacionamiento.Text = campos[0];
                                txtServidor.Text = campos[1];
                                txtTabla.Text = campos[2];
                                txtUsuario.Text = campos[3];
                                txtPassword.Text = bls.desencriptar(campos[4]);
                                nudTiempo.Value= Convert.ToInt32(campos[5]);
                            }
                        }
                        sr.Close();
                        sr.Dispose();
                        #endregion
                    }
                    catch(Exception ex)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = ex.Message;
                    }
                }
            }

        }

        private void btnProbar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            lblMensaje.Visible = false;

            string respValida = "";
            respValida = validaConfiguracion();
            if(respValida == "")
            {
                respValida = pruebaConexion();
                if (respValida == "")
                {
                    lblMensaje.Visible = true;
                    lblMensaje.Text = "Prueba de conexión exitosa";
                    txtNoEtacionamiento.Enabled = false;
                    txtServidor.Enabled = false;
                    txtTabla.Enabled = false;
                    txtUsuario.Enabled = false;
                    txtPassword.Enabled = false;
                    btnProbar.Enabled = false;
                    btnGuardar.Enabled = true;
                }
                else
                {
                    lblMensaje.Text = respValida;
                    lblMensaje.Visible = true;
                }
            }
            else
            {
                lblMensaje.Visible = true;
                lblMensaje.Text = respValida;
            }
        }

        /// <summary>
        /// Metodo para guardar la información de configuración de la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMensaje.Text = "";
            string strPath = "";
            strPath = nombreDirectorio;

            try
            {
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);
                else
                {
                    if (File.Exists(strPath + "\\" + nombreArchivo))
                        File.Delete(strPath + "\\" + nombreArchivo);
                }
            }
            catch(Exception ex)
            {
                lblMensaje.Text = ex.Message;
                lblMensaje.Visible = true;
            }
            

            string NoEstacionamiento = "";
            string Servidor = "";
            string Tabla = "";
            string Usuario = "";
            string Password = "";
            string Frecuencia = "";
            string Cadena = "";

            NoEstacionamiento = txtNoEtacionamiento.Text.Trim();
            Servidor = txtServidor.Text.Trim();
            Tabla = txtTabla.Text.Trim();
            Usuario = txtUsuario.Text.Trim();
            Frecuencia = nudTiempo.Value.ToString();
            //Encriptamos la contraseña
            BLSeguridad bls = new BLSeguridad();
            try
            {
                Password = bls.encriptar(txtPassword.Text.Trim());
                Cadena = NoEstacionamiento + "|" + Servidor + "|" + Tabla + "|" + Usuario + "|" + Password + "|" + Frecuencia;
                try
                {
                    using (StreamWriter escribe = new StreamWriter(strPath + "\\" + nombreArchivo))
                    {
                        escribe.Write(Cadena);
                        escribe.Close();
                        escribe.Dispose();
                    }

                    lblMensaje.Visible = true;
                    lblMensaje.Text = "Información guardada correctamente. Se be reiniciar la aplicación para que tome los cambios.";

                }
                catch(Exception ex)
                {
                    lblMensaje.Visible = true;
                    lblMensaje.Text = "Error al crear el archivo de configuracion " + ex.Message;
                }

            }
            catch (Exception ex)
            {
                lblMensaje.Visible = true;
                lblMensaje.Text = "Error al encriptar la contraseña " + ex.Message;
            }
        }

        /// <summary>
        /// Metodo para validar la información de los textos
        /// </summary>
        /// <returns></returns>
        private string validaConfiguracion()
        {
            string resp = "";
            if (txtNoEtacionamiento.Text.Length < 2)
                resp = "Debes ingresar un numero de estacionamiento valido";
            else
            {
                if (txtServidor.Text.Length < 5)
                    resp = "Debes ingresar un nombre de servidor valido";
                else
                {
                    if (txtTabla.Text.Length < 5)
                        resp = "Debes ingresar un nombre de base de datos inicial valido";
                    else
                    {
                        if (txtUsuario.Text.Length < 2)
                            resp = "Debes ingresar un nombre de usuario valido";
                        else
                        {
                            if (txtPassword.Text.Length < 2)
                                resp = "Debes ingresar una contraseña valida";
                        }
                    }
                }
            }
            return resp;
        }

        /// <summary>
        /// Metodo para probar la conexion a la base de datos.
        /// </summary>
        /// <returns></returns>
        private string pruebaConexion()
        {
            //Data Source=CARELLANO\SQL2008R2;Initial Catalog=RoportingMazcoa;Integrated Security=True" providerName="System.Data.SqlClient
            string resp = "";
            string Servidor = "";
            string Tabla = "";
            string Usuario = "";
            string Password = "";
            string cadenaConexion = "";
            //string providername = "";
            BLSeguridad bls = new BLSeguridad();

            Servidor = txtServidor.Text.Trim();
            Tabla = txtTabla.Text.Trim();
            Usuario = txtUsuario.Text.Trim();
            Password = txtPassword.Text.Trim();

            cadenaConexion = "Data Source=" + Servidor + ";Initial Catalog=" + Tabla + ";User ID="+Usuario+ ";Password="+ Password +"";
            //providername = "System.Data.SqlClient";
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = cadenaConexion;
            try
            {
                conexion.Open();
            }
            catch(Exception ex)
            {
                resp = ex.Message;
            }
            finally
            {
                conexion.Close();
                conexion.Dispose();
            }

            return resp;
        }
        
    }
}
