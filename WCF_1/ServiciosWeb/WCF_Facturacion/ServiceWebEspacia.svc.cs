﻿using BL.Catalogos;
using BL.Otros;
using BL.Portal;
using DAL.Bean.Catalogos;
using DAL.Bean.Portal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using WCF_Facturacion.Utilerias;
using System.Security.Cryptography;

namespace WCF_Facturacion
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceWebEspacia" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceWebEspacia.svc or ServiceWebEspacia.svc.cs at the Solution Explorer and start debugging.
   

    public class ServiceWebEspacia : IServiceWebEspacia
    {
        private static string _privateKey = "<RSAKeyValue><Modulus>s6lpjspk+3o2GOK5TM7JySARhhxE5gB96e9XLSSRuWY2W9F951MfistKRzVtg0cjJTdSk5mnWAVHLfKOEqp8PszpJx9z4IaRCwQ937KJmn2/2VyjcUsCsor+fdbIHOiJpaxBlsuI9N++4MgF/jb0tOVudiUutDqqDut7rhrB/oc=</Modulus><Exponent>AQAB</Exponent><P>3J2+VWMVWcuLjjnLULe5TmSN7ts0n/TPJqe+bg9avuewu1rDsz+OBfP66/+rpYMs5+JolDceZSiOT+ACW2Neuw==</P><Q>0HogL5BnWjj9BlfpILQt8ajJnBHYrCiPaJ4npghdD5n/JYV8BNOiOP1T7u1xmvtr2U4mMObE17rZjNOTa1rQpQ==</Q><DP>jbXh2dVQlKJznUMwf0PUiy96IDC8R/cnzQu4/ddtEe2fj2lJBe3QG7DRwCA1sJZnFPhQ9svFAXOgnlwlB3D4Gw==</DP><DQ>evrP6b8BeNONTySkvUoMoDW1WH+elVAH6OsC8IqWexGY1YV8t0wwsfWegZ9IGOifojzbgpVfIPN0SgK1P+r+kQ==</DQ><InverseQ>LeEoFGI+IOY/J+9SjCPKAKduP280epOTeSKxs115gW1b9CP4glavkUcfQTzkTPe2t21kl1OrnvXEe5Wrzkk8rA==</InverseQ><D>HD0rn0sGtlROPnkcgQsbwmYs+vRki/ZV1DhPboQJ96cuMh5qeLqjAZDUev7V2MWMq6PXceW73OTvfDRcymhLoNvobE4Ekiwc87+TwzS3811mOmt5DJya9SliqU/ro+iEicjO4v3nC+HujdpDh9CVXfUAWebKnd7Vo5p6LwC9nIk=</D></RSAKeyValue>";
        private static string _publicKey = "<RSAKeyValue><Modulus>s6lpjspk+3o2GOK5TM7JySARhhxE5gB96e9XLSSRuWY2W9F951MfistKRzVtg0cjJTdSk5mnWAVHLfKOEqp8PszpJx9z4IaRCwQ937KJmn2/2VyjcUsCsor+fdbIHOiJpaxBlsuI9N++4MgF/jb0tOVudiUutDqqDut7rhrB/oc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        private ResponseWS response = new ResponseWS();
        private ErrorSistema error = new ErrorSistema();
        private ResponseEncriptarDesencriptar respuestaED = new ResponseEncriptarDesencriptar();
        private int ID_Usuario;

        public static string Decrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            var dataArray = data.Split(new char[] { ',' });
            byte[] dataByte = new byte[dataArray.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataByte[i] = Convert.ToByte(dataArray[i]);
            }

            rsa.FromXmlString(_privateKey);
            var decryptedByte = rsa.Decrypt(dataByte, false);
            return _encoder.GetString(decryptedByte);
        }
        public static string Encrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_publicKey);
            var dataToEncrypt = _encoder.GetBytes(data);
            var encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            var length = encryptedByteArray.Count();
            var item = 0;
            var sb = new StringBuilder();
            foreach (var x in encryptedByteArray)
            {
                item++;
                sb.Append(x);

                if (item < length)
                    sb.Append(",");
            }

            return sb.ToString();
        }

        public string Hola(string name) {
            return name;
        }
        public string HelloWorld(int nciclos)
        {
            int iniAltura = 1;
            int finAltura = iniAltura;
            bool primavera = true;

            for(int x = 0; x < nciclos; x++)
            {
                if(primavera)
                {
                    finAltura = iniAltura + 1;
                    primavera = false;
                }
                else
                {
                    finAltura++;
                    primavera = true;
                }
            }

            return finAltura.ToString();
            //try
            //{
            //    DateTime fecha = new DateTime();
            //    fecha = DateTime.Parse(cadena, CultureInfo.CreateSpecificCulture("en-US"));
            //    return fecha.ToString();
            //}
            //catch (Exception exception)
            //{
            //    error.Mensaje = exception.Message;
            //    if (exception.InnerException != null)
            //        error.MensajeInterno = exception.InnerException.Message.ToString();

            //    error.Numero = exception.HResult;
            //    throw new FaultException<ErrorSistema>(error);
            //}

        }

        public decimal DividebyZero(decimal input)
        {
            try
            {
                decimal denominator = 0;
                return input / denominator;

            }
            catch (Exception exception)
            {
                error.Mensaje = exception.Message;
                if (exception.InnerException != null)
                    error.MensajeInterno = exception.InnerException.Message.ToString();

                error.Numero = exception.HResult;
                throw new FaultException<ErrorSistema>(error);
            }
        }

        public ResponseEncriptarDesencriptar EcriptarInfomacion(string textoencriptar)
        {
            EncripDesencrip classED = new EncripDesencrip();
            try
            {
                if (!string.IsNullOrEmpty(textoencriptar))
                {
                    string _PassWord = System.Configuration.ConfigurationManager.AppSettings["PasswordEncriptacion"].ToString();

                    //respuestaED.MensajeEncriptado = classED.EncriptarCadenaDeCaracteres(textoencriptar, _PassWord);
                    //respuestaED.Numero = 1;
                    //respuestaED.Sucess = true;
                    respuestaED.MensajeDesencriptado = ServiceWebEspacia.Encrypt(textoencriptar);
                    respuestaED.Numero = 1;
                    respuestaED.Sucess = true;
                }
                else
                {
                    error.Mensaje = "Error|" + "El texto es vacio. Verifique su información.";
                    throw new Exception(error.Mensaje);
                }

                return respuestaED;

            }
            catch (Exception exception)
            {
                error.Mensaje = exception.Message;
                if (exception.InnerException != null)
                    error.MensajeInterno = exception.InnerException.Message.ToString();

                error.Numero = exception.HResult;
                throw new FaultException<ErrorSistema>(error);
            }
        }

        public ResponseEncriptarDesencriptar DesecriptarInfomacion(string textodesencriptar)
        {
            EncripDesencrip classED = new EncripDesencrip();
            try
            {
                if (!string.IsNullOrEmpty(textodesencriptar))
                {
                    string _PassWord = System.Configuration.ConfigurationManager.AppSettings["PasswordEncriptacion"].ToString();

                    //respuestaED.MensajeDesencriptado = classED.DesencriptarCadenaDeCaracteres(textodesencriptar, _PassWord);
                    //string[] array_user = respuestaED.MensajeDesencriptado.Split('|');
                    //respuestaED.MensajeEncriptado = "Usuario: " + array_user[0] + " \t Password: " + array_user[1];
                    respuestaED.MensajeDesencriptado = ServiceWebEspacia.Decrypt(textodesencriptar);
                    respuestaED.Numero = 1;
                    respuestaED.Sucess = true;
                }
                else
                {
                    error.Mensaje = "Error|" + "El texto es vacio. Verifique su información.";
                    throw new Exception(error.Mensaje);
                }

                return respuestaED;
            }
            catch (Exception exception)
            {
                error.Mensaje = "Error no se desencripto el texto: " + exception.Message;
                if (exception.InnerException != null)
                    error.MensajeInterno = exception.InnerException.Message.ToString();

                error.Numero = exception.HResult;

                throw new FaultException<ErrorSistema>(error);
            }
        }

        /// <summary>
        /// Metodo recibe tikects 
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="contrasenia"></param>
        /// <param name="fecha"></param>
        /// <param name="no_registros"></param>
        /// <param name="tickets"></param>
        /// <returns>Objeto ResponseWS</returns>
        public ResponseWS recibeTicketsSinEncriptar(string usuario, string contrasenia, DateTime fecha, int no_registros, DataTable tickets)
        {
            string respWS = "";
            ID_Usuario = -1;

            ID_Usuario = validaUsuario(usuario, contrasenia);
            if (ID_Usuario >= 0)
            {
                if (no_registros > 0)
                {
                    if (tickets != null)
                    {
                        //Insertamos los registros
                        //int Reg_Recibidos = 0;
                        int Reg_Insertados = 0;
                        int Reg_Actualizados = 0;
                        int Reg_NoActualizado = 0;
                        int Reg_Error = 0;
                        string sReg_Insertados = "";
                        string sReg_Actualizados = "";
                        string sReg_NoActualizado = "";
                        string sReg_Error = "";


                        DataTable dtTickets = new DataTable();
                        dtTickets = tickets;

                        int No_Registros = 0;
                        No_Registros = dtTickets.Rows.Count;
                        if (No_Registros == no_registros)
                        {
                            DateTime Fecha_Carga = new DateTime();
                            Fecha_Carga = DateTime.Now;
                            for (int n = 0; n < No_Registros; n++)
                            {

                                #region Variables
                                string No_Ticket = "";
                                string Id_Sistema = "";
                                string Nombre_Sistema = "";
                                //string fecha_in = "";
                                DateTime Fecha_Ticket = new DateTime();
                                decimal Importe_Total = 0;
                                string Forma_Pago = "";
                                string No_Tarjeta = "";
                                //string strft = "";
                                #endregion

                                #region Asignacion
                                try
                                {
                                    Id_Sistema = dtTickets.Rows[n][0].ToString();
                                    Nombre_Sistema = dtTickets.Rows[n][1].ToString();
                                    Fecha_Ticket = Convert.ToDateTime(dtTickets.Rows[n][2].ToString());
                                    No_Ticket = dtTickets.Rows[n][3].ToString();
                                    Importe_Total = Convert.ToDecimal(dtTickets.Rows[n][4].ToString());
                                    Forma_Pago = dtTickets.Rows[n][5].ToString();
                                    No_Tarjeta = dtTickets.Rows[n][6].ToString();

                                    #region X
                                    BLTickets_Facturados bltf = new BLTickets_Facturados();
                                    BLTickets blbuscat = new BLTickets();
                                    BLTickets blinsertat = new BLTickets();
                                    BLTickets blactualizat = new BLTickets();
                                    try
                                    {
                                        List<Tickets> csTickets = blbuscat.buscaTicket(No_Ticket);
                                        if (csTickets.Count > 0)
                                        {
                                            //Buscamos ue no este facturaddo
                                            List<Tickets_Facturados> csTf = bltf.buscaTickets_Facturados(No_Ticket);
                                            if (csTf.Count > 0)
                                            {
                                                bool Valido = false;
                                                Valido = csTf[0].Valido;
                                                if (!Valido)     //Significa que se facturo pero que se cancelo la factura por algun motivo
                                                {
                                                    #region Actualiza
                                                    bool actualiza = false;
                                                    actualiza = blactualizat.actualizaTicket(No_Ticket, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta);
                                                    if (actualiza)
                                                    {
                                                        Reg_Actualizados = Reg_Actualizados + 1;
                                                        if (sReg_Actualizados == "")
                                                            sReg_Actualizados = No_Ticket;
                                                        else
                                                            sReg_Actualizados = sReg_Actualizados + ", " + No_Ticket;
                                                    }
                                                    else
                                                    {
                                                        Reg_Error = Reg_Error + 1;
                                                        if (sReg_Error == "")
                                                            sReg_Error = No_Ticket;
                                                        else
                                                            sReg_Error = sReg_Error + ", " + No_Ticket;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    //No actualiza porque el ticket ya esta facturado
                                                    Reg_NoActualizado = Reg_NoActualizado + 1;
                                                    if (sReg_NoActualizado == "")
                                                        sReg_NoActualizado = No_Ticket;
                                                    else
                                                        sReg_NoActualizado = sReg_NoActualizado + ", " + No_Ticket;
                                                }
                                            }
                                            else
                                            {
                                                //Actualiza ya que no se encuentra en la tabla de facturados
                                                #region Actualiza
                                                bool actualiza = false;
                                                actualiza = blactualizat.actualizaTicket(No_Ticket, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta);
                                                if (actualiza)
                                                {
                                                    Reg_Actualizados = Reg_Actualizados + 1;
                                                    if (sReg_Actualizados == "")
                                                        sReg_Actualizados = No_Ticket;
                                                    else
                                                        sReg_Actualizados = sReg_Actualizados + ", " + No_Ticket;
                                                }
                                                else
                                                {
                                                    Reg_Error = Reg_Error + 1;
                                                    if (sReg_Error == "")
                                                        sReg_Error = No_Ticket;
                                                    else
                                                        sReg_Error = sReg_Error + ", " + No_Ticket;
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region Inserta
                                            bool insert = false;

                                            insert = blinsertat.insertaTicket(No_Ticket, Id_Sistema, Nombre_Sistema, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta, Fecha_Carga);

                                            if (insert)
                                            {
                                                insertaLog("recibeTickets", "Inserta", "Ok", "Ticket insertado: " + No_Ticket, false);
                                                Reg_Insertados = Reg_Insertados + 1;
                                                if (sReg_Insertados == "")
                                                    sReg_Insertados = No_Ticket;
                                                else
                                                    sReg_Insertados = sReg_Insertados + ", " + No_Ticket;
                                            }
                                            else
                                            {
                                                insertaLog("recibeTickets", "Inserta", "Error", "Ticket no insertado: " + No_Ticket, false);
                                                Reg_Error = Reg_Error + 1;
                                                if (sReg_Error == "")
                                                    sReg_Error = No_Ticket;
                                                else
                                                    sReg_Error = sReg_Error + ", " + No_Ticket;
                                            }
                                            #endregion
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        insertaLog("recibeTickets", "Inserta o Actualiza", "Error", ex.Message, true);
                                        respWS = "Inserta o Actualiza: " + ex.Message;
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("recibeTickets", "Asigna", "Error", No_Ticket, true);
                                    respWS = "Asignación de datos a variables : " + ex.Message;
                                }
                                #endregion
                            }

                            #region Insertamos información al Log del sistema
                            if (respWS == "")
                            {
                                BLLogs_Recepcion bllr = new BLLogs_Recepcion();
                                try
                                {
                                    bool insert = false;
                                    string Observa = "";
                                    Observa = "Se insertaron los registros: " + sReg_Insertados + " Se actualizaron los registros: " + sReg_Actualizados
                                        + " No se actualizarón los registros; " + sReg_NoActualizado + " Los registros con error son: " + sReg_Error;
                                    insert = bllr.insertaLog_Recepcion(ID_Usuario, DateTime.Now, No_Registros, Reg_Insertados, Reg_Actualizados, Reg_Error, Observa);
                                    respWS = Observa;

                                    response.Mensaje = Observa;
                                    response.Numero = 1;
                                    response.Sucess = true;
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("recibeTickets", "BLLogs_Recepcion", "Error", ex.Message, true);

                                    error.Mensaje = "Error BLLogs_Recepcion:|" + ex.Message;

                                    if (ex.InnerException != null)
                                        error.MensajeInterno = ex.InnerException.Message.ToString();

                                    error.Numero = ex.HResult;
                                    throw new FaultException<ErrorSistema>(error);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            response.Mensaje = "El numero de registros reportados no coincide con el numero de registros de la tabla";
                            response.Numero = -1;
                            response.Sucess = false;
                        }
                    }
                    else
                    {
                        response.Mensaje = "La tabla esta vacia";
                        response.Numero = -1;
                        response.Sucess = false;
                    }
                }
                else
                {
                    response.Mensaje = "no_registros indica que no hay datos para insertar.";
                    response.Numero = -1;
                    response.Sucess = false;
                }
            }
            else
            {
                response.Mensaje = "Usuario o contraseña invalidos";
                response.Numero = -1;
                response.Sucess = false;
            }

            return response;
        }

        /// <summary>
        /// Metodo recibe tikects donde encripta al usuario y contrasenia
        /// </summary>
        /// <param name="usuariocontrasenia_encriptado">usuario: array[0], contasenia: array[1]</param>
        /// <param name="fecha"></param>
        /// <param name="no_registros"></param>
        /// <param name="tickets"></param>
        /// <returns>objeto ResponseWS</returns>
        public ResponseWS recibeTicketsEncriptarUserPass(string usuariocontrasenia_encriptado, DateTime fecha, int no_registros, DataTable tickets)
        {
            EncripDesencrip classED = new EncripDesencrip();
            string respWS = "";
            ID_Usuario = -1;

            //string _PassWord = System.Configuration.ConfigurationManager.AppSettings["PasswordEncriptacion"].ToString();
            //string usuer = classED.DesencriptarCadenaDeCaracteres(usuariocontrasenia_encriptado, _PassWord);

            string usuer = ServiceWebEspacia.Decrypt(usuariocontrasenia_encriptado);

            string[] array_user = usuer.Split('|');

            ID_Usuario = validaUsuario(array_user[0], array_user[1]);
            #region Usuario Valido
            if (ID_Usuario >= 0)
            {
                if (no_registros > 0)
                {
                    if (tickets != null)
                    {
                        //Insertamos los registros
                        //int Reg_Recibidos = 0;
                        int Reg_Insertados = 0;
                        int Reg_Actualizados = 0;
                        int Reg_NoActualizado = 0;
                        int Reg_Error = 0;
                        string sReg_Insertados = "";
                        string sReg_Actualizados = "";
                        string sReg_NoActualizado = "";
                        string sReg_Error = "";


                        DataTable dtTickets = new DataTable();
                        dtTickets = tickets;

                        int No_Registros = 0;
                        No_Registros = dtTickets.Rows.Count;
                        if (No_Registros == no_registros)
                        {
                            DateTime Fecha_Carga = new DateTime();
                            Fecha_Carga = DateTime.Now;
                            for (int n = 0; n < No_Registros; n++)
                            {

                                #region Variables
                                string No_Ticket = "";
                                string Id_Sistema = "";
                                string Nombre_Sistema = "";
                                //string fecha_in = "";
                                DateTime Fecha_Ticket = new DateTime();
                                decimal Importe_Total = 0;
                                string Forma_Pago = "";
                                string No_Tarjeta = "";
                                //string strft = "";
                                #endregion

                                #region Asignacion
                                try
                                {
                                    Id_Sistema = dtTickets.Rows[n][0].ToString();
                                    Nombre_Sistema = dtTickets.Rows[n][1].ToString();
                                    Fecha_Ticket = Convert.ToDateTime(dtTickets.Rows[n][2].ToString());
                                    No_Ticket = dtTickets.Rows[n][3].ToString();
                                    Importe_Total = Convert.ToDecimal(dtTickets.Rows[n][4].ToString());
                                    Forma_Pago = dtTickets.Rows[n][5].ToString();
                                    No_Tarjeta = dtTickets.Rows[n][6].ToString();

                                    #region X
                                    BLTickets_Facturados bltf = new BLTickets_Facturados();
                                    BLTickets blbuscat = new BLTickets();
                                    BLTickets blinsertat = new BLTickets();
                                    BLTickets blactualizat = new BLTickets();
                                    try
                                    {
                                        List<Tickets> csTickets = blbuscat.buscaTicket(No_Ticket);
                                        if (csTickets.Count > 0)
                                        {
                                            //Buscamos ue no este facturaddo
                                            List<Tickets_Facturados> csTf = bltf.buscaTickets_Facturados(No_Ticket);
                                            if (csTf.Count > 0)
                                            {
                                                bool Valido = false;
                                                Valido = csTf[0].Valido;
                                                if (!Valido)     //Significa que se facturo pero que se cancelo la factura por algun motivo
                                                {
                                                    #region Actualiza
                                                    bool actualiza = false;
                                                    actualiza = blactualizat.actualizaTicket(No_Ticket, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta);
                                                    if (actualiza)
                                                    {
                                                        Reg_Actualizados = Reg_Actualizados + 1;
                                                        if (sReg_Actualizados == "")
                                                            sReg_Actualizados = No_Ticket;
                                                        else
                                                            sReg_Actualizados = sReg_Actualizados + ", " + No_Ticket;
                                                    }
                                                    else
                                                    {
                                                        Reg_Error = Reg_Error + 1;
                                                        if (sReg_Error == "")
                                                            sReg_Error = No_Ticket;
                                                        else
                                                            sReg_Error = sReg_Error + ", " + No_Ticket;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    //No actualiza porque el ticket ya esta facturado
                                                    Reg_NoActualizado = Reg_NoActualizado + 1;
                                                    if (sReg_NoActualizado == "")
                                                        sReg_NoActualizado = No_Ticket;
                                                    else
                                                        sReg_NoActualizado = sReg_NoActualizado + ", " + No_Ticket;
                                                }
                                            }
                                            else
                                            {
                                                //Actualiza ya que no se encuentra en la tabla de facturados
                                                #region Actualiza
                                                bool actualiza = false;
                                                actualiza = blactualizat.actualizaTicket(No_Ticket, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta);
                                                if (actualiza)
                                                {
                                                    Reg_Actualizados = Reg_Actualizados + 1;
                                                    if (sReg_Actualizados == "")
                                                        sReg_Actualizados = No_Ticket;
                                                    else
                                                        sReg_Actualizados = sReg_Actualizados + ", " + No_Ticket;
                                                }
                                                else
                                                {
                                                    Reg_Error = Reg_Error + 1;
                                                    if (sReg_Error == "")
                                                        sReg_Error = No_Ticket;
                                                    else
                                                        sReg_Error = sReg_Error + ", " + No_Ticket;
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region Inserta
                                            bool insert = false;

                                            insert = blinsertat.insertaTicket(No_Ticket, Id_Sistema, Nombre_Sistema, Fecha_Ticket, Importe_Total, Forma_Pago, No_Tarjeta, Fecha_Carga);

                                            if (insert)
                                            {
                                                insertaLog("recibeTickets", "Inserta", "Ok", "Ticket insertado: " + No_Ticket, false);
                                                Reg_Insertados = Reg_Insertados + 1;
                                                if (sReg_Insertados == "")
                                                    sReg_Insertados = No_Ticket;
                                                else
                                                    sReg_Insertados = sReg_Insertados + ", " + No_Ticket;
                                            }
                                            else
                                            {
                                                insertaLog("recibeTickets", "Inserta", "Error", "Ticket no insertado: " + No_Ticket, false);
                                                Reg_Error = Reg_Error + 1;
                                                if (sReg_Error == "")
                                                    sReg_Error = No_Ticket;
                                                else
                                                    sReg_Error = sReg_Error + ", " + No_Ticket;
                                            }
                                            #endregion
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        insertaLog("recibeTickets", "Inserta o Actualiza", "Error", ex.Message, true);
                                        respWS = "Inserta o Actualiza: " + ex.Message;
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("recibeTickets", "Asigna", "Error", No_Ticket, true);
                                    respWS = "Asignación de datos a variables : " + ex.Message;
                                }
                                #endregion
                            }

                            #region Insertamos información al Log del sistema
                            if (respWS == "")
                            {
                                BLLogs_Recepcion bllr = new BLLogs_Recepcion();
                                try
                                {
                                    bool insert = false;
                                    string Observa = "";
                                    Observa = "Se insertaron los registros: " + sReg_Insertados + " Se actualizaron los registros: " + sReg_Actualizados
                                        + " No se actualizarón los registros; " + sReg_NoActualizado + " Los registros con error son: " + sReg_Error;
                                    insert = bllr.insertaLog_Recepcion(ID_Usuario, DateTime.Now, No_Registros, Reg_Insertados, Reg_Actualizados, Reg_Error, Observa);
                                    respWS = Observa;

                                    response.Mensaje = Observa;
                                    response.Numero = 1;
                                    response.Sucess = true;
                                }
                                catch (Exception ex)
                                {
                                    insertaLog("recibeTickets", "BLLogs_Recepcion", "Error", ex.Message, true);

                                    error.Mensaje = "Error BLLogs_Recepcion:|" + ex.Message;

                                    if (ex.InnerException != null)
                                        error.MensajeInterno = ex.InnerException.Message.ToString();

                                    error.Numero = ex.HResult;
                                    throw new FaultException<ErrorSistema>(error);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            response.Mensaje = "El numero de registros reportados no coincide con el numero de registros de la tabla";
                            response.Numero = -1;
                            response.Sucess = false;
                        }
                    }
                    else
                    {
                        response.Mensaje = "La tabla esta vacia";
                        response.Numero = -1;
                        response.Sucess = false;
                    }
                }
                else
                {
                    response.Mensaje = "no_registros indica que no hay datos para insertar.";
                    response.Numero = -1;
                    response.Sucess = false;
                }
            }
            else
            {
                response.Mensaje = "Usuario o contraseña invalidos";
                response.Numero = -1;
                response.Sucess = false;
            }
            #endregion

            return response;
        }

        /// <summary>
        /// Metodo de ABC-Clientes tikects
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="contrasenia"></param>
        /// <param name="rfc"></param>
        /// <param name="razon_social"></param>
        /// <param name="calle"></param>
        /// <param name="no_ext"></param>
        /// <param name="no_int"></param>
        /// <param name="cp"></param>
        /// <param name="colpobcd"></param>
        /// <param name="delmun"></param>
        /// <param name="ciudad"></param>
        /// <param name="estado"></param>
        /// <param name="correo"></param>
        /// <returns>Objeto RespondeWS</returns>
        public ResponseWS ABCClientesAppSinEncriptar(string usuario, string contrasenia, string rfc, string razon_social, string calle, string no_ext,
                                     string no_int, string cp, string colpobcd, string delmun, string ciudad, string estado, string correo)
        {
            string validaDatos = "";
            ID_Usuario = validaUsuario(usuario, contrasenia);
            if (ID_Usuario >= 0)
            {
                validaDatos = ValidaDatosFiscales(rfc, razon_social, calle, no_ext, cp, colpobcd, delmun, ciudad, estado, correo);
                if (validaDatos == "")
                {
                    int ID_Cliente = -1;
                    bool resp = false;
                    try
                    {
                        #region Asignación de variables
                        string Razon_social = "";
                        string Calle = "";
                        string No_Ext = "";
                        string No_Int = "";
                        string CP = "";
                        string ColPobCd = "";
                        string DelMun = "";
                        string Ciudad = "";
                        string Estado = "";
                        string Correo = "";

                        Razon_social = razon_social.Trim();
                        Calle = calle.Trim();
                        No_Ext = no_ext.Trim();
                        No_Int = no_int.Length > 0 ? no_int.Trim() : "";
                        CP = cp.Trim();
                        ColPobCd = colpobcd.Trim();
                        DelMun = delmun.Trim();
                        Ciudad = ciudad.Trim();
                        Estado = estado.Trim();
                        Correo = correo.Trim();
                        #endregion

                        ID_Cliente = BuscaCliente(rfc);
                        if (ID_Cliente > -1)
                        {
                            //Modificación
                            BLClientes blcupdate = new BLClientes();
                            try
                            {
                                resp = blcupdate.actualizaCliente(ID_Cliente, Razon_social, Calle, No_Ext, No_Int,
                                    CP, ColPobCd, DelMun, Ciudad, Estado);
                                if (!resp)
                                {
                                    response.Mensaje = "Error|" + "No se pudo actualizar el cliente : " + ID_Cliente;
                                    response.Numero = -1;
                                    response.Sucess = false;
                                    insertaLog("ABCClientesApp", "actualizaCliente", "Error", "No se pudo actualizar el cliente : " + ID_Cliente, false);
                                }
                            }
                            catch (Exception ex)
                            {
                                insertaLog("ABCClientesApp", "buscaClienteRFC", "Error", ex.Message, true);
                                //respws = "Error|" + ex.Message;

                                error.Mensaje = "Error| buscaClienteRFC: " + ex.Message;
                                throw new Exception(error.Mensaje);
                            }
                        }
                        else
                        {
                            //Alta
                            BLClientes blcinsert = new BLClientes();
                            try
                            {
                                resp = blcinsert.insertaClientes(rfc, Razon_social, Calle, No_Ext, No_Int,
                                    CP, ColPobCd, DelMun, Ciudad, Estado);
                                if (!resp)
                                {
                                    response.Mensaje = "Error| insertaClientes: " + "No se pudo insertar el cliente : " + rfc;
                                    response.Numero = -1;
                                    response.Sucess = false;
                                    //respws = "Error|" + "No se pudo insertar el cliente : " + rfc;
                                    insertaLog("ABCClientesApp", "insertaClientes", "Error", "No se pudo insertar el cliente : " + rfc, false);
                                }
                                else
                                    ID_Cliente = BuscaCliente(rfc);
                            }
                            catch (Exception ex)
                            {
                                insertaLog("ABCClientesApp", "buscaClienteRFC", "Error", ex.Message, true);
                                //respws = "Error|" + ex.Message;
                                error.Mensaje = "Error| buscaClienteRFC: " + ex.Message;
                                throw new Exception(error.Mensaje);
                            }
                        }

                        if (resp)
                        {
                            resp = BuscaCorreoCliente(ID_Cliente, Correo);
                            if (resp)
                            {
                                //respws = "Ok|" + ID_Cliente.ToString() + "|" + rfc;
                                response.Mensaje = "Ok|" + ID_Cliente.ToString() + "|" + rfc;
                                response.Numero = 1;
                                response.Sucess = true;
                                insertaLog("ABCClientesApp", "ABCClientesApp", "Ok", "Registro insertado / actualizado. Cliente : " + ID_Cliente, false);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //respws = "Error|" + ex.Message;
                        error.Mensaje = "Error|" + ex.Message;

                        if (ex.InnerException != null)
                            error.MensajeInterno = ex.InnerException.Message.ToString();

                        error.Numero = ex.HResult;
                        throw new FaultException<ErrorSistema>(error);
                    }
                }
                else
                {
                    //respws = "Error|" + validaDatos;
                    response.Mensaje = "Error|" + validaDatos;
                    response.Numero = -1;
                    response.Sucess = false;
                }
            }
            else
            {
                //respws = "Error|Usuario o contraseña invalidos";
                response.Mensaje = "Error|Usuario o contraseña invalidos";
                response.Numero = -1;
                response.Sucess = false;
            }

            return response;
        }

        /// <summary>
        /// Metodo de ABC-Clientes tikects donde encripta los datos fiscales
        /// </summary>
        /// <param name="datosfiscales_encriptados">
        /// usuario: array[0] 
        /// contasenia: array[1]
        /// rfc: array[2] 
        /// razon_social: array[3]
        /// calle: array[4]
        /// no_ext: array[5]
        /// no_int: array[6]
        /// cp: array[7]
        /// colpobcd: array[8]
        /// delmun: array[9]
        /// ciudad: array[10]
        /// estado: array[11]
        /// correo: array[12]
        /// </param>
        /// <returns>objeto ResponseWS</returns>
        public ResponseWS ABCClientesAppEncriptar(string datosfiscales_encriptados)
        {
            EncripDesencrip classED = new EncripDesencrip();
            string validaDatos = "";
            //string _PassWord = System.Configuration.ConfigurationManager.AppSettings["PasswordEncriptacion"].ToString();
            //string datos = classED.DesencriptarCadenaDeCaracteres(datosfiscales_encriptados, _PassWord);

            string datos = ServiceWebEspacia.Decrypt(datosfiscales_encriptados);

            string[] datosfiscales = datos.Split('|');

            ID_Usuario = validaUsuario(datosfiscales[0], datosfiscales[1]);
            if (ID_Usuario >= 0)
            {
                validaDatos = ValidaDatosFiscales(datosfiscales[2], datosfiscales[3], datosfiscales[4], datosfiscales[6], datosfiscales[7], datosfiscales[8],
                                                  datosfiscales[9], datosfiscales[10], datosfiscales[11], datosfiscales[12]);
                if (validaDatos == "")
                {
                    int ID_Cliente = -1;
                    bool resp = false;
                    try
                    {
                        #region Asignación de variables
                        string Razon_social = "";
                        string Calle = "";
                        string No_Ext = "";
                        string No_Int = "";
                        string CP = "";
                        string ColPobCd = "";
                        string DelMun = "";
                        string Ciudad = "";
                        string Estado = "";
                        string Correo = "";

                        Razon_social = datosfiscales[2];
                        Calle = datosfiscales[4].Trim();
                        No_Ext = datosfiscales[5].Trim();
                        No_Int = datosfiscales[6].Length > 0 ? datosfiscales[6].Trim() : "";
                        CP = datosfiscales[7].Trim();
                        ColPobCd = datosfiscales[8].Trim();
                        DelMun = datosfiscales[9].Trim();
                        Ciudad = datosfiscales[11].Trim();
                        Estado = datosfiscales[12].Trim();
                        Correo = datosfiscales[13].Trim();
                        #endregion

                        ID_Cliente = BuscaCliente(datosfiscales[2]);
                        if (ID_Cliente > -1)
                        {
                            //Modificación
                            BLClientes blcupdate = new BLClientes();
                            try
                            {
                                resp = blcupdate.actualizaCliente(ID_Cliente, Razon_social, Calle, No_Ext, No_Int,
                                    CP, ColPobCd, DelMun, Ciudad, Estado);
                                if (!resp)
                                {
                                    response.Mensaje = "Error|" + "No se pudo actualizar el cliente : " + ID_Cliente;
                                    response.Numero = -1;
                                    response.Sucess = false;
                                    insertaLog("ABCClientesApp", "actualizaCliente", "Error", "No se pudo actualizar el cliente : " + ID_Cliente, false);
                                }
                            }
                            catch (Exception ex)
                            {
                                insertaLog("ABCClientesApp", "buscaClienteRFC", "Error", ex.Message, true);
                                //respws = "Error|" + ex.Message;

                                error.Mensaje = "Error| buscaClienteRFC: " + ex.Message;
                                throw new Exception(error.Mensaje);
                            }
                        }
                        else
                        {
                            //Alta
                            BLClientes blcinsert = new BLClientes();
                            try
                            {
                                resp = blcinsert.insertaClientes(datosfiscales[2], Razon_social, Calle, No_Ext, No_Int, CP, ColPobCd, DelMun, Ciudad, Estado);
                                if (!resp)
                                {
                                    response.Mensaje = "Error|" + "No se pudo insertar el cliente : " + datosfiscales[2];
                                    response.Numero = -1;
                                    response.Sucess = false;
                                    //respws = "Error|" + "No se pudo insertar el cliente : " + rfc;
                                    insertaLog("ABCClientesApp", "insertaClientes", "Error", "No se pudo insertar el cliente : " + datosfiscales[2], false);
                                }
                                else
                                    ID_Cliente = BuscaCliente(datosfiscales[2]);
                            }
                            catch (Exception ex)
                            {
                                insertaLog("ABCClientesApp", "buscaClienteRFC", "Error", ex.Message, true);
                                //respws = "Error|" + ex.Message;
                                error.Mensaje = "Error| buscaClienteRFC: " + ex.Message;
                                throw new Exception(error.Mensaje);
                            }
                        }

                        if (resp)
                        {
                            resp = BuscaCorreoCliente(ID_Cliente, Correo);
                            if (resp)
                            {
                                //respws = "Ok|" + ID_Cliente.ToString() + "|" + rfc;
                                response.Mensaje = "Ok|" + ID_Cliente.ToString() + "|" + datosfiscales[2];
                                response.Numero = 1;
                                response.Sucess = true;
                                insertaLog("ABCClientesApp", "ABCClientesApp", "Ok", "Registro insertado / actualizado. Cliente : " + ID_Cliente, false);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //respws = "Error|" + ex.Message;
                        error.Mensaje = "Error|" + ex.Message;

                        if (ex.InnerException != null)
                            error.MensajeInterno = ex.InnerException.Message.ToString();

                        error.Numero = ex.HResult;
                        throw new FaultException<ErrorSistema>(error);
                    }
                }
                else
                {
                    //respws = "Error|" + validaDatos;
                    response.Mensaje = "Error|" + validaDatos;
                    response.Numero = -1;
                    response.Sucess = false;
                }
            }
            else
            {
                //respws = "Error|Usuario o contraseña invalidos";
                response.Mensaje = "Error|Usuario o contraseña invalidos";
                response.Numero = -1;
                response.Sucess = false;
            }

            return response;
        }

        /// <summary>
        /// Metodo creaFactura sin encriptar
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="contrasenia"></param>
        /// <param name="rfc"></param>
        /// <param name="tickets"></param>
        /// <returns>objeto ResponseWS</returns>
        public ResponseWS creaFacturaSinEncriptar(string usuario, string contrasenia, string rfc, string tickets)
        {
            string respws = "";
            string validaDatos = "";
            try
            {
                ID_Usuario = validaUsuario(usuario, contrasenia);
                if (ID_Usuario >= 0)
                {
                    string Correo = "";
                    #region Busca Cliente
                    Clientes cliente_ = new Clientes();
                    cliente_ = GetCliente(rfc);
                    if (cliente_ != null)
                    {
                        validaDatos = ValidaTicket(tickets);
                        if (validaDatos == "")
                        {
                            #region Obtiene y crea Tabla de tickets
                            DataTable tablatickets = new DataTable();

                            tablatickets = TicketsaFacturar(tickets);

                            if (tablatickets.Rows.Count > 0)
                            {
                                #region Timbra y guarda cfdi
                                TimbraCFDI_FI_33.CreaYTimbraCFDI _creatimbracfdi = new TimbraCFDI_FI_33.CreaYTimbraCFDI();
                                string[] resultado_timbre = new string[4];
                                bool EsProduccion = false;

                                Correo = BuscaCorreoCliente(cliente_.ID_Cliente);

                                EsProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["EsProduccion"].ToString());

                                resultado_timbre = _creatimbracfdi.CreaTimbraCFDIWebService(tablatickets, cliente_.RFC, cliente_.Razon_social, cliente_.ID_Cliente, "A", Correo, "Se facturan los tickets: " + tickets, EsProduccion);

                                if (resultado_timbre != null)
                                {
                                    if (resultado_timbre.Length > 0 && resultado_timbre[0] == "Ok")
                                    {
                                        response.Mensaje = resultado_timbre[0] + "|" + resultado_timbre[1];
                                        response.Numero = 1;
                                        response.Sucess = true;
                                    }
                                    else
                                    {
                                        response.Mensaje = resultado_timbre[1] + "\n" + resultado_timbre[2] + "\n" + resultado_timbre[3];
                                        response.Numero = -1;
                                        response.Sucess = false;
                                    }
                                }
                                else
                                {
                                    response.Mensaje = "Error: Intentelo nuevamente.";
                                    response.Numero = -1;
                                    response.Sucess = false;
                                }
                                #endregion
                            }
                            else
                            {
                                error.Mensaje = " El Cliente registrado. RFC : " + rfc + ", no ingreso ningún ticket.";
                                throw new Exception(error.Mensaje);
                            }

                            #endregion
                        }
                        else
                        {
                            respws = validaDatos;
                            error.Mensaje = " " + validaDatos;
                            throw new Exception(error.Mensaje);
                        }
                    }
                    else
                    {
                        //respws = "Error|El Cliente no se encuentra registrado. RFC : " + rfc;
                        error.Mensaje = " El Cliente no se encuentra registrado. RFC : " + rfc;
                        throw new Exception(error.Mensaje);
                    }
                    #endregion
                }
                else
                {
                    //respws = "Error|Usuario o contraseña invalidos";
                    error.Mensaje = " Usuario o contraseña invalidos";
                    throw new Exception(error.Mensaje);
                }
            }
            catch (Exception ex)
            {
                error.Mensaje = "Error buscaTicket:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }

            return response;
        }

        /// <summary>
        /// Metodo creaFacturaEncriptar donde encripta los datos del usuario, contrasenia y rfc
        /// </summary>
        /// <param name="datosUsu_Con_Rfc_encriptados">
        /// usuario: array[0] 
        /// contasenia: array[1]
        /// rfc: array[2] 
        /// </param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        public ResponseWS creaFacturaEncriptar(string datosUsu_Con_Rfc_encriptados, string tickets)
        {
            string respws = "";
            string validaDatos = "";

            try
            {
                string datos = Decrypt(datosUsu_Con_Rfc_encriptados);

                string[] datosusuario = datos.Split('|');

                ID_Usuario = validaUsuario(datosusuario[0], datosusuario[1]);

                if (ID_Usuario >= 0)
                {
                    string Correo = "";
                    try
                    {
                        #region Busca Cliente
                        Clientes cliente_ = new Clientes();
                        cliente_ = GetCliente(datosusuario[2]);
                        if (cliente_ != null)
                        {
                            validaDatos = ValidaTicket(tickets);
                            if (validaDatos == "")
                            {
                                #region Obtiene y crea Tabla de tickets
                                DataTable tablatickets = new DataTable();

                                tablatickets = TicketsaFacturar(tickets);

                                if (tablatickets.Rows.Count > 0)
                                {
                                    #region Timbra y guarda cfdi
                                    TimbraCFDI_FI_33.CreaYTimbraCFDI _creatimbracfdi = new TimbraCFDI_FI_33.CreaYTimbraCFDI();
                                    string[] resultado_timbre = new string[4];
                                    bool EsProduccion = false;

                                    Correo = BuscaCorreoCliente(cliente_.ID_Cliente);

                                    EsProduccion = Convert.ToBoolean(ConfigurationManager.AppSettings["EsProduccion"].ToString());

                                    resultado_timbre = _creatimbracfdi.CreaTimbraCFDIWebService(tablatickets, cliente_.RFC, cliente_.Razon_social, cliente_.ID_Cliente, "A", Correo, "Se facturan los tickets: " + tickets, EsProduccion);

                                    if (resultado_timbre != null)
                                    {
                                        if (resultado_timbre.Length > 0 && resultado_timbre[0] == "Ok")
                                        {
                                            response.Mensaje = resultado_timbre[0] + "|" + resultado_timbre[1];
                                            response.Numero = 1;
                                            response.Sucess = true;
                                        }
                                        else
                                        {
                                            response.Mensaje = resultado_timbre[1] + "\n" + resultado_timbre[2] + "\n" + resultado_timbre[3];
                                            response.Numero = -1;
                                            response.Sucess = false;
                                        }                                        
                                    }
                                    else
                                    {
                                        response.Mensaje = "Error: Intentelo nuevamente.";
                                        response.Numero = -1;
                                        response.Sucess = false;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    error.Mensaje = " El Cliente registrado. RFC : " + datosusuario[2] + ", no ingreso ningún ticket.";
                                    throw new Exception(error.Mensaje);
                                }

                                #endregion
                            }
                            else
                            {
                                respws = validaDatos;
                                error.Mensaje = " " + validaDatos;
                                throw new Exception(error.Mensaje);
                            }
                        }
                        else
                        {
                            //respws = "Error|El Cliente no se encuentra registrado. RFC : " + rfc;
                            error.Mensaje = " El Cliente no se encuentra registrado. RFC : " + datosusuario[2];
                            throw new Exception(error.Mensaje);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //respws = "Error|" + ex.Message;
                        insertaLog("creafactura", "buscacCliente", "Error", ex.Message, true);
                        error.Mensaje = ex.Message;
                        throw new Exception(error.Mensaje);
                    }
                }
                else
                {
                    error.Mensaje = " Usuario o contraseña invalidos";
                    throw new Exception(error.Mensaje);
                }
            }
            catch (Exception ex)
            {
                error.Mensaje = "Error buscaTicket:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }

            return response;
        }

        #region Metodos creaFactura
        private string ValidaTicket(string _tickets)
        {
            string resp = "";
            string noTiket = "";
            string faTicket = "";
            string ftTicket = "";

            char[] separador = { '|' };
            string[] inTickets = _tickets.Split(separador);
            if (_tickets != "")
            {
                foreach (string no_ticket in inTickets)
                {
                    bool puedefacturar = true;
                    string No_Ticket = "";
                    No_Ticket = no_ticket.Trim();

                    BLTickets blt = new BLTickets();
                    try
                    {
                        List<Tickets> csT = blt.buscaTicket(No_Ticket);
                        if (csT.Count > 0)
                        {
                            BLTickets_Facturados bltf = new BLTickets_Facturados();
                            try
                            {
                                List<Tickets_Facturados> csTF = bltf.buscaTickets_Facturados(No_Ticket);
                                for (int n = 0; n < csTF.Count; n++)
                                {
                                    if (csTF[n].Valido)
                                        puedefacturar = false;
                                    else
                                        puedefacturar = true;
                                }

                                if (puedefacturar)
                                {
                                    DateTime Fecha_Ticket = new DateTime();
                                    int Mes_Ticket = 0;
                                    int Anio_Ticket = 0;
                                    Fecha_Ticket = csT[0].Fecha_Ticket;
                                    Mes_Ticket = Fecha_Ticket.Month;
                                    Anio_Ticket = Fecha_Ticket.Year;

                                    if (Mes_Ticket == DateTime.Now.Month && Anio_Ticket == DateTime.Now.Year)
                                    {
                                    }
                                    else
                                    {
                                        if (ftTicket == "")
                                            ftTicket = no_ticket;
                                        else
                                            ftTicket = ftTicket + ", " + no_ticket;
                                    }
                                }
                                else
                                {
                                    if (faTicket == "")
                                        faTicket = no_ticket;
                                    else
                                        faTicket = faTicket + ", " + no_ticket;
                                }


                            }
                            catch (Exception ex)
                            {
                                insertaLog("validaTicket", "buscaTicket", "Error", ex.Message, true);
                                error.Mensaje = " buscaTicket:|" + ex.Message;

                                error.Mensaje = "Error buscaTicket:|" + ex.Message;

                                if (ex.InnerException != null)
                                    error.MensajeInterno = ex.InnerException.Message.ToString();

                                error.Numero = ex.HResult;
                                throw new FaultException<ErrorSistema>(error);
                                //throw ex;
                            }
                        }
                        else
                        {
                            if (noTiket == "")
                                noTiket = No_Ticket;
                            else
                                noTiket = noTiket + ", " + no_ticket;
                        }
                    }
                    catch (Exception ex)
                    {
                        insertaLog("validaTicket", "buscaTicket", "Error", ex.Message, true);

                        error.Mensaje = "Error buscaTicket:|" + ex.Message;

                        if (ex.InnerException != null)
                            error.MensajeInterno = ex.InnerException.Message.ToString();

                        error.Numero = ex.HResult;
                        throw new FaultException<ErrorSistema>(error);
                        //throw ex;
                    }
                }

                if (noTiket != "")
                {
                    resp = "Error|No existe Ticket : " + noTiket;
                    if (faTicket != "")
                    {
                        resp = resp + ", Ticket facurado : " + faTicket;
                        if (ftTicket != "")
                            resp = resp + ", Ticket fuera de tiempo : " + ftTicket;
                    }

                }
                else if (faTicket != "")
                {
                    resp = "Error|Ticket facurado : " + faTicket;
                    if (ftTicket != "")
                        resp = resp + ", Ticket fuera de tiempo : " + ftTicket;
                }
                else if (ftTicket != "")
                    resp = "Error|Ticket fuera de tiempo : " + ftTicket;

            }
            else
                resp = "Error|No se enviarón tickets para facturar";

            return resp;

        }        

        private string BuscaCorreoCliente(int _id_cliente)
        {
            string Correo = "";
            BLCorreo_Cliente blcc = new BLCorreo_Cliente();
            try
            {
                List<Correo_Cliente> csCC = blcc.BuscaCorreo(_id_cliente);
                for (int n = 0; n < csCC.Count; n++)
                {
                    Correo = csCC[n].Correo;
                }
            }
            catch (Exception ex)
            {
                insertaLog("BuscaCorreoCliente", "BuscaCorreo", "Error", ex.Message, true);
                error.Mensaje = "Error BuscaCorreo:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }

            return Correo;
        }

        private DataTable TicketsaFacturar(string _tickets)
        {
            DataTable dtTSeleccionados = new DataTable();

            dtTSeleccionados.Columns.Add(new DataColumn("No_Ticket", typeof(string)));
            dtTSeleccionados.Columns.Add(new DataColumn("Nombre_Sistema", typeof(string)));
            dtTSeleccionados.Columns.Add(new DataColumn("Fecha_Ticket", typeof(DateTime)));
            dtTSeleccionados.Columns.Add(new DataColumn("Importe_Total", typeof(decimal)));
            dtTSeleccionados.Columns.Add(new DataColumn("Forma_Pago", typeof(string)));
            dtTSeleccionados.Columns.Add(new DataColumn("Borrar", typeof(string)));

            char[] separador = { '|' };
            string[] inTickets = _tickets.Split(separador);
            if (_tickets != "")
            {
                foreach (string no_ticket in inTickets)
                {
                    string No_Ticket = "";
                    No_Ticket = no_ticket.Trim();

                    BLTickets blt = new BLTickets();
                    List<Tickets> csT = blt.buscaTicket(No_Ticket);
                    if (csT.Count > 0)
                    {
                        BLTickets_Facturados bltf = new BLTickets_Facturados();

                        Tickets_Facturados csTF = bltf.buscaTickets_Facturados(No_Ticket).FirstOrDefault();
                        if (csTF == null)
                        {
                            DataRow drticket = dtTSeleccionados.NewRow();

                            drticket["No_Ticket"] = No_Ticket;
                            drticket["Nombre_Sistema"] = csT[0].Nombre_Sistema;
                            drticket["Fecha_Ticket"] = csT[0].Fecha_Ticket;
                            drticket["Importe_Total"] = Convert.ToDecimal(csT[0].Importe_Total);
                            drticket["Forma_Pago"] = csT[0].Forma_Pago;

                            dtTSeleccionados.Rows.Add(drticket);
                        }
                        else
                        {
                            if (!csTF.Valido)
                            {
                                DataRow drticket = dtTSeleccionados.NewRow();

                                drticket["No_Ticket"] = No_Ticket;
                                drticket["Nombre_Sistema"] = csT[0].Nombre_Sistema;
                                drticket["Fecha_Ticket"] = csT[0].Fecha_Ticket;
                                drticket["Importe_Total"] = Convert.ToDecimal(csT[0].Importe_Total);
                                drticket["Forma_Pago"] = csT[0].Forma_Pago;

                                dtTSeleccionados.Rows.Add(drticket);
                            }
                        }
                    }
                    else
                    {
                        dtTSeleccionados.Rows.Clear();
                    }
                }
            }

            return dtTSeleccionados;
        }
        #endregion

        #region Métodos ABCClientesApp
        private string ValidaDatosFiscales(string _rfc, string _razon_social, string _calle, string _no_ext, string _cp,
            string _colpobcd, string _delmun, string _ciduad, string _estado, string _correo)
        {
            string resp = "";

            if (_rfc.Length > 11)
            {
                string newRFC = string.Empty;
                foreach (char c in _rfc)
                {
                    if (!char.IsLetterOrDigit(c))
                    {
                        resp = "El RFC no debe contener espacios ni caracteres especiales";
                    }
                    else
                        newRFC = newRFC + c;
                }

                if (resp == "")
                    _rfc = newRFC;

                if (_rfc.Length > 13)
                {
                    if (resp == "")
                        resp = "El  RFC no debe contener mas de 13 caracteres alfanumericos";
                    else
                        resp = resp + ". " + "El  RFC no debe contener mas de 13 caracteres alfanumericos";
                }
            }
            else
                resp = "Ingrese un RFC valido";

            if (_razon_social.Length < 5)
            {
                if (resp == "")
                    resp = "Debes ingresar una Razón Social valida";
                else
                    resp = resp + ", Debes ingresar una Razón Social valida";
            }

            if (_calle.Length < 5)
            {
                if (resp == "")
                    resp = "Debes ingresar una Calle valida";
                else
                    resp = resp + ", Debes ingresar una Calle valida";
            }

            if (_no_ext.Length < 1)
            {
                if (resp == "")
                    resp = "Debes ingresar un No. Exterior valido";
                else
                    resp = resp + ", Debes ingresar un No. Exterior valido";
            }

            if (_cp.Length != 5)
            {
                if (resp == "")
                    resp = "Debes ingresar un Código Postal valido";
                else
                    resp = resp + ", Debes ingresar un Código Postal valido";
            }

            if (_colpobcd.Length < 5)
            {
                if (resp == "")
                    resp = "Debes ingresar una Colonia, Poblacion o Ciudad valida";
                else
                    resp = resp + ", Debes ingresar una Colonia, Poblacion o Ciudad valida";
            }

            if (_delmun.Length < 5)
            {
                if (resp == "")
                    resp = "Debes ingresar una Delegación o Municipio valida";
                else
                    resp = resp + ", Debes ingresar una Delegación o Municipio valida";
            }

            if (_ciduad.Length < 5)
            {
                if (resp == "")
                    resp = "Debes ingresar una Ciudad valida";
                else
                    resp = resp + ", Debes ingresar una Ciudad valida";
            }

            if (_estado.Length < 4)
            {
                if (resp == "")
                    resp = "Debes ingresar un Estado valido";
                else
                    resp = resp + ", Debes ingresar un Estado valido";
            }

            string strFormat = string.Empty;
            strFormat = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (!Regex.IsMatch(_correo, strFormat))
            {
                if (resp == "")
                    resp = "Debes ingresar un correo valido";
                else
                    resp = resp + ". " + "Debes ingresar un correo valido";
            }

            return resp;
        }

        private bool BuscaCorreoCliente(int _id_cliente, string _correo)
        {
            bool bresp = false;
            BLCorreo_Cliente blcc = new BLCorreo_Cliente();
            BLCorreo_Cliente blccupdate = new BLCorreo_Cliente();
            BLCorreo_Cliente blccinsert = new BLCorreo_Cliente();
            try
            {
                List<Correo_Cliente> csCC = blcc.BuscaCorreo(_id_cliente, _correo);
                if (csCC.Count > 0)
                {
                    int ID_Correo = 0;
                    ID_Correo = csCC[0].ID_Correo;
                    if (!csCC[0].Valido)
                        bresp = blccupdate.ActualizaCorreoCliente(ID_Correo, true);
                    else
                        bresp = true;
                }
                else
                {
                    bresp = blccinsert.insertaCorreoCliente(_id_cliente, _correo);
                }
            }
            catch (Exception ex)
            {
                insertaLog("BuscaCorreoCliente", "BuscaCorreoCliente", "Error", ex.Message, true);
                throw ex;
            }

            return bresp;
        }
        #endregion

        #region Métodos Comunes
        /// <summary>
        /// Verifica que el usuario exista
        /// </summary>
        /// <param name="_usuario"></param>
        /// <param name="_contrasenia"></param>
        /// <returns></returns>
        private int validaUsuario(string _usuario, string _contrasenia)
        {
            int id_usuario = -1;
            if (_usuario != "" && _contrasenia != "")
            {
                BLUsuarios blu = new BLUsuarios();
                try
                {
                    List<Usuarios> csUsuarios = blu.buscaUsuarioWS(_usuario);
                    if (csUsuarios.Count > 0)
                    {
                        string contrasenia = "";
                        contrasenia = csUsuarios[0].Contrasenia;
                        if (contrasenia == _contrasenia)
                            id_usuario = csUsuarios[0].Id_Usuario;
                    }
                }
                catch (Exception ex)
                {
                    insertaLog("recibeTickets", "validaUsuario", "Error", ex.Message, true);

                    error.Mensaje = "Error validaUsuario:|" + ex.Message;

                    if (ex.InnerException != null)
                        error.MensajeInterno = ex.InnerException.Message.ToString();

                    error.Numero = ex.HResult;
                    throw new FaultException<ErrorSistema>(error);
                }
            }
            return id_usuario;
        }

        private int BuscaCliente(string _rfc)
        {
            int ID_Cliente = -1;
            BLClientes blc = new BLClientes();
            try
            {
                List<Clientes> csCliente = blc.buscaClienteRFC(_rfc);
                if (csCliente.Count > 0)
                {
                    ID_Cliente = csCliente[0].ID_Cliente;

                }
            }
            catch (Exception ex)
            {
                insertaLog("BuscaCliente", "buscaClienteRFC", "Error", ex.Message, true);
                error.Mensaje = "Error BuscaCliente:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }

            return ID_Cliente;

        }

        private Clientes GetCliente(string _rfc)
        {
            BLClientes blc = new BLClientes();
            Clientes csCliente = new Clientes();
            try
            {
                csCliente = blc.buscaClienteRFC(_rfc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                insertaLog("BuscaCliente", "buscaClienteRFC", "Error", ex.Message, true);
                error.Mensaje = "Error BuscaCliente:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }

            return csCliente;

        }
        #endregion

        #region Manejo de errores
        /// <summary>
        /// Metodo para insertar Log
        /// </summary>
        /// <param name="_evento"></param>
        /// <param name="_metodo"></param>
        /// <param name="_respuesta"></param>
        /// <param name="_descripcion"></param>
        /// <param name="_essistema"></param>
        private void insertaLog(string _evento, string _metodo, string _respuesta, string _descripcion, bool _essistema)
        {
            DateTime dtAhora = new DateTime();

            dtAhora = DateTime.Now;
            BLLogs bllog = new BLLogs();
            try
            {
                bool resp = false;
                resp = bllog.insertaLog("ServiceEspacia", _evento, _metodo, dtAhora, _respuesta, _descripcion, _essistema, ID_Usuario);
            }
            catch (Exception ex)
            {
                error.Mensaje = "Error insertaLog:|" + ex.Message;

                if (ex.InnerException != null)
                    error.MensajeInterno = ex.InnerException.Message.ToString();

                error.Numero = ex.HResult;
                throw new FaultException<ErrorSistema>(error);
            }
        }

        public string HelloWorld(string cadena)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
