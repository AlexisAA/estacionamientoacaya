﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF_Facturacion
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceWebEspacia" in both code and config file together.
    [ServiceContract]
    public interface IServiceWebEspacia
    {
        [OperationContract(Name = "HelloWorld")]
        [FaultContract(typeof(ErrorSistema))]
        string HelloWorld(string cadena);

        [OperationContract(Name = "Error")]
        [FaultContract(typeof(ErrorSistema))]
        decimal DividebyZero(decimal input);

        [OperationContract(Name = "EcriptarInfomacion")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseEncriptarDesencriptar EcriptarInfomacion(string textoencriptar);

        [OperationContract(Name = "DesecriptarInfomacion")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseEncriptarDesencriptar DesecriptarInfomacion(string textoencriptar);

        [OperationContract(Name = "recibeTicketsSinEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS recibeTicketsSinEncriptar(string usuario, string contrasenia, DateTime fecha, int no_registros, DataTable tickets);

        [OperationContract(Name = "recibeTicketsEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS recibeTicketsEncriptarUserPass(string usuariocontrasenia_encriptado, DateTime fecha, int no_registros, DataTable tickets);

        [OperationContract(Name = "ABCClientesAppSinEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS ABCClientesAppSinEncriptar(string usuario, string contrasenia, string rfc, string razon_social, string calle, string no_ext,
                                     string no_int, string cp, string colpobcd, string delmun, string ciudad, string estado, string correo);

        [OperationContract(Name = "ABCClientesAppEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS ABCClientesAppEncriptar(string datosfiscales_encriptados);

        [OperationContract(Name = "creaFacturaSinEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS creaFacturaSinEncriptar(string usuario, string contrasenia, string rfc, string tickets);

        [OperationContract(Name = "creaFacturaEncriptar")]
        [FaultContract(typeof(ErrorSistema))]
        ResponseWS creaFacturaEncriptar(string datosUsu_Con_Rfc_encriptados, string tickets);
    }

    [DataContract]
    public class ErrorSistema
    {
        private string dt_mensaje;
        private string dt_mensajeinterno;
        private int dt_numero;

        [DataMember]
        public string Mensaje
        {
            get { return dt_mensaje; }
            set { dt_mensaje = value; }
        }

        [DataMember]
        public string MensajeInterno
        {
            get { return dt_mensajeinterno; }
            set { dt_mensajeinterno = value; }
        }

        [DataMember]
        public int Numero
        {
            get { return dt_numero; }
            set { dt_numero = value; }
        }
    }

    [DataContract]
    public class ResponseWS
    {
        private string dt_mensaje;
        private int dt_numero;
        private bool dt_sucess;

        [DataMember]
        public string Mensaje
        {
            get { return dt_mensaje; }
            set { dt_mensaje = value; }
        }

        [DataMember]
        public int Numero
        {
            get { return dt_numero; }
            set { dt_numero = value; }
        }

        [DataMember]
        public bool Sucess
        {
            get { return dt_sucess; }
            set { dt_sucess = value; }
        }
    }

    [DataContract]
    public class ResponseEncriptarDesencriptar
    {
        private string dt_mensajeEncriptado;
        private string dt_mensajeDesencriptado;
        private int dt_numero;
        private bool dt_sucess;

        [DataMember]
        public string MensajeEncriptado
        {
            get { return dt_mensajeEncriptado; }
            set { dt_mensajeEncriptado = value; }
        }

        [DataMember]
        public string MensajeDesencriptado
        {
            get { return dt_mensajeDesencriptado; }
            set { dt_mensajeDesencriptado = value; }
        }

        [DataMember]
        public int Numero
        {
            get { return dt_numero; }
            set { dt_numero = value; }
        }

        [DataMember]
        public bool Sucess
        {
            get { return dt_sucess; }
            set { dt_sucess = value; }
        }
    }
}
